package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao;

import java.io.Serializable;

/* loaded from: classes.dex */
public class GDAOPodcast implements Serializable {
    private String artist;
    private String description;
    private Long id;
    private String image_url;
    private String name;

    public GDAOPodcast() {
    }

    public String getArtist() {
        return this.artist;
    }

    public String getDescription() {
        return this.description;
    }

    public Long getId() {
        return this.id;
    }

    public String getImage_url() {
        return this.image_url;
    }

    public String getName() {
        return this.name;
    }

    public void setArtist(String str) {
        this.artist = str;
    }

    public void setDescription(String str) {
        this.description = str;
    }

    public void setId(Long l) {
        this.id = l;
    }

    public void setImage_url(String str) {
        this.image_url = str;
    }

    public void setName(String str) {
        this.name = str;
    }

    public GDAOPodcast(Long l) {
        this.id = l;
    }

    public GDAOPodcast(Long l, String str, String str2, String str3, String str4) {
        this.id = l;
        this.name = str;
        this.artist = str2;
        this.description = str3;
        this.image_url = str4;
    }
}
