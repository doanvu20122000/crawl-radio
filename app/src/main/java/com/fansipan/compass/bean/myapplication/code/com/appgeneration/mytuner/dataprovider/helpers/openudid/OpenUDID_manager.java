package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.helpers.openudid;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import android.provider.Settings;
import android.util.Log;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

/* loaded from: classes.dex */
public class OpenUDID_manager implements ServiceConnection {
    private static final String ACTION_GET_UDID = "org.OpenUDID.GETUDID";
    private static final boolean LOG = true;
    private static String OpenUDID = null;
    public static final String PREFS_NAME = "openudid_prefs";
    public static final String PREF_KEY = "openudid";
    public static final String TAG = "OpenUDID";
    private static boolean mInitialized = false;
    public static ArrayList<OpenUDIDListener> mListenerList;
    private final Context mContext;
    private List<ResolveInfo> mMatchingIntents;
    private final SharedPreferences mPreferences;
    private final Random mRandom;
    private Map<String, Integer> mReceivedOpenUDIDs;

    /* loaded from: classes.dex */
    public interface OpenUDIDListener {
        void openUDIDInitialized(String str);
    }

    /* loaded from: classes.dex */
    public class ValueComparator implements Comparator {
        private ValueComparator() {
        }

        @Override // java.util.Comparator
        public int compare(Object obj, Object obj2) {
            if (((Integer) OpenUDID_manager.this.mReceivedOpenUDIDs.get(obj)).intValue() < ((Integer) OpenUDID_manager.this.mReceivedOpenUDIDs.get(obj2)).intValue()) {
                return 1;
            }
            if (OpenUDID_manager.this.mReceivedOpenUDIDs.get(obj) == OpenUDID_manager.this.mReceivedOpenUDIDs.get(obj2)) {
                return 0;
            }
            return -1;
        }
    }

    private OpenUDID_manager(Context context) {
        mListenerList = new ArrayList<>();
        this.mPreferences = context.getSharedPreferences(PREFS_NAME, 0);
        this.mContext = context;
        this.mRandom = new Random();
        this.mReceivedOpenUDIDs = new HashMap();
    }

    private void generateOpenUDID() {
        Log.d(TAG, "Generating openUDID");
        String string = Settings.Secure.getString(this.mContext.getContentResolver(), "android_id");
        OpenUDID = string;
        if (string == null || string.equals("9774d56d682e549c") || OpenUDID.length() < 15) {
            OpenUDID = new BigInteger(64, new SecureRandom()).toString(16);
        }
    }

    private void getMostFrequentOpenUDID() {
        if (!this.mReceivedOpenUDIDs.isEmpty()) {
            TreeMap treeMap = new TreeMap(new ValueComparator());
            treeMap.putAll(this.mReceivedOpenUDIDs);
            OpenUDID = (String) treeMap.firstKey();
        }
    }

    public static String getOpenUDID() {
        if (!mInitialized) {
            Log.d(TAG, "Initialisation isn't done");
        }
        return OpenUDID;
    }

    public static boolean isInitialized() {
        return mInitialized;
    }

    private static void sendCallback() {
        Iterator<OpenUDIDListener> it = mListenerList.iterator();
        while (it.hasNext()) {
            OpenUDIDListener next = it.next();
            if (next != null) {
                next.openUDIDInitialized(OpenUDID);
            }
        }
        mListenerList.clear();
    }

    private void startService() {
        if (this.mMatchingIntents.size() > 0) {
            StringBuilder m = new StringBuilder("Trying service ");
            m.append((Object) this.mMatchingIntents.get(0).loadLabel(this.mContext.getPackageManager()));
            Log.d(TAG, m.toString());
            ServiceInfo serviceInfo = this.mMatchingIntents.get(0).serviceInfo;
            Intent intent = new Intent();
            intent.setComponent(new ComponentName(serviceInfo.applicationInfo.packageName, serviceInfo.name));
            this.mMatchingIntents.remove(0);
            try {
                this.mContext.bindService(intent, this, Context.BIND_AUTO_CREATE);
                return;
            } catch (SecurityException unused) {
                startService();
                return;
            }
        }
        getMostFrequentOpenUDID();
        if (OpenUDID == null) {
            generateOpenUDID();
        }
        StringBuilder m2 = new StringBuilder("OpenUDID: ");
        m2.append(OpenUDID);
        Log.d(TAG, m2.toString());
        storeOpenUDID();
        mInitialized = true;
        sendCallback();
    }

    private void storeOpenUDID() {
        SharedPreferences.Editor edit = this.mPreferences.edit();
        edit.putString(PREF_KEY, OpenUDID);
        edit.commit();
    }

    public static void sync(Context context, OpenUDIDListener openUDIDListener) {
        OpenUDID_manager openUDID_manager = new OpenUDID_manager(context);
        mListenerList.add(openUDIDListener);
        String string = openUDID_manager.mPreferences.getString(PREF_KEY, null);
        OpenUDID = string;
        if (string == null) {
            openUDID_manager.mMatchingIntents = context.getPackageManager().queryIntentServices(new Intent(ACTION_GET_UDID), 0);
            Log.d(TAG, openUDID_manager.mMatchingIntents.size() + " services matches OpenUDID");
            if (openUDID_manager.mMatchingIntents != null) {
                openUDID_manager.startService();
                return;
            }
            return;
        }
        StringBuilder m = new StringBuilder("OpenUDID: ");
        m.append(OpenUDID);
        Log.d(TAG, m.toString());
        mInitialized = true;
        sendCallback();
    }

    @Override // android.content.ServiceConnection
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        String readString;
        try {
            Parcel obtain = Parcel.obtain();
            obtain.writeInt(this.mRandom.nextInt());
            Parcel obtain2 = Parcel.obtain();
            iBinder.transact(1, Parcel.obtain(), obtain2, 0);
            if (obtain.readInt() == obtain2.readInt() && (readString = obtain2.readString()) != null) {
                Log.d(TAG, "Received " + readString);
                if (this.mReceivedOpenUDIDs.containsKey(readString)) {
                    Map<String, Integer> map = this.mReceivedOpenUDIDs;
                    map.put(readString, Integer.valueOf(map.get(readString).intValue() + 1));
                } else {
                    this.mReceivedOpenUDIDs.put(readString, 1);
                }
            }
        } catch (RemoteException e2) {
            StringBuilder m = new StringBuilder("RemoteException: ");
            m.append(e2.getMessage());
            Log.d(TAG, m.toString());
        }
        this.mContext.unbindService(this);
        startService();
    }

    @Override // android.content.ServiceConnection
    public void onServiceDisconnected(ComponentName componentName) {
    }
}
