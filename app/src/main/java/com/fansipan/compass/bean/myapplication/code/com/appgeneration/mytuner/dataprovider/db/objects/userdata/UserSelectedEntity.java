package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.objects.userdata;


import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.objects.UserSelectable;

/* compiled from: UserSelectedEntity.kt */
/* loaded from: classes.dex */
public final class UserSelectedEntity {
    private final int order;
    private final UserSelectable selectable;
    private final long timestamp;
    private final UserType userType;

    /* compiled from: UserSelectedEntity.kt */
    /* loaded from: classes.dex */
    public enum MediaType {
        RADIO,
        PODCAST
    }

    /* compiled from: UserSelectedEntity.kt */
    /* loaded from: classes.dex */
    public enum UserType {
        FAVORITE,
        RECENT
    }

    public UserSelectedEntity(UserSelectable userSelectable, long j, int i, UserType userType) {
        this.selectable = userSelectable;
        this.timestamp = j;
        this.order = i;
        this.userType = userType;
    }

    public static /* synthetic */ UserSelectedEntity copy$default(UserSelectedEntity userSelectedEntity, UserSelectable userSelectable, long j, int i, UserType userType, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            userSelectable = userSelectedEntity.selectable;
        }
        if ((i2 & 2) != 0) {
            j = userSelectedEntity.timestamp;
        }
        long j2 = j;
        if ((i2 & 4) != 0) {
            i = userSelectedEntity.order;
        }
        int i3 = i;
        if ((i2 & 8) != 0) {
            userType = userSelectedEntity.userType;
        }
        return userSelectedEntity.copy(userSelectable, j2, i3, userType);
    }

    public final UserSelectable component1() {
        return this.selectable;
    }

    public final long component2() {
        return this.timestamp;
    }

    public final int component3() {
        return this.order;
    }

    public final UserType component4() {
        return this.userType;
    }

    public final UserSelectedEntity copy(UserSelectable userSelectable, long j, int i, UserType userType) {
        return new UserSelectedEntity(userSelectable, j, i, userType);
    }

    public final int getOrder() {
        return this.order;
    }


    public final long getTimestamp() {
        return this.timestamp;
    }

    public final UserType getUserType() {
        return this.userType;
    }

    public String toString() {
        StringBuilder m = new StringBuilder("UserSelectedEntity(selectable=");
        m.append(", timestamp=");
        m.append(this.timestamp);
        m.append(", order=");
        m.append(this.order);
        m.append(", userType=");
        m.append(this.userType);
        m.append(')');
        return m.toString();
    }
}
