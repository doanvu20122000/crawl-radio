package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.helpers.openudid;

import android.content.Context;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/* loaded from: classes.dex */
public class OpenUDIDFuture implements Future<String>, OpenUDID_manager.OpenUDIDListener {
    private final BlockingQueue<String> openUDIDValue = new ArrayBlockingQueue(1);
    private volatile State state = State.WAITING;

    /* loaded from: classes.dex */
    public enum State {
        WAITING,
        DONE,
        CANCELLED
    }

    public OpenUDIDFuture(Context context) {
        OpenUDID_manager.sync(context, this);
    }

    @Override // java.util.concurrent.Future
    public boolean cancel(boolean z) {
        this.state = State.CANCELLED;
        return true;
    }

    @Override // java.util.concurrent.Future
    public boolean isCancelled() {
        if (this.state == State.CANCELLED) {
            return true;
        }
        return false;
    }

    @Override // java.util.concurrent.Future
    public boolean isDone() {
        if (this.state == State.DONE) {
            return true;
        }
        return false;
    }

    @Override // com.appgeneration.mytuner.dataprovider.helpers.openudid.OpenUDID_manager.OpenUDIDListener
    public void openUDIDInitialized(String str) {
        try {
            this.openUDIDValue.put(str);
            this.state = State.DONE;
        } catch (InterruptedException unused) {
        }
    }

    @Override // java.util.concurrent.Future
    public String get() throws InterruptedException, ExecutionException {
        return this.openUDIDValue.take();
    }

    @Override // java.util.concurrent.Future
    public String get(long j, TimeUnit timeUnit) throws InterruptedException, ExecutionException, TimeoutException {
        String poll = this.openUDIDValue.poll(j, timeUnit);
        if (poll != null) {
            return poll;
        }
        throw new TimeoutException();
    }
}
