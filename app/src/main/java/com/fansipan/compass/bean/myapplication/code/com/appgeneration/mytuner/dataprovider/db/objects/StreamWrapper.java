package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.objects;

import com.appgeneration.mytuner.dataprovider.helpers.DateTimeHelpers;

import java.util.Date;
import java.util.LinkedList;
import java.util.Queue;

/* loaded from: classes.dex */
public class StreamWrapper {
    private boolean mDidPlay;
    private Date mEndDate;
    private boolean mHasMetadata;
    private Date mPlayDate;
    private Date mStartDate;
    private long mStreamId;

    public Queue<URLWrapper> getmUnresolvedUrls() {
        return mUnresolvedUrls;
    }

    private final Queue<URLWrapper> mUnresolvedUrls;

    public StreamWrapper(long j, URLWrapper uRLWrapper) {
        LinkedList linkedList = new LinkedList();
        this.mUnresolvedUrls = linkedList;
        this.mDidPlay = false;
        this.mHasMetadata = false;
        this.mStreamId = j;
        linkedList.add(uRLWrapper);
    }

    public boolean didFail() {
        return this.mUnresolvedUrls.isEmpty();
    }

    public boolean didPlay() {
        return this.mDidPlay;
    }

    public Date getEndDate() {
        return this.mEndDate;
    }

    public Date getPlayDate() {
        return this.mPlayDate;
    }

    public Date getStartDate() {
        return this.mStartDate;
    }

    public long getStreamId() {
        return this.mStreamId;
    }

    public boolean hasMetadata() {
        return this.mHasMetadata;
    }

    public boolean hasUnresolved() {
        return !this.mUnresolvedUrls.isEmpty();
    }

    public URLWrapper popNextUnresolvedOrNull() {
        return this.mUnresolvedUrls.poll();
    }

    public void setDidPlay(boolean z) {
        this.mDidPlay = z;
    }

    public void setEndDateToNow() {
        if (this.mEndDate == null) {
            this.mEndDate = DateTimeHelpers.getCurrentDate();
        }
    }

    public void setHasMetadata(boolean z) {
        this.mHasMetadata = z;
    }

    public void setPlayDateToNow() {
        if (this.mPlayDate == null) {
            this.mPlayDate = DateTimeHelpers.getCurrentDate();
        }
    }

    public void setStartDateToNow() {
        if (this.mStartDate == null) {
            this.mStartDate = DateTimeHelpers.getCurrentDate();
        }
    }

    public StreamWrapper(URLWrapper uRLWrapper) {
        LinkedList linkedList = new LinkedList();
        this.mUnresolvedUrls = linkedList;
        this.mStreamId = -1L;
        this.mDidPlay = false;
        this.mHasMetadata = false;
        linkedList.add(uRLWrapper);
    }

    @Override
    public String toString() {
        return "StreamWrapper{" +
                "mDidPlay=" + mDidPlay +
                ", mEndDate=" + mEndDate +
                ", mHasMetadata=" + mHasMetadata +
                ", mPlayDate=" + mPlayDate +
                ", mStartDate=" + mStartDate +
                ", mStreamId=" + mStreamId +
                ", mUnresolvedUrls=" + mUnresolvedUrls +
                '}';
    }
}
