package com.fansipan.compass.bean.myapplication.code.convert

data class CityEntity(
    val country: Int,
    val id: Int,
    val latitude: Double,
    val longitude: Double,
    val name: String,
    val state: Int
)