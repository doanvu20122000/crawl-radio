package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.objects;

import com.appgeneration.mytuner.dataprovider.helpers.DateTimeHelpers;
import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao.DaoSession;
import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao.GDAOAppUsageEvent;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/* loaded from: classes.dex */
public class AppUsageEvent {
    private final GDAOAppUsageEvent mDbAppUsageEvent;

    private AppUsageEvent(GDAOAppUsageEvent gDAOAppUsageEvent) {
        this.mDbAppUsageEvent = gDAOAppUsageEvent;
    }

    private static List<AppUsageEvent> convertList(List<GDAOAppUsageEvent> list) {
        if (list == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (GDAOAppUsageEvent gDAOAppUsageEvent : list) {
            if (gDAOAppUsageEvent != null) {
                arrayList.add(new AppUsageEvent(gDAOAppUsageEvent));
            }
        }
        return arrayList;
    }

    public static void createSessionEndEvent(DaoSession daoSession, String str, Date date) {
        GDAOAppUsageEvent gDAOAppUsageEvent = new GDAOAppUsageEvent();
        gDAOAppUsageEvent.setSession_id("" + str);
        gDAOAppUsageEvent.setDate(DateTimeHelpers.formatDateToServer(date));
        gDAOAppUsageEvent.setStartup(Boolean.FALSE);
        daoSession.getGDAOAppUsageEventDao().insert(gDAOAppUsageEvent);
    }

    public static void createSessionStartupEvent(DaoSession daoSession, String str, Date date) {
        GDAOAppUsageEvent gDAOAppUsageEvent = new GDAOAppUsageEvent();
        gDAOAppUsageEvent.setSession_id("" + str);
        gDAOAppUsageEvent.setDate(DateTimeHelpers.formatDateToServer(date));
        gDAOAppUsageEvent.setStartup(Boolean.TRUE);
        daoSession.getGDAOAppUsageEventDao().insert(gDAOAppUsageEvent);
    }

    public static void deleteAll(DaoSession daoSession) {
        daoSession.getGDAOAppUsageEventDao().deleteAll();
    }

    public static List<AppUsageEvent> getAll(DaoSession daoSession) {
        return convertList(daoSession.getGDAOAppUsageEventDao().loadAll());
    }

    public String getDate() {
        return this.mDbAppUsageEvent.getDate();
    }

    public long getId() {
        return this.mDbAppUsageEvent.getId().longValue();
    }

    public String getSessionId() {
        return this.mDbAppUsageEvent.getSession_id();
    }

    public boolean getStartup() {
        return this.mDbAppUsageEvent.getStartup().booleanValue();
    }
}
