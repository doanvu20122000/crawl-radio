package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao;

import java.io.Serializable;

/* loaded from: classes.dex */
public class GDAORadioCity implements Serializable {
    private Long city;
    private String frequency;

    /* renamed from: radio  reason: collision with root package name */
    private Long f535radio;

    public GDAORadioCity() {
    }

    public Long getCity() {
        return this.city;
    }

    public String getFrequency() {
        return this.frequency;
    }

    public Long getRadio() {
        return this.f535radio;
    }

    public void setCity(Long l) {
        this.city = l;
    }

    public void setFrequency(String str) {
        this.frequency = str;
    }

    public void setRadio(Long l) {
        this.f535radio = l;
    }

    public GDAORadioCity(Long l, Long l2, String str) {
        this.f535radio = l;
        this.city = l2;
        this.frequency = str;
    }
}
