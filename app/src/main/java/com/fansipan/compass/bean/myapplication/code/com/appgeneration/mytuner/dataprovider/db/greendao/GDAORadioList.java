package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao;

import java.io.Serializable;

/* loaded from: classes.dex */
public class GDAORadioList implements Serializable {
    private Long id;
    private String name;
    private Integer rank;

    public GDAORadioList() {
    }

    public Long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public Integer getRank() {
        return this.rank;
    }

    public void setId(Long l) {
        this.id = l;
    }

    public void setName(String str) {
        this.name = str;
    }

    public void setRank(Integer num) {
        this.rank = num;
    }

    public GDAORadioList(Long l) {
        this.id = l;
    }

    public GDAORadioList(Long l, String str, Integer num) {
        this.id = l;
        this.name = str;
        this.rank = num;
    }
}
