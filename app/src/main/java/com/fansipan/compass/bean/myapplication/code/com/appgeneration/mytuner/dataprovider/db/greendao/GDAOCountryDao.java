package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import com.fansipan.compass.bean.myapplication.code.com.greendao.AbstractDao;
import com.fansipan.compass.bean.myapplication.code.com.greendao.Property;
import com.fansipan.compass.bean.myapplication.code.com.greendao.database.Database;
import com.fansipan.compass.bean.myapplication.code.com.greendao.database.DatabaseStatement;
import com.fansipan.compass.bean.myapplication.code.com.greendao.internal.DaoConfig;

/* loaded from: classes.dex */
public class GDAOCountryDao extends AbstractDao<GDAOCountry, Long> {
    public static final String TABLENAME = "country";

    /* loaded from: classes.dex */
    public static class Properties {
        public static final Property Id = new Property(0, Long.class, "id", true, "ID");
        public static final Property Name = new Property(1, String.class, "name", false, "NAME");
        public static final Property Flag_url = new Property(2, String.class, "flag_url", false, "FLAG_URL");
        public static final Property Code = new Property(3, String.class, "code", false, "CODE");
        public static final Property Show_in_list = new Property(4, Boolean.class, "show", false, "SHOW_IN_LIST");
        public static final Property Use_state = new Property(5, Boolean.class, "use_states", false, "USE_STATE");
    }

    public GDAOCountryDao(DaoConfig daoConfig) {
        super(daoConfig);
    }

    public static void createTable(Database database, boolean z) {
        String str;
        if (z) {
            str = "IF NOT EXISTS ";
        } else {
            str = "";
        }
        database.execSQL("CREATE TABLE " + str + "\"country\" (\"ID\" INTEGER PRIMARY KEY ,\"NAME\" TEXT,\"FLAG_URL\" TEXT,\"CODE\" TEXT,\"SHOW_IN_LIST\" INTEGER,\"USE_STATES\" INTEGER);");
    }

    public static void dropTable(Database database, boolean z) {
        String str;
        StringBuilder m = new StringBuilder("DROP TABLE ");
        if (z) {
            str = "IF EXISTS ";
        } else {
            str = "";
        }
        m.append(str);
        m.append("\"country\"");
        database.execSQL(m.toString());
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final boolean isEntityUpdateable() {
        return true;
    }

    public GDAOCountryDao(DaoConfig daoConfig, DaoSession daoSession) {
        super(daoConfig, daoSession);
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public Long getKey(GDAOCountry gDAOCountry) {
        if (gDAOCountry != null) {
            return gDAOCountry.getId();
        }
        return null;
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public boolean hasKey(GDAOCountry gDAOCountry) {
        return gDAOCountry.getId() != null;
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public Long readKey(Cursor cursor, int i) {
        int i2 = i + 0;
        if (cursor.isNull(i2)) {
            return null;
        }
        return Long.valueOf(cursor.getLong(i2));
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final Long updateKeyAfterInsert(GDAOCountry gDAOCountry, long j) {
        gDAOCountry.setId(Long.valueOf(j));
        return Long.valueOf(j);
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final void bindValues(DatabaseStatement databaseStatement, GDAOCountry gDAOCountry) {
        databaseStatement.clearBindings();
        Long id = gDAOCountry.getId();
        if (id != null) {
            databaseStatement.bindLong(1, id.longValue());
        }
        String name = gDAOCountry.getName();
        if (name != null) {
            databaseStatement.bindString(2, name);
        }
        String flag_url = gDAOCountry.getFlag_url();
        if (flag_url != null) {
            databaseStatement.bindString(3, flag_url);
        }
        String code = gDAOCountry.getCode();
        if (code != null) {
            databaseStatement.bindString(4, code);
        }
        Boolean show_in_list = gDAOCountry.getShow_in_list();
        if (show_in_list != null) {
            databaseStatement.bindLong(5, show_in_list.booleanValue() ? 1L : 0L);
        }
        Boolean use_states = gDAOCountry.getUse_states();
        if (use_states != null) {
            databaseStatement.bindLong(6, use_states.booleanValue() ? 1L : 0L);
        }
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public GDAOCountry readEntity(Cursor cursor, int i) {
        Boolean valueOf;
        Boolean valueOf2;
        int i2 = i + 0;
        Long valueOf3 = cursor.isNull(i2) ? null : Long.valueOf(cursor.getLong(i2));
        int i3 = i + 1;
        String string = cursor.isNull(i3) ? null : cursor.getString(i3);
        int i4 = i + 2;
        String string2 = cursor.isNull(i4) ? null : cursor.getString(i4);
        int i5 = i + 3;
        String string3 = cursor.isNull(i5) ? null : cursor.getString(i5);
        int i6 = i + 4;
        if (cursor.isNull(i6)) {
            valueOf = null;
        } else {
            valueOf = Boolean.valueOf(cursor.getShort(i6) != 0);
        }
        int i7 = i + 5;
        if (cursor.isNull(i7)) {
            valueOf2 = null;
        } else {
            valueOf2 = Boolean.valueOf(cursor.getShort(i7) != 0);
        }
        return new GDAOCountry(valueOf3, string, string2, string3, valueOf, valueOf2);
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public void readEntity(Cursor cursor, GDAOCountry gDAOCountry, int i) {
        Boolean valueOf;
        int i2 = i + 0;
        Boolean bool = null;
        gDAOCountry.setId(cursor.isNull(i2) ? null : Long.valueOf(cursor.getLong(i2)));
        int i3 = i + 1;
        gDAOCountry.setName(cursor.isNull(i3) ? null : cursor.getString(i3));
        int i4 = i + 2;
        gDAOCountry.setFlag_url(cursor.isNull(i4) ? null : cursor.getString(i4));
        int i5 = i + 3;
        gDAOCountry.setCode(cursor.isNull(i5) ? null : cursor.getString(i5));
        int i6 = i + 4;
        if (cursor.isNull(i6)) {
            valueOf = null;
        } else {
            valueOf = Boolean.valueOf(cursor.getShort(i6) != 0);
        }
        gDAOCountry.setShow_in_list(valueOf);
        int i7 = i + 5;
        if (!cursor.isNull(i7)) {
            bool = Boolean.valueOf(cursor.getShort(i7) != 0);
        }
        gDAOCountry.setUse_states(bool);
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final void bindValues(SQLiteStatement sQLiteStatement, GDAOCountry gDAOCountry) {
        sQLiteStatement.clearBindings();
        Long id = gDAOCountry.getId();
        if (id != null) {
            sQLiteStatement.bindLong(1, id.longValue());
        }
        String name = gDAOCountry.getName();
        if (name != null) {
            sQLiteStatement.bindString(2, name);
        }
        String flag_url = gDAOCountry.getFlag_url();
        if (flag_url != null) {
            sQLiteStatement.bindString(3, flag_url);
        }
        String code = gDAOCountry.getCode();
        if (code != null) {
            sQLiteStatement.bindString(4, code);
        }
        Boolean show_in_list = gDAOCountry.getShow_in_list();
        if (show_in_list != null) {
            sQLiteStatement.bindLong(5, show_in_list.booleanValue() ? 1L : 0L);
        }
        Boolean use_states = gDAOCountry.getUse_states();
        if (use_states != null) {
            sQLiteStatement.bindLong(6, use_states.booleanValue() ? 1L : 0L);
        }
    }
}
