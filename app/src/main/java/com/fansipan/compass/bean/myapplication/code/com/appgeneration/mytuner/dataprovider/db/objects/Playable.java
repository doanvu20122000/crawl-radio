package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.objects;

import com.appgeneration.mytuner.dataprovider.helpers.DateTimeHelpers;

import java.util.Date;
import java.util.List;

/* loaded from: classes.dex */
public abstract class Playable implements NavigationEntityItem, UserSelectable {
    private static final long serialVersionUID = -1589094981643071885L;
    private Date mEndDate;
    private Date mPlayDate;
    private Date mStartDate;

    public boolean equals(Object obj) {
        if (!(obj instanceof Playable)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        Playable playable = (Playable) obj;
        if (!getClass().equals(playable.getClass()) || getObjectId() != playable.getObjectId()) {
            return false;
        }
        return true;
    }

    public Date getEndDate() {
        return this.mEndDate;
    }

    public Date getPlayDate() {
        return this.mPlayDate;
    }

    public Date getStartDate() {
        return this.mStartDate;
    }

    public abstract List<StreamWrapper> getUrls();

    public abstract boolean isSeekable();

    public void setEndDateToNow() {
        if (this.mEndDate == null) {
            this.mEndDate = DateTimeHelpers.getCurrentDate();
        }
    }

    public void setPlayDateToNow() {
        if (this.mPlayDate == null) {
            this.mPlayDate = DateTimeHelpers.getCurrentDate();
        }
    }

    public void setStartDateToNow() {
        if (this.mStartDate == null) {
            this.mStartDate = DateTimeHelpers.getCurrentDate();
        }
    }

    public String toString() {
        return getClass().getSimpleName() + " - " + getObjectId() + " (" + getTitle() + ")";
    }
}
