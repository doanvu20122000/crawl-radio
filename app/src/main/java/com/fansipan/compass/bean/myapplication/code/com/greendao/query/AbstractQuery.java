package com.fansipan.compass.bean.myapplication.code.com.greendao.query;

import com.fansipan.compass.bean.myapplication.code.com.greendao.AbstractDao;
import com.fansipan.compass.bean.myapplication.code.com.greendao.DaoException;

/* loaded from: classes4.dex */
public abstract class AbstractQuery<T> {
    public final AbstractDao<T, ?> dao;
    public final Thread ownerThread = Thread.currentThread();
    public String[] parameters;
    public String sql;

    public AbstractQuery(AbstractDao<T, ?> abstractDao, String str, String[] strArr) {
        this.dao = abstractDao;
        this.sql = str;
        this.parameters = strArr;
    }

    public static String[] toStringArray(Object[] objArr) {
        int length = objArr.length;
        String[] strArr = new String[length];
        for (int i = 0; i < length; i++) {
            Object obj = objArr[i];
            if (obj != null) {
                strArr[i] = obj.toString();
            } else {
                strArr[i] = null;
            }
        }
        return strArr;
    }

    public final void checkThread() {
        if (Thread.currentThread() == this.ownerThread) {
            return;
        }
        throw new DaoException("Method may be called only in owner thread, use forCurrentThread to get an instance for this thread");
    }
}
