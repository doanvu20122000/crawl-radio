package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import com.fansipan.compass.bean.myapplication.code.com.greendao.AbstractDao;
import com.fansipan.compass.bean.myapplication.code.com.greendao.Property;
import com.fansipan.compass.bean.myapplication.code.com.greendao.database.Database;
import com.fansipan.compass.bean.myapplication.code.com.greendao.database.DatabaseStatement;
import com.fansipan.compass.bean.myapplication.code.com.greendao.internal.DaoConfig;
import com.fansipan.compass.bean.myapplication.code.com.greendao.internal.SqlUtils;

import java.util.ArrayList;
import java.util.List;

/* loaded from: classes.dex */
public class GDAORadioDao extends AbstractDao<GDAORadio, Long> {
    public static final String TABLENAME = "radio";
    private DaoSession daoSession;
    private String selectDeep;

    /* loaded from: classes.dex */
    public static class Properties {
        public static final Property Id = new Property(0, Long.class, "id", true, "ID");
        public static final Property Ord = new Property(1, Long.class, "ord", false, "ORD");
        public static final Property Name = new Property(2, String.class, "name", false, "NAME");
        public static final Property Image_url = new Property(3, String.class, "image_url", false, "IMAGE_URL");
        public static final Property Hidden = new Property(4, Boolean.class, "hidden", false, "HIDDEN");
        public static final Property Has_metadata = new Property(5, Boolean.class, "has_metadata", false, "HAS_METADATA");
        public static final Property Ignore_metadata = new Property(6, Boolean.class, "ignore_metadata", false, "IGNORE_METADATA");
        public static final Property Geolocation_codes = new Property(7, String.class, "geolocation_codes", false, "GEOLOCATION_CODES");
        public static final Property Status = new Property(8, String.class, "status", false, "STATUS");
        public static final Property Country = new Property(9, Long.class, "country", false, "COUNTRY");
        public static final List<String> columnName(){
            ArrayList<String> data = new ArrayList();
            data.add(Id.columnName);
            data.add(Ord.columnName);
            data.add(Name.columnName);
            data.add(Image_url.columnName);
            data.add(Hidden.columnName);
            data.add(Has_metadata.columnName);
            data.add(Ignore_metadata.columnName);
            data.add(Geolocation_codes.columnName);
            data.add(Status.columnName);
            data.add(Country.columnName);
            return data;
        }
    }

    public GDAORadioDao(DaoConfig daoConfig) {
        super(daoConfig);
    }

    public static void createTable(Database database, boolean z) {
        String str;
        if (z) {
            str = "IF NOT EXISTS ";
        } else {
            str = "";
        }
        database.execSQL("CREATE TABLE " + str + "\"radio\" (\"ID\" INTEGER PRIMARY KEY ,\"ORD\" INTEGER,\"NAME\" TEXT,\"IMAGE_URL\" TEXT,\"HIDDEN\" INTEGER,\"HAS_METADATA\" INTEGER,\"IGNORE_METADATA\" INTEGER,\"GEOLOCATION_CODES\" TEXT,\"STATUS\" TEXT,\"COUNTRY\" INTEGER);");
    }

    public static void dropTable(Database database, boolean z) {
        String str;
        StringBuilder m = new StringBuilder("DROP TABLE ");
        if (z) {
            str = "IF EXISTS ";
        } else {
            str = "";
        }
        m.append(str);
        m.append("\"radio\"");
        database.execSQL(m.toString());
    }

    public String getSelectDeep() {
        if (this.selectDeep == null) {
            StringBuilder sb = new StringBuilder("SELECT ");
//            SqlUtils.appendColumns(sb, RequestConfiguration.MAX_AD_CONTENT_RATING_T, getAllColumns());
            sb.append(',');
            SqlUtils.appendColumns(sb, "T0", this.daoSession.getGDAOCountryDao().getAllColumns());
            sb.append(" FROM radio T");
            sb.append(" LEFT JOIN country T0 ON T.\"COUNTRY\"=T0.\"ID\"");
            sb.append(' ');
            this.selectDeep = sb.toString();
        }
        return this.selectDeep;
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final boolean isEntityUpdateable() {
        return true;
    }

    public GDAORadio loadCurrentDeep(Cursor cursor, boolean z) {
        GDAORadio loadCurrent = loadCurrent(cursor, 0, z);
        loadCurrent.setCountryObject((GDAOCountry) loadCurrentOther(this.daoSession.getGDAOCountryDao(), cursor, getAllColumns().length));
        return loadCurrent;
    }

    public GDAORadioDao(DaoConfig daoConfig, DaoSession daoSession) {
        super(daoConfig, daoSession);
        this.daoSession = daoSession;
    }

    @Override
    public final void attachEntity(GDAORadio gDAORadio) {
        gDAORadio.__setDaoSession(this.daoSession);
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public Long getKey(GDAORadio gDAORadio) {
        if (gDAORadio != null) {
            return gDAORadio.getId();
        }
        return null;
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public boolean hasKey(GDAORadio gDAORadio) {
        return gDAORadio.getId() != null;
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public Long readKey(Cursor cursor, int i) {
        int i2 = i + 0;
        if (cursor.isNull(i2)) {
            return null;
        }
        return Long.valueOf(cursor.getLong(i2));
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final Long updateKeyAfterInsert(GDAORadio gDAORadio, long j) {
        gDAORadio.setId(Long.valueOf(j));
        return Long.valueOf(j);
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final void bindValues(DatabaseStatement databaseStatement, GDAORadio gDAORadio) {
        databaseStatement.clearBindings();
        Long id = gDAORadio.getId();
        if (id != null) {
            databaseStatement.bindLong(1, id.longValue());
        }
        Long ord = gDAORadio.getOrd();
        if (ord != null) {
            databaseStatement.bindLong(2, ord.longValue());
        }
        String name = gDAORadio.getName();
        if (name != null) {
            databaseStatement.bindString(3, name);
        }
        String image_url = gDAORadio.getImage_url();
        if (image_url != null) {
            databaseStatement.bindString(4, image_url);
        }
        Boolean hidden = gDAORadio.getHidden();
        if (hidden != null) {
            databaseStatement.bindLong(5, hidden.booleanValue() ? 1L : 0L);
        }
        Boolean has_metadata = gDAORadio.getHas_metadata();
        if (has_metadata != null) {
            databaseStatement.bindLong(6, has_metadata.booleanValue() ? 1L : 0L);
        }
        Boolean ignore_metadata = gDAORadio.getIgnore_metadata();
        if (ignore_metadata != null) {
            databaseStatement.bindLong(7, ignore_metadata.booleanValue() ? 1L : 0L);
        }
        String geolocation_codes = gDAORadio.getGeolocation_codes();
        if (geolocation_codes != null) {
            databaseStatement.bindString(8, geolocation_codes);
        }
        String status = gDAORadio.getStatus();
        if (status != null) {
            databaseStatement.bindString(9, status);
        }
        Long country = gDAORadio.getCountry();
        if (country != null) {
            databaseStatement.bindLong(10, country.longValue());
        }
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public GDAORadio readEntity(Cursor cursor, int i) {
        Boolean valueOf;
        Boolean valueOf2;
        Boolean valueOf3;
        int i2 = i + 0;
        Long valueOf4 = cursor.isNull(i2) ? null : Long.valueOf(cursor.getLong(i2));
        int i3 = i + 1;
        Long valueOf5 = cursor.isNull(i3) ? null : Long.valueOf(cursor.getLong(i3));
        int i4 = i + 2;
        String string = cursor.isNull(i4) ? null : cursor.getString(i4);
        int i5 = i + 3;
        String string2 = cursor.isNull(i5) ? null : cursor.getString(i5);
        int i6 = i + 4;
        if (cursor.isNull(i6)) {
            valueOf = null;
        } else {
            valueOf = Boolean.valueOf(cursor.getShort(i6) != 0);
        }
        int i7 = i + 5;
        if (cursor.isNull(i7)) {
            valueOf2 = null;
        } else {
            valueOf2 = Boolean.valueOf(cursor.getShort(i7) != 0);
        }
        int i8 = i + 6;
        if (cursor.isNull(i8)) {
            valueOf3 = null;
        } else {
            valueOf3 = Boolean.valueOf(cursor.getShort(i8) != 0);
        }
        int i9 = i + 7;
        String string3 = cursor.isNull(i9) ? null : cursor.getString(i9);
        int i10 = i + 8;
        int i11 = i + 9;
        return new GDAORadio(valueOf4, valueOf5, string, string2, valueOf, valueOf2, valueOf3, string3, cursor.isNull(i10) ? null : cursor.getString(i10), cursor.isNull(i11) ? null : Long.valueOf(cursor.getLong(i11)));
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public void readEntity(Cursor cursor, GDAORadio gDAORadio, int i) {
        Boolean valueOf;
        Boolean valueOf2;
        Boolean valueOf3;
        int i2 = i + 0;
        gDAORadio.setId(cursor.isNull(i2) ? null : Long.valueOf(cursor.getLong(i2)));
        int i3 = i + 1;
        gDAORadio.setOrd(cursor.isNull(i3) ? null : Long.valueOf(cursor.getLong(i3)));
        int i4 = i + 2;
        gDAORadio.setName(cursor.isNull(i4) ? null : cursor.getString(i4));
        int i5 = i + 3;
        gDAORadio.setImage_url(cursor.isNull(i5) ? null : cursor.getString(i5));
        int i6 = i + 4;
        if (cursor.isNull(i6)) {
            valueOf = null;
        } else {
            valueOf = Boolean.valueOf(cursor.getShort(i6) != 0);
        }
        gDAORadio.setHidden(valueOf);
        int i7 = i + 5;
        if (cursor.isNull(i7)) {
            valueOf2 = null;
        } else {
            valueOf2 = Boolean.valueOf(cursor.getShort(i7) != 0);
        }
        gDAORadio.setHas_metadata(valueOf2);
        int i8 = i + 6;
        if (cursor.isNull(i8)) {
            valueOf3 = null;
        } else {
            valueOf3 = Boolean.valueOf(cursor.getShort(i8) != 0);
        }
        gDAORadio.setIgnore_metadata(valueOf3);
        int i9 = i + 7;
        gDAORadio.setGeolocation_codes(cursor.isNull(i9) ? null : cursor.getString(i9));
        int i10 = i + 8;
        gDAORadio.setStatus(cursor.isNull(i10) ? null : cursor.getString(i10));
        int i11 = i + 9;
        gDAORadio.setCountry(cursor.isNull(i11) ? null : Long.valueOf(cursor.getLong(i11)));
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final void bindValues(SQLiteStatement sQLiteStatement, GDAORadio gDAORadio) {
        sQLiteStatement.clearBindings();
        Long id = gDAORadio.getId();
        if (id != null) {
            sQLiteStatement.bindLong(1, id.longValue());
        }
        Long ord = gDAORadio.getOrd();
        if (ord != null) {
            sQLiteStatement.bindLong(2, ord.longValue());
        }
        String name = gDAORadio.getName();
        if (name != null) {
            sQLiteStatement.bindString(3, name);
        }
        String image_url = gDAORadio.getImage_url();
        if (image_url != null) {
            sQLiteStatement.bindString(4, image_url);
        }
        Boolean hidden = gDAORadio.getHidden();
        if (hidden != null) {
            sQLiteStatement.bindLong(5, hidden.booleanValue() ? 1L : 0L);
        }
        Boolean has_metadata = gDAORadio.getHas_metadata();
        if (has_metadata != null) {
            sQLiteStatement.bindLong(6, has_metadata.booleanValue() ? 1L : 0L);
        }
        Boolean ignore_metadata = gDAORadio.getIgnore_metadata();
        if (ignore_metadata != null) {
            sQLiteStatement.bindLong(7, ignore_metadata.booleanValue() ? 1L : 0L);
        }
        String geolocation_codes = gDAORadio.getGeolocation_codes();
        if (geolocation_codes != null) {
            sQLiteStatement.bindString(8, geolocation_codes);
        }
        String status = gDAORadio.getStatus();
        if (status != null) {
            sQLiteStatement.bindString(9, status);
        }
        Long country = gDAORadio.getCountry();
        if (country != null) {
            sQLiteStatement.bindLong(10, country.longValue());
        }
    }
}
