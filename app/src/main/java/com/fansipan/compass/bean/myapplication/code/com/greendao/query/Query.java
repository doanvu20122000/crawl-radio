package com.fansipan.compass.bean.myapplication.code.com.greendao.query;

import com.fansipan.compass.bean.myapplication.code.com.greendao.AbstractDao;

/* loaded from: classes4.dex */
public final class Query<T> extends AbstractQueryWithLimit<T> {
    public final QueryData<T> queryData;

    /* loaded from: classes4.dex */
    public static final class QueryData<T2> extends AbstractQueryData<T2, Query<T2>> {
        public final int limitPosition;
        public final int offsetPosition;

        public QueryData(AbstractDao abstractDao, String str, String[] strArr, int i) {
            super(abstractDao, str, strArr);
            this.limitPosition = i;
            this.offsetPosition = -1;
        }

        @Override // org.greenrobot.greendao.query.AbstractQueryData
        public final Query<T2> createQuery() {
            return new Query(this, this.dao, this.sql, (String[]) this.initialValues.clone(), this.limitPosition, this.offsetPosition);
        }
    }

    public Query(QueryData queryData, AbstractDao abstractDao, String str, String[] strArr, int i, int i2) {
        super(abstractDao, str, strArr, i, i2);
        this.queryData = queryData;
    }

    public final void setParameter(Object obj) {
        if (this.limitPosition != 0 && this.offsetPosition != 0) {
            checkThread();
            if (obj != null) {
                parameters = new String[1];
                this.parameters[0] = obj.toString();
                return;
            } else {
                this.parameters[0] = null;
                return;
            }
        }
        throw new IllegalArgumentException("Illegal parameter index: 0");
    }
}
