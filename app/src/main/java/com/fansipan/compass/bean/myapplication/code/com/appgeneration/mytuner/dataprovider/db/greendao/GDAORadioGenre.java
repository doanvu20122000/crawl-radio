package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao;

import java.io.Serializable;

/* loaded from: classes.dex */
public class GDAORadioGenre implements Serializable {
    private Long genre;

    /* renamed from: radio  reason: collision with root package name */
    private Long f536radio;

    public GDAORadioGenre() {
    }

    public Long getGenre() {
        return this.genre;
    }

    public Long getRadio() {
        return this.f536radio;
    }

    public void setGenre(Long l) {
        this.genre = l;
    }

    public void setRadio(Long l) {
        this.f536radio = l;
    }

    public GDAORadioGenre(Long l, Long l2) {
        this.f536radio = l;
        this.genre = l2;
    }
}
