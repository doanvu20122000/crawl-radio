package com.fansipan.compass.bean.myapplication.code.com.greendao.internal;

import java.lang.ref.WeakReference;

/* loaded from: classes4.dex */
public final class LongHashMap<T> {
    public int size;
    public int capacity = 16;
    public int threshold = 21;
    public Entry<T>[] table = new Entry[16];

    /* loaded from: classes4.dex */
    public static final class Entry<T> {
        public final long key;
        public Entry<T> next;
        public T value;

        /* JADX WARN: Multi-variable type inference failed */
        public Entry(long j, WeakReference weakReference, Entry entry) {
            this.key = j;
            this.value = (T) weakReference;
            this.next = entry;
        }
    }

    public final T get(long j) {
        for (Entry<T> entry = this.table[((((int) j) ^ ((int) (j >>> 32))) & Integer.MAX_VALUE) % this.capacity]; entry != null; entry = entry.next) {
            if (entry.key == j) {
                return entry.value;
            }
        }
        return null;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public final void put(long j, WeakReference weakReference) {
        int i = ((((int) j) ^ ((int) (j >>> 32))) & Integer.MAX_VALUE) % this.capacity;
        Entry<T> entry = this.table[i];
        for (Entry<T> entry2 = entry; entry2 != null; entry2 = entry2.next) {
            if (entry2.key == j) {
                entry2.value = (T) weakReference;
                return;
            }
        }
        this.table[i] = new Entry<>(j, weakReference, entry);
        int i2 = this.size + 1;
        this.size = i2;
        if (i2 > this.threshold) {
            setCapacity(this.capacity * 2);
        }
    }

    public final void remove(long j) {
        int i = ((((int) j) ^ ((int) (j >>> 32))) & Integer.MAX_VALUE) % this.capacity;
        Entry<T> entry = this.table[i];
        Entry<T> entry2 = null;
        while (entry != null) {
            Entry<T> entry3 = entry.next;
            if (entry.key == j) {
                if (entry2 == null) {
                    this.table[i] = entry3;
                } else {
                    entry2.next = entry3;
                }
                this.size--;
                return;
            }
            entry2 = entry;
            entry = entry3;
        }
    }

    public final void setCapacity(int i) {
        Entry<T>[] entryArr = new Entry[i];
        int length = this.table.length;
        for (int i2 = 0; i2 < length; i2++) {
            Entry<T> entry = this.table[i2];
            while (entry != null) {
                long j = entry.key;
                int i3 = ((((int) (j >>> 32)) ^ ((int) j)) & Integer.MAX_VALUE) % i;
                Entry<T> entry2 = entry.next;
                entry.next = entryArr[i3];
                entryArr[i3] = entry;
                entry = entry2;
            }
        }
        this.table = entryArr;
        this.capacity = i;
        this.threshold = (i * 4) / 3;
    }
}
