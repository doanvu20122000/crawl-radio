package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import com.fansipan.compass.bean.myapplication.code.com.POBRequest$AdPosition$EnumUnboxingLocalUtility;
import com.fansipan.compass.bean.myapplication.code.com.greendao.AbstractDao;
import com.fansipan.compass.bean.myapplication.code.com.greendao.Property;
import com.fansipan.compass.bean.myapplication.code.com.greendao.database.Database;
import com.fansipan.compass.bean.myapplication.code.com.greendao.database.DatabaseStatement;
import com.fansipan.compass.bean.myapplication.code.com.greendao.internal.DaoConfig;

/* loaded from: classes.dex */
public class GDAORadioListDao extends AbstractDao<GDAORadioList, Long> {
    public static final String TABLENAME = "radio_list";

    /* loaded from: classes.dex */
    public static class Properties {
        public static final Property Id = new Property(0, Long.class, "id", true, "ID");
        public static final Property Name = new Property(1, String.class, "name", false, "NAME");
        public static final Property Rank = new Property(2, Integer.class, "rank", false, "RANK");
    }

    public GDAORadioListDao(DaoConfig daoConfig) {
        super(daoConfig);
    }

    public static void createTable(Database database, boolean z) {
        String str;
        if (z) {
            str = "IF NOT EXISTS ";
        } else {
            str = "";
        }
        database.execSQL("CREATE TABLE " + str + "\"radio_list\" (\"ID\" INTEGER PRIMARY KEY ,\"NAME\" TEXT,\"RANK\" INTEGER);");
    }

    public static void dropTable(Database database, boolean z) {
        String str;
        StringBuilder m = new StringBuilder("DROP TABLE ");
        if (z) {
            str = "IF EXISTS ";
        } else {
            str = "";
        }
        POBRequest$AdPosition$EnumUnboxingLocalUtility.m(m, str, "\"radio_list\"", database);
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final boolean isEntityUpdateable() {
        return true;
    }

    public GDAORadioListDao(DaoConfig daoConfig, DaoSession daoSession) {
        super(daoConfig, daoSession);
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public Long getKey(GDAORadioList gDAORadioList) {
        if (gDAORadioList != null) {
            return gDAORadioList.getId();
        }
        return null;
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public boolean hasKey(GDAORadioList gDAORadioList) {
        return gDAORadioList.getId() != null;
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public Long readKey(Cursor cursor, int i) {
        int i2 = i + 0;
        if (cursor.isNull(i2)) {
            return null;
        }
        return Long.valueOf(cursor.getLong(i2));
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final Long updateKeyAfterInsert(GDAORadioList gDAORadioList, long j) {
        gDAORadioList.setId(Long.valueOf(j));
        return Long.valueOf(j);
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final void bindValues(DatabaseStatement databaseStatement, GDAORadioList gDAORadioList) {
        databaseStatement.clearBindings();
        Long id = gDAORadioList.getId();
        if (id != null) {
            databaseStatement.bindLong(1, id.longValue());
        }
        String name = gDAORadioList.getName();
        if (name != null) {
            databaseStatement.bindString(2, name);
        }
        Integer rank = gDAORadioList.getRank();
        if (rank != null) {
            databaseStatement.bindLong(3, rank.intValue());
        }
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public GDAORadioList readEntity(Cursor cursor, int i) {
        int i2 = i + 0;
        int i3 = i + 1;
        int i4 = i + 2;
        return new GDAORadioList(cursor.isNull(i2) ? null : Long.valueOf(cursor.getLong(i2)), cursor.isNull(i3) ? null : cursor.getString(i3), cursor.isNull(i4) ? null : Integer.valueOf(cursor.getInt(i4)));
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public void readEntity(Cursor cursor, GDAORadioList gDAORadioList, int i) {
        int i2 = i + 0;
        gDAORadioList.setId(cursor.isNull(i2) ? null : Long.valueOf(cursor.getLong(i2)));
        int i3 = i + 1;
        gDAORadioList.setName(cursor.isNull(i3) ? null : cursor.getString(i3));
        int i4 = i + 2;
        gDAORadioList.setRank(cursor.isNull(i4) ? null : Integer.valueOf(cursor.getInt(i4)));
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final void bindValues(SQLiteStatement sQLiteStatement, GDAORadioList gDAORadioList) {
        sQLiteStatement.clearBindings();
        Long id = gDAORadioList.getId();
        if (id != null) {
            sQLiteStatement.bindLong(1, id.longValue());
        }
        String name = gDAORadioList.getName();
        if (name != null) {
            sQLiteStatement.bindString(2, name);
        }
        Integer rank = gDAORadioList.getRank();
        if (rank != null) {
            sQLiteStatement.bindLong(3, rank.intValue());
        }
    }
}
