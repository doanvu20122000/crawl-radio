package com.fansipan.compass.bean.myapplication.code.com.greendao;

import android.database.SQLException;
import android.util.Log;

/* loaded from: classes4.dex */
public class DaoException extends SQLException {
    public DaoException() {
    }

    public DaoException(String str) {
        super(str);
    }

    public DaoException(String str, Exception exc) {
        super(str);
        try {
            initCause(exc);
        } catch (Throwable th) {
            Log.e("greenDAO", "Could not set initial cause", th);
            Log.e("greenDAO", "Initial cause is:", exc);
        }
    }

    public DaoException(Exception exc) {
        try {
            initCause(exc);
        } catch (Throwable th) {
            Log.e("greenDAO", "Could not set initial cause", th);
            Log.e("greenDAO", "Initial cause is:", exc);
        }
    }
}
