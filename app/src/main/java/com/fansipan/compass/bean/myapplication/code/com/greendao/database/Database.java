package com.fansipan.compass.bean.myapplication.code.com.greendao.database;

import android.database.Cursor;
import android.database.SQLException;

/* loaded from: classes4.dex */
public interface Database {
    void beginTransaction();

    DatabaseStatement compileStatement(String str);

    void endTransaction();

    void execSQL(String str) throws SQLException;

    void execSQL(String str, Object[] objArr) throws SQLException;

    Object getRawDatabase();

    boolean isDbLockedByCurrentThread();

    boolean isOpen();

    Cursor rawQuery(String str, String[] strArr);

    void setTransactionSuccessful();
}
