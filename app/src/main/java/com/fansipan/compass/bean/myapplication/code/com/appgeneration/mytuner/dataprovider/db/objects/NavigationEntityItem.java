package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.objects;

import java.io.Serializable;

/* loaded from: classes.dex */
public interface NavigationEntityItem extends Serializable {
    long getObjectId();
}
