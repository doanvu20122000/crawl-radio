package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import com.fansipan.compass.bean.myapplication.code.com.POBRequest$AdPosition$EnumUnboxingLocalUtility;
import com.fansipan.compass.bean.myapplication.code.com.greendao.AbstractDao;
import com.fansipan.compass.bean.myapplication.code.com.greendao.Property;
import com.fansipan.compass.bean.myapplication.code.com.greendao.database.Database;
import com.fansipan.compass.bean.myapplication.code.com.greendao.database.DatabaseStatement;
import com.fansipan.compass.bean.myapplication.code.com.greendao.internal.DaoConfig;

/* loaded from: classes.dex */
public class GDAOUserSelectedEntityDao extends AbstractDao<GDAOUserSelectedEntity, Void> {
    public static final String TABLENAME = "user_selected_entities";

    /* loaded from: classes.dex */
    public static class Properties {
        public static final Property Id = new Property(0, Long.class, "id", false, "ID");
        public static final Property Type = new Property(1, Integer.class, "type", false, "TYPE");
        public static final Property Subtype = new Property(2, Integer.class, "subtype", false, "SUBTYPE");
        public static final Property Timestamp = new Property(3, Long.class, "timestamp", false, "TIMESTAMP");
        public static final Property N_ord = new Property(4, Integer.class, "n_ord", false, "N_ORD");
    }

    public GDAOUserSelectedEntityDao(DaoConfig daoConfig) {
        super(daoConfig);
    }

    public static void createTable(Database database, boolean z) {
        String str;
        if (z) {
            str = "IF NOT EXISTS ";
        } else {
            str = "";
        }
        database.execSQL("CREATE TABLE " + str + "\"user_selected_entities\" (\"ID\" INTEGER,\"TYPE\" INTEGER,\"SUBTYPE\" INTEGER,\"TIMESTAMP\" INTEGER,\"N_ORD\" INTEGER);");
    }

    public static void dropTable(Database database, boolean z) {
        String str;
        StringBuilder m = new StringBuilder("DROP TABLE ");
        if (z) {
            str = "IF EXISTS ";
        } else {
            str = "";
        }
        POBRequest$AdPosition$EnumUnboxingLocalUtility.m(m, str, "\"user_selected_entities\"", database);
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public Void getKey(GDAOUserSelectedEntity gDAOUserSelectedEntity) {
        return null;
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public boolean hasKey(GDAOUserSelectedEntity gDAOUserSelectedEntity) {
        return false;
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final boolean isEntityUpdateable() {
        return true;
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public Void readKey(Cursor cursor, int i) {
        return null;
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final Void updateKeyAfterInsert(GDAOUserSelectedEntity gDAOUserSelectedEntity, long j) {
        return null;
    }

    public GDAOUserSelectedEntityDao(DaoConfig daoConfig, DaoSession daoSession) {
        super(daoConfig, daoSession);
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final void bindValues(DatabaseStatement databaseStatement, GDAOUserSelectedEntity gDAOUserSelectedEntity) {
        databaseStatement.clearBindings();
        Long id = gDAOUserSelectedEntity.getId();
        if (id != null) {
            databaseStatement.bindLong(1, id.longValue());
        }
        Integer type = gDAOUserSelectedEntity.getType();
        if (type != null) {
            databaseStatement.bindLong(2, type.intValue());
        }
        Integer subtype = gDAOUserSelectedEntity.getSubtype();
        if (subtype != null) {
            databaseStatement.bindLong(3, subtype.intValue());
        }
        Long timestamp = gDAOUserSelectedEntity.getTimestamp();
        if (timestamp != null) {
            databaseStatement.bindLong(4, timestamp.longValue());
        }
        Integer n_ord = gDAOUserSelectedEntity.getN_ord();
        if (n_ord != null) {
            databaseStatement.bindLong(5, n_ord.intValue());
        }
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public GDAOUserSelectedEntity readEntity(Cursor cursor, int i) {
        int i2 = i + 0;
        Long valueOf = cursor.isNull(i2) ? null : Long.valueOf(cursor.getLong(i2));
        int i3 = i + 1;
        Integer valueOf2 = cursor.isNull(i3) ? null : Integer.valueOf(cursor.getInt(i3));
        int i4 = i + 2;
        Integer valueOf3 = cursor.isNull(i4) ? null : Integer.valueOf(cursor.getInt(i4));
        int i5 = i + 3;
        int i6 = i + 4;
        return new GDAOUserSelectedEntity(valueOf, valueOf2, valueOf3, cursor.isNull(i5) ? null : Long.valueOf(cursor.getLong(i5)), cursor.isNull(i6) ? null : Integer.valueOf(cursor.getInt(i6)));
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public void readEntity(Cursor cursor, GDAOUserSelectedEntity gDAOUserSelectedEntity, int i) {
        int i2 = i + 0;
        gDAOUserSelectedEntity.setId(cursor.isNull(i2) ? null : Long.valueOf(cursor.getLong(i2)));
        int i3 = i + 1;
        gDAOUserSelectedEntity.setType(cursor.isNull(i3) ? null : Integer.valueOf(cursor.getInt(i3)));
        int i4 = i + 2;
        gDAOUserSelectedEntity.setSubtype(cursor.isNull(i4) ? null : Integer.valueOf(cursor.getInt(i4)));
        int i5 = i + 3;
        gDAOUserSelectedEntity.setTimestamp(cursor.isNull(i5) ? null : Long.valueOf(cursor.getLong(i5)));
        int i6 = i + 4;
        gDAOUserSelectedEntity.setN_ord(cursor.isNull(i6) ? null : Integer.valueOf(cursor.getInt(i6)));
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final void bindValues(SQLiteStatement sQLiteStatement, GDAOUserSelectedEntity gDAOUserSelectedEntity) {
        sQLiteStatement.clearBindings();
        Long id = gDAOUserSelectedEntity.getId();
        if (id != null) {
            sQLiteStatement.bindLong(1, id.longValue());
        }
        Integer type = gDAOUserSelectedEntity.getType();
        if (type != null) {
            sQLiteStatement.bindLong(2, type.intValue());
        }
        Integer subtype = gDAOUserSelectedEntity.getSubtype();
        if (subtype != null) {
            sQLiteStatement.bindLong(3, subtype.intValue());
        }
        Long timestamp = gDAOUserSelectedEntity.getTimestamp();
        if (timestamp != null) {
            sQLiteStatement.bindLong(4, timestamp.longValue());
        }
        Integer n_ord = gDAOUserSelectedEntity.getN_ord();
        if (n_ord != null) {
            sQLiteStatement.bindLong(5, n_ord.intValue());
        }
    }
}
