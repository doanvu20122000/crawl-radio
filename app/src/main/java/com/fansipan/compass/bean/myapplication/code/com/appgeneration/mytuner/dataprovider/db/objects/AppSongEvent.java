package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.objects;

import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao.DaoSession;
import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao.GDAOAppSongEvent;

import java.util.ArrayList;
import java.util.List;

import kotlin.jvm.internal.DefaultConstructorMarker;

/* compiled from: AppSongEvent.kt */
/* loaded from: classes.dex */
public final class AppSongEvent {
    public static final Companion Companion = new Companion(null);
    private final GDAOAppSongEvent event;

    /* compiled from: AppSongEvent.kt */
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        public final void deleteAll(DaoSession daoSession) {
            daoSession.getGDAOAppSongEventDao().deleteAll();
        }

        public final List<AppSongEvent> getAll(DaoSession daoSession) {
            List<GDAOAppSongEvent> loadAll = daoSession.getGDAOAppSongEventDao().loadAll();
            if (loadAll != null) {
                ArrayList arrayList = new ArrayList();
                for (GDAOAppSongEvent gDAOAppSongEvent : loadAll) {
                    arrayList.add(new AppSongEvent(gDAOAppSongEvent));
                }
                return arrayList;
            }
            return null;
        }
    }

    public AppSongEvent(GDAOAppSongEvent gDAOAppSongEvent) {
        this.event = gDAOAppSongEvent;
    }

    public static final void deleteAll(DaoSession daoSession) {
        Companion.deleteAll(daoSession);
    }

    public static final List<AppSongEvent> getAll(DaoSession daoSession) {
        return Companion.getAll(daoSession);
    }

    public final String getEndDate() {
        return this.event.getEnd_date();
    }

    public final long getId() {
        return this.event.getId().longValue();
    }

    public final String getMetadata() {
        return this.event.getMetadata();
    }

    public final long getRadio() {
        return this.event.getRadio().longValue();
    }

    public final long getSong() {
        return this.event.getSong().longValue();
    }

    public final String getStartDate() {
        return this.event.getStart_date();
    }

    public final boolean getWasZapping() {
        return this.event.getWas_zapping().booleanValue();
    }

    public final boolean hasIncreasedVolume() {
        return this.event.getIncreased_volume().booleanValue();
    }
}
