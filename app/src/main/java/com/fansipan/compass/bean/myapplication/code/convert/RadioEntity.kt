package com.fansipan.compass.bean.myapplication.code.convert

data class RadioEntity(
    val country: Int,
    val geolocation_codes: String,
    val has_metadata: Int,
    val hidden: Int,
    val id: Int,
    val ignore_metadata: Int,
    val image_url: String,
    val name: String,
    val ord: Int,
    val player_webpage: String,
    val status: String,
    val universal_radio: Int,
    val use_external_player: Int
)