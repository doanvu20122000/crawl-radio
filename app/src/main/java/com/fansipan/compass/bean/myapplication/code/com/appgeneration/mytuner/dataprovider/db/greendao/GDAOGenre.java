package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao;

import java.io.Serializable;

/* loaded from: classes.dex */
public class GDAOGenre implements Serializable {
    private Long id;
    private String name;

    public GDAOGenre() {
    }

    public Long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public void setId(Long l) {
        this.id = l;
    }

    public void setName(String str) {
        this.name = str;
    }

    public GDAOGenre(Long l) {
        this.id = l;
    }

    public GDAOGenre(Long l, String str) {
        this.id = l;
        this.name = str;
    }
}
