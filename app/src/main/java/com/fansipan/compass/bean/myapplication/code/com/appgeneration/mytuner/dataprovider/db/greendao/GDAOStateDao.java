package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import com.fansipan.compass.bean.myapplication.code.com.greendao.AbstractDao;
import com.fansipan.compass.bean.myapplication.code.com.greendao.Property;
import com.fansipan.compass.bean.myapplication.code.com.greendao.database.Database;
import com.fansipan.compass.bean.myapplication.code.com.greendao.database.DatabaseStatement;
import com.fansipan.compass.bean.myapplication.code.com.greendao.internal.DaoConfig;
import com.fansipan.compass.bean.myapplication.code.com.greendao.internal.SqlUtils;

import java.util.ArrayList;
import java.util.List;

/* loaded from: classes.dex */
public class GDAOStateDao extends AbstractDao<GDAOState, Long> {
    public static final String TABLENAME = "state";
    private DaoSession daoSession;
    private String selectDeep;

    /* loaded from: classes.dex */
    public static class Properties {
        public static final Property Id = new Property(0, Long.class, "id", true, "ID");
        public static final Property Name = new Property(1, String.class, "name", false, "NAME");
        public static final Property Country = new Property(2, Long.class, "country", false, "country");
    }

    public GDAOStateDao(DaoConfig daoConfig) {
        super(daoConfig);
    }

    public static void createTable(Database database, boolean z) {
        String str;
        if (z) {
            str = "IF NOT EXISTS ";
        } else {
            str = "";
        }
        database.execSQL("CREATE TABLE " + str + "\"state\" (\"ID\" INTEGER PRIMARY KEY ,\"NAME\" TEXT,\"COUNTRY\" INTEGER);");
    }

    public static void dropTable(Database database, boolean z) {
        String str;
        StringBuilder m = new StringBuilder("DROP TABLE ");
        if (z) {
            str = "IF EXISTS ";
        } else {
            str = "";
        }
        m.append(str);
        m.append("\"state\"");
        database.execSQL(m.toString());
    }

    public String getSelectDeep() {
        if (this.selectDeep == null) {
            StringBuilder sb = new StringBuilder("SELECT ");
            sb.append(',');
            SqlUtils.appendColumns(sb, "T0", this.daoSession.getGDAOCountryDao().getAllColumns());
            sb.append(" FROM state T");
            sb.append(" LEFT JOIN country T0 ON T.\"COUNTRY\"=T0.\"ID\"");
            sb.append(' ');
            this.selectDeep = sb.toString();
        }
        return this.selectDeep;
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final boolean isEntityUpdateable() {
        return true;
    }

    public List<GDAOState> loadAllDeepFromCursor(Cursor cursor) {
        int count = cursor.getCount();
        ArrayList arrayList = new ArrayList(count);
        if (cursor.moveToFirst()) {
            do {
                try {
                    arrayList.add(loadCurrentDeep(cursor, false));
                } finally {
                }
            } while (cursor.moveToNext());
        }
        return arrayList;
    }

    public GDAOState loadCurrentDeep(Cursor cursor, boolean z) {
        GDAOState loadCurrent = loadCurrent(cursor, 0, z);
        loadCurrent.setCountryObject((GDAOCountry) loadCurrentOther(this.daoSession.getGDAOCountryDao(), cursor, getAllColumns().length));
        return loadCurrent;
    }

    public GDAOState loadDeep(Long l) {
        assertSinglePk();
        if (l == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder(getSelectDeep());
        sb.append("WHERE ");
        Cursor rawQuery = this.db.rawQuery(sb.toString(), new String[]{l.toString()});
        try {
            if (!rawQuery.moveToFirst()) {
                return null;
            }
            if (rawQuery.isLast()) {
                return loadCurrentDeep(rawQuery, true);
            }
            throw new IllegalStateException("Expected unique result, but count was " + rawQuery.getCount());
        } finally {
            rawQuery.close();
        }
    }

    public List<GDAOState> loadDeepAllAndCloseCursor(Cursor cursor) {
        try {
            return loadAllDeepFromCursor(cursor);
        } finally {
            cursor.close();
        }
    }

    public List<GDAOState> queryDeep(String str, String... strArr) {
        Database database = this.db;
        return loadDeepAllAndCloseCursor(database.rawQuery(getSelectDeep() + str, strArr));
    }

    public GDAOStateDao(DaoConfig daoConfig, DaoSession daoSession) {
        super(daoConfig, daoSession);
        this.daoSession = daoSession;
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final void attachEntity(GDAOState gDAOState) {
//        super.attachEntity((GDAOStateDao) gDAOState);
//        gDAOState.__setDaoSession(this.daoSession);
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public Long getKey(GDAOState gDAOState) {
        if (gDAOState != null) {
            return gDAOState.getId();
        }
        return null;
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public boolean hasKey(GDAOState gDAOState) {
        return gDAOState.getId() != null;
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public Long readKey(Cursor cursor, int i) {
        int i2 = i + 0;
        if (cursor.isNull(i2)) {
            return null;
        }
        return Long.valueOf(cursor.getLong(i2));
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final Long updateKeyAfterInsert(GDAOState gDAOState, long j) {
        gDAOState.setId(Long.valueOf(j));
        return Long.valueOf(j);
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final void bindValues(DatabaseStatement databaseStatement, GDAOState gDAOState) {
        databaseStatement.clearBindings();
        Long id = gDAOState.getId();
        if (id != null) {
            databaseStatement.bindLong(1, id.longValue());
        }
        String name = gDAOState.getName();
        if (name != null) {
            databaseStatement.bindString(2, name);
        }
        Long country = gDAOState.getCountry();
        if (country != null) {
            databaseStatement.bindLong(3, country.longValue());
        }
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public GDAOState readEntity(Cursor cursor, int i) {
        int i2 = i + 0;
        int i3 = i + 1;
        int i4 = i + 2;
        return new GDAOState(cursor.isNull(i2) ? null : Long.valueOf(cursor.getLong(i2)), cursor.isNull(i3) ? null : cursor.getString(i3), cursor.isNull(i4) ? null : Long.valueOf(cursor.getLong(i4)));
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public void readEntity(Cursor cursor, GDAOState gDAOState, int i) {
        int i2 = i + 0;
        gDAOState.setId(cursor.isNull(i2) ? null : Long.valueOf(cursor.getLong(i2)));
        int i3 = i + 1;
        gDAOState.setName(cursor.isNull(i3) ? null : cursor.getString(i3));
        int i4 = i + 2;
        gDAOState.setCountry(cursor.isNull(i4) ? null : Long.valueOf(cursor.getLong(i4)));
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final void bindValues(SQLiteStatement sQLiteStatement, GDAOState gDAOState) {
        sQLiteStatement.clearBindings();
        Long id = gDAOState.getId();
        if (id != null) {
            sQLiteStatement.bindLong(1, id.longValue());
        }
        String name = gDAOState.getName();
        if (name != null) {
            sQLiteStatement.bindString(2, name);
        }
        Long country = gDAOState.getCountry();
        if (country != null) {
            sQLiteStatement.bindLong(3, country.longValue());
        }
    }
}
