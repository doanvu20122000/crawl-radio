package com.fansipan.compass.bean.myapplication.code.com.greendao.query;

import com.fansipan.compass.bean.myapplication.code.com.greendao.AbstractDao;

/* loaded from: classes4.dex */
public final class CountQuery<T> extends AbstractQuery<T> {

    /* loaded from: classes4.dex */
    public static final class QueryData<T2> extends AbstractQueryData<T2, CountQuery<T2>> {
        public QueryData(AbstractDao abstractDao, String str, String[] strArr) {
            super(abstractDao, str, strArr);
        }

        @Override // org.greenrobot.greendao.query.AbstractQueryData
        public final CountQuery<T2> createQuery() {
            return new CountQuery(this.dao, this.sql, (String[]) this.initialValues.clone());
        }
    }

    public CountQuery(AbstractDao abstractDao, String str, String[] strArr) {
        super(abstractDao, str, strArr);
    }
}
