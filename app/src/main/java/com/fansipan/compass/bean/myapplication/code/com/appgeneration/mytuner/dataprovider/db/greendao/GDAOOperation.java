package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao;

import java.io.Serializable;

/* loaded from: classes.dex */
public class GDAOOperation implements Serializable {
    private String object_id;
    private Integer object_type;
    private Integer operation_type;
    private Long timestamp;

    public GDAOOperation() {
    }

    public String getObject_id() {
        return this.object_id;
    }

    public Integer getObject_type() {
        return this.object_type;
    }

    public Integer getOperation_type() {
        return this.operation_type;
    }

    public Long getTimestamp() {
        return this.timestamp;
    }

    public void setObject_id(String str) {
        this.object_id = str;
    }

    public void setObject_type(Integer num) {
        this.object_type = num;
    }

    public void setOperation_type(Integer num) {
        this.operation_type = num;
    }

    public void setTimestamp(Long l) {
        this.timestamp = l;
    }

    public GDAOOperation(String str, Integer num, Integer num2, Long l) {
        this.object_id = str;
        this.operation_type = num;
        this.object_type = num2;
        this.timestamp = l;
    }
}
