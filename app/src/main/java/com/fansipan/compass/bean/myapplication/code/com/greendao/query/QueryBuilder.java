package com.fansipan.compass.bean.myapplication.code.com.greendao.query;

import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao.GDAORadioList;
import com.fansipan.compass.bean.myapplication.code.com.greendao.AbstractDao;
import com.fansipan.compass.bean.myapplication.code.com.greendao.DaoException;
import com.fansipan.compass.bean.myapplication.code.com.greendao.Property;
import com.fansipan.compass.bean.myapplication.code.com.greendao.internal.SqlUtils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* loaded from: classes4.dex */
public final class QueryBuilder<T> {
    public final AbstractDao<T, ?> dao;
    public boolean distinct;
    public Integer limit;
    public StringBuilder orderBuilder;
    public final WhereCollector<T> whereCollector;
    public final ArrayList<Integer> values = new ArrayList<>();
    public final ArrayList joins = new ArrayList();
    public String stringOrderCollation = " COLLATE NOCASE";

    public QueryBuilder(AbstractDao<T, ?> abstractDao) {
        this.dao = abstractDao;
        this.whereCollector = new WhereCollector<>(abstractDao);
    }

    public final <J> Join<T, J> addJoin(String str, Property property, AbstractDao<J, ?> abstractDao, Property property2) {
        StringBuilder m = new StringBuilder("J");
        m.append(this.joins.size() + 1);
        Join<T, J> join = new Join<T, J>(str, property, abstractDao, property2, m.toString());
        this.joins.add(join);
        return join;
    }

    public final void appendJoinsAndWheres(StringBuilder sb) {
        this.values.clear();
        Iterator it = this.joins.iterator();
        while (it.hasNext()) {
            Join join = (Join) it.next();
            sb.append(" JOIN ");
            sb.append('\"');
            sb.append(join.daoDestination.getTablename());
            sb.append('\"');
            sb.append(' ');
            sb.append(join.tablePrefix);
            sb.append(" ON ");
            SqlUtils.appendProperty(sb, join.sourceTablePrefix, join.joinPropertySource);
            sb.append('=');
            SqlUtils.appendProperty(sb, join.tablePrefix, join.joinPropertyDestination);
        }
        boolean z = !this.whereCollector.whereConditions.isEmpty();
        Iterator it2 = this.joins.iterator();
        while (it2.hasNext()) {
            Join join2 = (Join) it2.next();
            if (!join2.whereCollector.whereConditions.isEmpty()) {
                if (!z) {
                    sb.append(" WHERE ");
                    z = true;
                } else {
                    sb.append(" AND ");
                }
                join2.whereCollector.appendWhereClause(sb, join2.tablePrefix, this.values);
            }
        }
    }

    public final Query<T> build() {
        StringBuilder sb = new StringBuilder(SqlUtils.createSqlSelect(this.dao.getTablename(), this.dao.getAllColumns(), this.distinct));
        appendJoinsAndWheres(sb);
        StringBuilder sb2 = this.orderBuilder;
        if (sb2 != null && sb2.length() > 0) {
            sb.append(" ORDER BY ");
            sb.append((CharSequence) this.orderBuilder);
        }
        int i = -1;
        if (this.limit != null) {
            sb.append(" LIMIT ?");
            this.values.add(this.limit);
            i = (-1) + this.values.size();
        }
        return (Query<T>) new Query.QueryData(this.dao, sb.toString(), AbstractQuery.toStringArray(this.values.toArray()), i).forCurrentThread$1();
    }

    public final DeleteQuery<T> buildDelete() {
        if (this.joins.isEmpty()) {
            String tablename = this.dao.getTablename();
            StringBuilder sb = new StringBuilder(SqlUtils.createSqlDelete(tablename, null));
            appendJoinsAndWheres(sb);
            String sb2 = sb.toString();
            return (DeleteQuery<T>) new DeleteQuery.QueryData(this.dao, sb2.replace("T.\"", '\"' + tablename + "\".\""), AbstractQuery.toStringArray(this.values.toArray())).forCurrentThread$1();
        }
        throw new DaoException("JOINs are not supported for DELETE queries");
    }

    public final long count() {
        String tablename = this.dao.getTablename();
        int i = SqlUtils.$r8$clinit;
        StringBuilder sb = new StringBuilder("SELECT COUNT(*) FROM ");
        sb.append('\"');
        sb.append(tablename);
        sb.append('\"');
        sb.append(' ');
        return 1;
    }

    public final <J> Join<?, GDAORadioList> join(Property property, Class<J> cls, Property property2) {
        return (Join<?, GDAORadioList>) addJoin("", property, this.dao.getSession().getDao(cls), property2);
    }

    public final void limit(int i) {
        this.limit = Integer.valueOf(i);
    }

    public final List<T> list() {
        Query<T> build = build();
        build.checkThread();
        return ((AbstractDao) build.dao).loadAllAndCloseCursor(build.dao.getDatabase().rawQuery(build.sql, build.parameters));
    }

    public final void orderAscOrDesc(String str, Property... propertyArr) {
        String str2;
        for (Property property : propertyArr) {
            StringBuilder sb = this.orderBuilder;
            if (sb == null) {
                this.orderBuilder = new StringBuilder();
            } else if (sb.length() > 0) {
                this.orderBuilder.append(",");
            }
            StringBuilder sb2 = this.orderBuilder;
            this.whereCollector.checkProperty(property);
            sb2.append('.');
            sb2.append('\'');
            sb2.append(property.columnName);
            sb2.append('\'');
            if (String.class.equals(property.type) && (str2 = this.stringOrderCollation) != null) {
                this.orderBuilder.append(str2);
            }
            this.orderBuilder.append(str);
        }
    }

    public final <J> Join join(Join<?, T> join, Property property, Class<J> cls, Property property2) {
        return addJoin(join.tablePrefix, property, this.dao.getSession().getDao(cls), property2);
    }
}
