package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import com.fansipan.compass.bean.myapplication.code.com.POBRequest$AdPosition$EnumUnboxingLocalUtility;
import com.fansipan.compass.bean.myapplication.code.com.greendao.AbstractDao;
import com.fansipan.compass.bean.myapplication.code.com.greendao.Property;
import com.fansipan.compass.bean.myapplication.code.com.greendao.database.Database;
import com.fansipan.compass.bean.myapplication.code.com.greendao.database.DatabaseStatement;
import com.fansipan.compass.bean.myapplication.code.com.greendao.internal.DaoConfig;

/* loaded from: classes.dex */
public class GDAOSettingsDao extends AbstractDao<GDAOSettings, Void> {
    public static final String TABLENAME = "settings";

    /* loaded from: classes.dex */
    public static class Properties {
        public static final Property Key = new Property(0, String.class, "key", false, "KEY");
        public static final Property Value = new Property(1, String.class, "value", false, "VALUE");
    }

    public GDAOSettingsDao(DaoConfig daoConfig) {
        super(daoConfig);
    }

    public static void createTable(Database database, boolean z) {
        String str;
        if (z) {
            str = "IF NOT EXISTS ";
        } else {
            str = "";
        }
        database.execSQL("CREATE TABLE " + str + "\"settings\" (\"KEY\" TEXT UNIQUE ,\"VALUE\" TEXT);");
    }

    public static void dropTable(Database database, boolean z) {
        String str;
        StringBuilder m = new StringBuilder("DROP TABLE ");
        if (z) {
            str = "IF EXISTS ";
        } else {
            str = "";
        }
        POBRequest$AdPosition$EnumUnboxingLocalUtility.m(m, str, "\"settings\"", database);
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public Void getKey(GDAOSettings gDAOSettings) {
        return null;
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public boolean hasKey(GDAOSettings gDAOSettings) {
        return false;
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final boolean isEntityUpdateable() {
        return true;
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public Void readKey(Cursor cursor, int i) {
        return null;
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final Void updateKeyAfterInsert(GDAOSettings gDAOSettings, long j) {
        return null;
    }

    public GDAOSettingsDao(DaoConfig daoConfig, DaoSession daoSession) {
        super(daoConfig, daoSession);
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final void bindValues(DatabaseStatement databaseStatement, GDAOSettings gDAOSettings) {
        databaseStatement.clearBindings();
        String key = gDAOSettings.getKey();
        if (key != null) {
            databaseStatement.bindString(1, key);
        }
        String value = gDAOSettings.getValue();
        if (value != null) {
            databaseStatement.bindString(2, value);
        }
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public GDAOSettings readEntity(Cursor cursor, int i) {
        int i2 = i + 0;
        int i3 = i + 1;
        return new GDAOSettings(cursor.isNull(i2) ? null : cursor.getString(i2), cursor.isNull(i3) ? null : cursor.getString(i3));
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public void readEntity(Cursor cursor, GDAOSettings gDAOSettings, int i) {
        int i2 = i + 0;
        gDAOSettings.setKey(cursor.isNull(i2) ? null : cursor.getString(i2));
        int i3 = i + 1;
        gDAOSettings.setValue(cursor.isNull(i3) ? null : cursor.getString(i3));
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final void bindValues(SQLiteStatement sQLiteStatement, GDAOSettings gDAOSettings) {
        sQLiteStatement.clearBindings();
        String key = gDAOSettings.getKey();
        if (key != null) {
            sQLiteStatement.bindString(1, key);
        }
        String value = gDAOSettings.getValue();
        if (value != null) {
            sQLiteStatement.bindString(2, value);
        }
    }
}
