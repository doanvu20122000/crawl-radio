package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;


import com.fansipan.compass.bean.myapplication.code.com.POBRequest$AdPosition$EnumUnboxingLocalUtility;
import com.fansipan.compass.bean.myapplication.code.com.greendao.AbstractDao;
import com.fansipan.compass.bean.myapplication.code.com.greendao.Property;
import com.fansipan.compass.bean.myapplication.code.com.greendao.database.Database;
import com.fansipan.compass.bean.myapplication.code.com.greendao.database.DatabaseStatement;
import com.fansipan.compass.bean.myapplication.code.com.greendao.internal.DaoConfig;
/* loaded from: classes.dex */
public class GDAOAppPlaybackEventDao extends AbstractDao<GDAOAppPlaybackEvent, Long> {
    public static final String TABLENAME = "app_playback_events";

    /* loaded from: classes.dex */
    public static class Properties {
        public static final Property Id = new Property(0, Long.class, "id", true, "ID");
        public static final Property Radio = new Property(1, Long.class, "radio", false, "RADIO");
        public static final Property Start_date = new Property(2, String.class, "start_date", false, "START_DATE");
        public static final Property Play_date = new Property(3, String.class, "play_date", false, "PLAY_DATE");
        public static final Property End_date = new Property(4, String.class, "end_date", false, "END_DATE");
        public static final Property Success = new Property(5, Boolean.class, "success", false, "SUCCESS");
        public static final Property Stream = new Property(6, Long.class, GDAOStreamDao.TABLENAME, false, "STREAM");
        public static final Property Stream_url = new Property(7, String.class, "stream_url", false, "STREAM_URL");
        public static final Property Metadata = new Property(8, Boolean.class, "metadata", false, "METADATA");
        public static final Property Error_domain = new Property(9, String.class, "error_domain", false, "ERROR_DOMAIN");
        public static final Property Error_code = new Property(10, String.class, "error_code", false, "ERROR_CODE");
        public static final Property Error_description = new Property(11, String.class, "error_description", false, "ERROR_DESCRIPTION");
        public static final Property Source = new Property(12, String.class, "source", false, "SOURCE");
    }

    public GDAOAppPlaybackEventDao(DaoConfig daoConfig) {
        super(daoConfig);
    }

    public static void createTable(Database database, boolean z) {
        String str;
        if (z) {
            str = "IF NOT EXISTS ";
        } else {
            str = "";
        }
        database.execSQL("CREATE TABLE " + str + "\"app_playback_events\" (\"ID\" INTEGER PRIMARY KEY AUTOINCREMENT ,\"RADIO\" INTEGER,\"START_DATE\" TEXT,\"PLAY_DATE\" TEXT,\"END_DATE\" TEXT,\"SUCCESS\" INTEGER,\"STREAM\" INTEGER,\"STREAM_URL\" TEXT,\"METADATA\" INTEGER,\"ERROR_DOMAIN\" TEXT,\"ERROR_CODE\" TEXT,\"ERROR_DESCRIPTION\" TEXT,\"SOURCE\" TEXT);");
    }

    public static void dropTable(Database database, boolean z) {
        String str;
        StringBuilder m = new StringBuilder("DROP TABLE ");
        if (z) {
            str = "IF EXISTS ";
        } else {
            str = "";
        }
        POBRequest$AdPosition$EnumUnboxingLocalUtility.m(m, str, "\"app_playback_events\"", database);
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final boolean isEntityUpdateable() {
        return true;
    }

    public GDAOAppPlaybackEventDao(DaoConfig daoConfig, DaoSession daoSession) {
        super(daoConfig, daoSession);
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public Long getKey(GDAOAppPlaybackEvent gDAOAppPlaybackEvent) {
        if (gDAOAppPlaybackEvent != null) {
            return gDAOAppPlaybackEvent.getId();
        }
        return null;
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public boolean hasKey(GDAOAppPlaybackEvent gDAOAppPlaybackEvent) {
        return gDAOAppPlaybackEvent.getId() != null;
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public Long readKey(Cursor cursor, int i) {
        int i2 = i + 0;
        if (cursor.isNull(i2)) {
            return null;
        }
        return Long.valueOf(cursor.getLong(i2));
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final Long updateKeyAfterInsert(GDAOAppPlaybackEvent gDAOAppPlaybackEvent, long j) {
        gDAOAppPlaybackEvent.setId(Long.valueOf(j));
        return Long.valueOf(j);
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final void bindValues(DatabaseStatement databaseStatement, GDAOAppPlaybackEvent gDAOAppPlaybackEvent) {
        databaseStatement.clearBindings();
        Long id = gDAOAppPlaybackEvent.getId();
        if (id != null) {
            databaseStatement.bindLong(1, id.longValue());
        }
        Long radio2 = gDAOAppPlaybackEvent.getRadio();
        if (radio2 != null) {
            databaseStatement.bindLong(2, radio2.longValue());
        }
        String start_date = gDAOAppPlaybackEvent.getStart_date();
        if (start_date != null) {
            databaseStatement.bindString(3, start_date);
        }
        String play_date = gDAOAppPlaybackEvent.getPlay_date();
        if (play_date != null) {
            databaseStatement.bindString(4, play_date);
        }
        String end_date = gDAOAppPlaybackEvent.getEnd_date();
        if (end_date != null) {
            databaseStatement.bindString(5, end_date);
        }
        Boolean success = gDAOAppPlaybackEvent.getSuccess();
        if (success != null) {
            databaseStatement.bindLong(6, success.booleanValue() ? 1L : 0L);
        }
        Long stream = gDAOAppPlaybackEvent.getStream();
        if (stream != null) {
            databaseStatement.bindLong(7, stream.longValue());
        }
        String stream_url = gDAOAppPlaybackEvent.getStream_url();
        if (stream_url != null) {
            databaseStatement.bindString(8, stream_url);
        }
        Boolean metadata = gDAOAppPlaybackEvent.getMetadata();
        if (metadata != null) {
            databaseStatement.bindLong(9, metadata.booleanValue() ? 1L : 0L);
        }
        String error_domain = gDAOAppPlaybackEvent.getError_domain();
        if (error_domain != null) {
            databaseStatement.bindString(10, error_domain);
        }
        String error_code = gDAOAppPlaybackEvent.getError_code();
        if (error_code != null) {
            databaseStatement.bindString(11, error_code);
        }
        String error_description = gDAOAppPlaybackEvent.getError_description();
        if (error_description != null) {
            databaseStatement.bindString(12, error_description);
        }
        String source = gDAOAppPlaybackEvent.getSource();
        if (source != null) {
            databaseStatement.bindString(13, source);
        }
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public GDAOAppPlaybackEvent readEntity(Cursor cursor, int i) {
        Boolean valueOf;
        Boolean valueOf2;
        int i2 = i + 0;
        Long valueOf3 = cursor.isNull(i2) ? null : Long.valueOf(cursor.getLong(i2));
        int i3 = i + 1;
        Long valueOf4 = cursor.isNull(i3) ? null : Long.valueOf(cursor.getLong(i3));
        int i4 = i + 2;
        String string = cursor.isNull(i4) ? null : cursor.getString(i4);
        int i5 = i + 3;
        String string2 = cursor.isNull(i5) ? null : cursor.getString(i5);
        int i6 = i + 4;
        String string3 = cursor.isNull(i6) ? null : cursor.getString(i6);
        int i7 = i + 5;
        if (cursor.isNull(i7)) {
            valueOf = null;
        } else {
            valueOf = Boolean.valueOf(cursor.getShort(i7) != 0);
        }
        int i8 = i + 6;
        Long valueOf5 = cursor.isNull(i8) ? null : Long.valueOf(cursor.getLong(i8));
        int i9 = i + 7;
        String string4 = cursor.isNull(i9) ? null : cursor.getString(i9);
        int i10 = i + 8;
        if (cursor.isNull(i10)) {
            valueOf2 = null;
        } else {
            valueOf2 = Boolean.valueOf(cursor.getShort(i10) != 0);
        }
        int i11 = i + 9;
        int i12 = i + 10;
        int i13 = i + 11;
        int i14 = i + 12;
        return new GDAOAppPlaybackEvent(valueOf3, valueOf4, string, string2, string3, valueOf, valueOf5, string4, valueOf2, cursor.isNull(i11) ? null : cursor.getString(i11), cursor.isNull(i12) ? null : cursor.getString(i12), cursor.isNull(i13) ? null : cursor.getString(i13), cursor.isNull(i14) ? null : cursor.getString(i14));
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public void readEntity(Cursor cursor, GDAOAppPlaybackEvent gDAOAppPlaybackEvent, int i) {
        Boolean valueOf;
        Boolean valueOf2;
        int i2 = i + 0;
        gDAOAppPlaybackEvent.setId(cursor.isNull(i2) ? null : Long.valueOf(cursor.getLong(i2)));
        int i3 = i + 1;
        gDAOAppPlaybackEvent.setRadio(cursor.isNull(i3) ? null : Long.valueOf(cursor.getLong(i3)));
        int i4 = i + 2;
        gDAOAppPlaybackEvent.setStart_date(cursor.isNull(i4) ? null : cursor.getString(i4));
        int i5 = i + 3;
        gDAOAppPlaybackEvent.setPlay_date(cursor.isNull(i5) ? null : cursor.getString(i5));
        int i6 = i + 4;
        gDAOAppPlaybackEvent.setEnd_date(cursor.isNull(i6) ? null : cursor.getString(i6));
        int i7 = i + 5;
        if (cursor.isNull(i7)) {
            valueOf = null;
        } else {
            valueOf = Boolean.valueOf(cursor.getShort(i7) != 0);
        }
        gDAOAppPlaybackEvent.setSuccess(valueOf);
        int i8 = i + 6;
        gDAOAppPlaybackEvent.setStream(cursor.isNull(i8) ? null : Long.valueOf(cursor.getLong(i8)));
        int i9 = i + 7;
        gDAOAppPlaybackEvent.setStream_url(cursor.isNull(i9) ? null : cursor.getString(i9));
        int i10 = i + 8;
        if (cursor.isNull(i10)) {
            valueOf2 = null;
        } else {
            valueOf2 = Boolean.valueOf(cursor.getShort(i10) != 0);
        }
        gDAOAppPlaybackEvent.setMetadata(valueOf2);
        int i11 = i + 9;
        gDAOAppPlaybackEvent.setError_domain(cursor.isNull(i11) ? null : cursor.getString(i11));
        int i12 = i + 10;
        gDAOAppPlaybackEvent.setError_code(cursor.isNull(i12) ? null : cursor.getString(i12));
        int i13 = i + 11;
        gDAOAppPlaybackEvent.setError_description(cursor.isNull(i13) ? null : cursor.getString(i13));
        int i14 = i + 12;
        gDAOAppPlaybackEvent.setSource(cursor.isNull(i14) ? null : cursor.getString(i14));
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final void bindValues(SQLiteStatement sQLiteStatement, GDAOAppPlaybackEvent gDAOAppPlaybackEvent) {
        sQLiteStatement.clearBindings();
        Long id = gDAOAppPlaybackEvent.getId();
        if (id != null) {
            sQLiteStatement.bindLong(1, id.longValue());
        }
        Long radio2 = gDAOAppPlaybackEvent.getRadio();
        if (radio2 != null) {
            sQLiteStatement.bindLong(2, radio2.longValue());
        }
        String start_date = gDAOAppPlaybackEvent.getStart_date();
        if (start_date != null) {
            sQLiteStatement.bindString(3, start_date);
        }
        String play_date = gDAOAppPlaybackEvent.getPlay_date();
        if (play_date != null) {
            sQLiteStatement.bindString(4, play_date);
        }
        String end_date = gDAOAppPlaybackEvent.getEnd_date();
        if (end_date != null) {
            sQLiteStatement.bindString(5, end_date);
        }
        Boolean success = gDAOAppPlaybackEvent.getSuccess();
        if (success != null) {
            sQLiteStatement.bindLong(6, success.booleanValue() ? 1L : 0L);
        }
        Long stream = gDAOAppPlaybackEvent.getStream();
        if (stream != null) {
            sQLiteStatement.bindLong(7, stream.longValue());
        }
        String stream_url = gDAOAppPlaybackEvent.getStream_url();
        if (stream_url != null) {
            sQLiteStatement.bindString(8, stream_url);
        }
        Boolean metadata = gDAOAppPlaybackEvent.getMetadata();
        if (metadata != null) {
            sQLiteStatement.bindLong(9, metadata.booleanValue() ? 1L : 0L);
        }
        String error_domain = gDAOAppPlaybackEvent.getError_domain();
        if (error_domain != null) {
            sQLiteStatement.bindString(10, error_domain);
        }
        String error_code = gDAOAppPlaybackEvent.getError_code();
        if (error_code != null) {
            sQLiteStatement.bindString(11, error_code);
        }
        String error_description = gDAOAppPlaybackEvent.getError_description();
        if (error_description != null) {
            sQLiteStatement.bindString(12, error_description);
        }
        String source = gDAOAppPlaybackEvent.getSource();
        if (source != null) {
            sQLiteStatement.bindString(13, source);
        }
    }
}
