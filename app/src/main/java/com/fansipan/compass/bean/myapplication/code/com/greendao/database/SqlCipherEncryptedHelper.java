package com.fansipan.compass.bean.myapplication.code.com.greendao.database;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

/* loaded from: classes4.dex */
public class SqlCipherEncryptedHelper extends SQLiteOpenHelper implements DatabaseOpenHelper.EncryptedHelper {
    public SqlCipherEncryptedHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public SqlCipherEncryptedHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version, @Nullable DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    @RequiresApi(api = Build.VERSION_CODES.P)
    public SqlCipherEncryptedHelper(@Nullable Context context, @Nullable String name, int version, @NonNull SQLiteDatabase.OpenParams openParams) {
        super(context, name, version, openParams);
    }

    @Override // org.greenrobot.greendao.database.DatabaseOpenHelper.EncryptedHelper
    public final EncryptedDatabase getEncryptedReadableDb(String str) {
        return new EncryptedDatabase(getReadableDatabase());
    }

    @Override // org.greenrobot.greendao.database.DatabaseOpenHelper.EncryptedHelper
    public final EncryptedDatabase getEncryptedWritableDb(String str) {
        return new EncryptedDatabase(getWritableDatabase());
    }

    @Override // net.sqlcipher.database.SQLiteOpenHelper
    public final void onCreate(SQLiteDatabase sQLiteDatabase) {
        throw null;
    }

    @Override // net.sqlcipher.database.SQLiteOpenHelper
    public final void onOpen(SQLiteDatabase sQLiteDatabase) {
        throw null;
    }

    @Override // net.sqlcipher.database.SQLiteOpenHelper
    public final void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        throw null;
    }

    @Override // org.greenrobot.greendao.database.DatabaseOpenHelper.EncryptedHelper
    public final EncryptedDatabase getEncryptedReadableDb(char[] cArr) {
        return new EncryptedDatabase(getReadableDatabase());
    }

    @Override // org.greenrobot.greendao.database.DatabaseOpenHelper.EncryptedHelper
    public final EncryptedDatabase getEncryptedWritableDb(char[] cArr) {
        return new EncryptedDatabase(getWritableDatabase());
    }
}
