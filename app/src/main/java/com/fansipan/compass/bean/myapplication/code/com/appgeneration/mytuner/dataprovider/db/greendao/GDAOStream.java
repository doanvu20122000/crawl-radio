package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao;

import android.util.Base64;

import com.fansipan.compass.bean.myapplication.code.com.greendao.DaoException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/* loaded from: classes.dex */
public class GDAOStream implements Serializable {
    private static final String KEY = "EjM93GIyMehKgi8ZZW8NZiee6WfOoadlH6vmBfXMNUehIazzpXaJBJxEAOAdF4d5mJd6Dn8tmoSSNFUATnMl206Ro3zxrqKq9uHaKp1ZQuiG42GKcMVwIvvODRbGIxsfAIgUkfVv2rjZYkDCpJAHzShCBibvzXSIgvptQsW3cqsyH8vhUjlSfxhtOMSSl2F4f4zpLg7zxMEcuxWIbJKS4WkeYt7yP2xEopf55zUfpdH6tAzPwmXA0HD7D2KAc0QLPoo7kr2OJ7GILek9yH73EBXIL1FVEZ42czXqp7cESITtPuCyLVAUR4dJ6snfGUHdvpBb4RYqVEmgyicMHnW03kJfXCCzHgxHWJQk1nYVlJTpJQzYKdD1f1hPHYV1Ux3bKAn1FbbNqmy8pEIMdxpcXFEcKNKMOgoaTO4zN1unxCPXAQ2p4pSR5j7GSc2Q1RDRQVAageQYOfEFXgCObaazXYNu3LDC4a4nPVxyKABGaXqAq1ndKqDwbKnZBCvasE71WYI0sPIkpVAcUpfsM6VKY6miJc3Ua8EuJMYSSBUqce4O3yGOpKtUqvWEtHOtFJNlxjNM7fulrjkFxhc5fCaAfSwJ8jyrDQi3p0yqvQpuHXrd3LvEEwdKpTK0pu8oEO8MkJ5FRLq1TZXVg0WPBuwmAtdBspXLWE3cERyf2bA6Om1VvIvOeVXvPFzbqLeb67VjL4P1VvK2SOWAqZfkHmCK0Q6s7vn0yfI2E1mKC92ysDjQGz8wja4R65LkjLQHOgTjL65P1cTCKI9G0Lctz9lof3t5QlRGWqKnGxRNWR3VhfsEf1jWsBmLaEGvYg9WJqeTIGndZ4DPiG2bfG97xOGj3a8RCN5qXa6b2WDm7DzI752lOQykcSdrtkJTAgO6TWwsJDsSUIWbu7ptdeDrG7yaMbKJvt5xlkjWEYpHJ89p7WgO4zcRmyJxp3L3HHiVNXJtehN26gDFrymp2KMJRwydPnJoNrxqrSk5J7cBbuAYXYnO5OJss0oUsnW7AHWIF4iq";
    private transient DaoSession daoSession;
    private Long id;
    private transient GDAOStreamDao myDao;
    private String params_json;
    private Long quality;

    /* renamed from: radio */
    private Long f538radio;
    private GDAORadio radioObject;
    private transient Long radioObject__resolvedKey;
    private Long rank;
    private String url;

    public GDAOStream() {
    }

    private void __throwIfDetached() {
        if (this.myDao != null) {
            return;
        }
        throw new DaoException("Entity is detached from DAO context");
    }

    public void __setDaoSession(DaoSession daoSession) {
        GDAOStreamDao gDAOStreamDao;
        this.daoSession = daoSession;
        if (daoSession != null) {
            gDAOStreamDao = daoSession.getGDAOStreamDao();
        } else {
            gDAOStreamDao = null;
        }
        this.myDao = gDAOStreamDao;
    }

    public String deObfuscate() {
        byte[] decode = Base64.decode(getUrl(), 0);
        long j = 1024;
        int abs = (int) (Math.abs((this.id.longValue() * 5) + (this.f538radio.longValue() * 3)) % j);
        int abs2 = (int) (Math.abs(this.id.longValue() * 11) % 256);
        int abs3 = (int) (Math.abs(this.f538radio.longValue() - this.id.longValue()) % j);
        String str = "";
        byte NETWORK_LOAD_LIMIT_DISABLED = -1;
        for (byte b2 : decode) {
            int i = b2 & NETWORK_LOAD_LIMIT_DISABLED;
            char charAt = KEY.charAt(abs);
            StringBuilder m = new StringBuilder(str);
            m.append((char) ((i ^ charAt) ^ abs2));
            str = m.toString();
            abs += abs3;
            if (abs >= 1024) {
                abs %= 1024;
            }
        }
        return new String(Base64.decode(str, 0));
    }

    public void delete() {
        __throwIfDetached();
        this.myDao.delete(this);
    }

    public Long getId() {
        return this.id;
    }

    public List<Map.Entry<String, String>> getOrderedParamsJson() {
        String params_json = getParams_json();
        if (params_json != null && !params_json.isEmpty()) {
            try {
                JSONArray jSONArray = new JSONArray(params_json);
                ArrayList arrayList = new ArrayList(jSONArray.length());
                for (int i = 0; i < jSONArray.length(); i++) {
                    JSONObject optJSONObject = jSONArray.optJSONObject(i);
                    if (optJSONObject != null) {
                        String optString = optJSONObject.optString("key", null);
                        String optString2 = optJSONObject.optString("value", null);
                        if (optString != null && optString2 != null) {
                            arrayList.add(new AbstractMap.SimpleImmutableEntry(optString, optString2));
                        }
                    }
                }
                return Collections.unmodifiableList(arrayList);
            } catch (JSONException e2) {
                e2.printStackTrace();
                return Collections.emptyList();
            }
        }
        return Collections.emptyList();
    }

    public String getParams_json() {
        return this.params_json;
    }

    public Long getQuality() {
        return this.quality;
    }

    public Long getRadio() {
        return this.f538radio;
    }

    public GDAORadio getRadioObject() {
        Long l = this.f538radio;
        Long l2 = this.radioObject__resolvedKey;
        if (l2 == null || !l2.equals(l)) {
            __throwIfDetached();
            GDAORadio load = this.daoSession.getGDAORadioDao().load(l);
            synchronized (this) {
                this.radioObject = load;
                this.radioObject__resolvedKey = l;
            }
        }
        return this.radioObject;
    }

    public Long getRank() {
        return this.rank;
    }

    public String getUrl() {
        return this.url;
    }

    public void refresh() {
        __throwIfDetached();
        this.myDao.refresh(this);
    }

    public void setId(Long l) {
        this.id = l;
    }

    public void setParams_json(String str) {
        this.params_json = str;
    }

    public void setQuality(Long l) {
        this.quality = l;
    }

    public void setRadio(Long l) {
        this.f538radio = l;
    }

    public void setRadioObject(GDAORadio gDAORadio) {
        Long id;
        synchronized (this) {
            this.radioObject = gDAORadio;
            if (gDAORadio == null) {
                id = null;
            } else {
                id = gDAORadio.getId();
            }
            this.f538radio = id;
            this.radioObject__resolvedKey = id;
        }
    }

    public void setRank(Long l) {
        this.rank = l;
    }

    public void setUrl(String str) {
        this.url = str;
    }

    public void update() {
        __throwIfDetached();
        this.myDao.update(this);
    }

    public GDAOStream(Long l) {
        this.id = l;
    }

    public GDAOStream(Long l, String str, Long l2, Long l3, Long l4, String str2) {
        this.id = l;
        this.url = str;
        this.rank = l2;
        this.quality = l3;
        this.f538radio = l4;
        this.params_json = str2;
    }
}
