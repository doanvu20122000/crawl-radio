package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.objects;


import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.objects.userdata.UserSelectedEntity;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/* loaded from: classes.dex */
public class PodcastEpisode extends Playable {
    private static final DateFormat DATE_FORMAT = DateFormat.getDateInstance(2);
    private static final String SIZE_REGEX = "\\d+x\\d+b";
    private static final String SIZE_TARGET_FORMAT = "%1$dx%1$db";
    private final String artworkUrl;
    private long episodeId;
    private String formattedPublishDate;
    private String mediaUrl;
    private long parsedDateSeconds;
    private long podcastId;
    private String title;

    public PodcastEpisode(long j, String str) {
        this.podcastId = j;
        this.artworkUrl = str;
    }

    @Override // com.appgeneration.mytuner.dataprovider.db.objects.Playable
    public boolean equals(Object obj) {
        if ((obj instanceof PodcastEpisode) && this.mediaUrl.equals(((PodcastEpisode) obj).mediaUrl)) {
            return true;
        }
        return false;
    }

    public long getEpisodeId() {
        return this.episodeId;
    }

    @Override // com.appgeneration.mytuner.dataprovider.db.objects.UserSelectable
    public String getImageURL() {
        String format = String.format(Locale.US, SIZE_TARGET_FORMAT, 400);
        String str = this.artworkUrl;
        if (str != null) {
            return str.replaceAll(SIZE_REGEX, format);
        }
        return "";
    }

    @Override // com.appgeneration.mytuner.dataprovider.db.objects.UserSelectable
    public String getMediaID() {
        StringBuilder m = new StringBuilder("PodcastEpisode:");
        m.append(this.episodeId);
        m.append("/Podcast:");
        m.append(this.podcastId);
        return m.toString();
    }

    @Override
    public UserSelectedEntity.MediaType getSelectedEntityType() {
        return null;
    }

    @Override // com.appgeneration.mytuner.dataprovider.db.objects.NavigationEntityItem
    public long getObjectId() {
        return this.episodeId;
    }

    public long getPodcastId() {
        return this.podcastId;
    }

    @Override // com.appgeneration.mytuner.dataprovider.db.objects.UserSelectable
    public String getSubTitle(UserLocation userLocation) {
        if (this.formattedPublishDate == null) {
            this.formattedPublishDate = DATE_FORMAT.format(new Date(this.parsedDateSeconds * 1000));
        }
        return this.formattedPublishDate;
    }

    @Override // com.appgeneration.mytuner.dataprovider.db.objects.UserSelectable
    public String getTitle() {
        return this.title;
    }

    @Override // com.appgeneration.mytuner.dataprovider.db.objects.Playable
    public List<StreamWrapper> getUrls() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new StreamWrapper(new URLWrapper(this.mediaUrl)));
        return arrayList;
    }

    public int hashCode() {
        return Long.valueOf(this.episodeId).hashCode();
    }

    @Override // com.appgeneration.mytuner.dataprovider.db.objects.Playable
    public boolean isSeekable() {
        return true;
    }
}
