package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.objects;

/* compiled from: UserLocation.kt */
/* loaded from: classes.dex */
public final class UserLocation {
    private final double latitude;
    private final double longitude;

    public UserLocation(double d2, double d3) {
        this.latitude = d2;
        this.longitude = d3;
    }

    public static /* synthetic */ UserLocation copy$default(UserLocation userLocation, double d2, double d3, int i, Object obj) {
        if ((i & 1) != 0) {
            d2 = userLocation.latitude;
        }
        if ((i & 2) != 0) {
            d3 = userLocation.longitude;
        }
        return userLocation.copy(d2, d3);
    }

    public final double component1() {
        return this.latitude;
    }

    public final double component2() {
        return this.longitude;
    }

    public final UserLocation copy(double d2, double d3) {
        return new UserLocation(d2, d3);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof UserLocation) {
            UserLocation userLocation = (UserLocation) obj;
            return Double.compare(this.latitude, userLocation.latitude) == 0 && Double.compare(this.longitude, userLocation.longitude) == 0;
        }
        return false;
    }

    public final double getLatitude() {
        return this.latitude;
    }

    public final double getLongitude() {
        return this.longitude;
    }

    public int hashCode() {
        long doubleToLongBits = Double.doubleToLongBits(this.latitude);
        long doubleToLongBits2 = Double.doubleToLongBits(this.longitude);
        return (((int) (doubleToLongBits ^ (doubleToLongBits >>> 32))) * 31) + ((int) (doubleToLongBits2 ^ (doubleToLongBits2 >>> 32)));
    }

    public String toString() {
        StringBuilder m = new StringBuilder("UserLocation(latitude=");
        m.append(this.latitude);
        m.append(", longitude=");
        m.append(this.longitude);
        m.append(')');
        return m.toString();
    }
}
