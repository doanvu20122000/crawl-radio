package com.fansipan.compass.bean.myapplication.code.com.greendao.query;

import com.fansipan.compass.bean.myapplication.code.com.greendao.AbstractDao;
import com.fansipan.compass.bean.myapplication.code.com.greendao.Property;

/* loaded from: classes4.dex */
public final class Join<SRC, DST> {
    public final AbstractDao<DST, ?> daoDestination;
    public final Property joinPropertyDestination;
    public final Property joinPropertySource;
    public final String sourceTablePrefix;
    public final String tablePrefix;
    public final WhereCollector<DST> whereCollector;

    public Join(String str, Property property, AbstractDao<DST, ?> abstractDao, Property property2, String str2) {
        this.sourceTablePrefix = str;
        this.joinPropertySource = property;
        this.daoDestination = abstractDao;
        this.joinPropertyDestination = property2;
        this.tablePrefix = str2;
        this.whereCollector = new WhereCollector<>(abstractDao);
    }

    public final void where(WhereCondition.PropertyCondition propertyCondition, WhereCondition... whereConditionArr) {
        this.whereCollector.add(propertyCondition, whereConditionArr);
    }
}
