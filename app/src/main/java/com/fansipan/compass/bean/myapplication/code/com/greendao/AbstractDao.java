package com.fansipan.compass.bean.myapplication.code.com.greendao;

import android.database.CrossProcessCursor;
import android.database.Cursor;
import android.database.CursorWindow;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import com.fansipan.compass.bean.myapplication.code.com.POBRequest$AdPosition$EnumUnboxingLocalUtility;
import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao.GDAORadio;
import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao.GDAORadioDao;
import com.fansipan.compass.bean.myapplication.code.com.greendao.database.Database;
import com.fansipan.compass.bean.myapplication.code.com.greendao.database.DatabaseStatement;
import com.fansipan.compass.bean.myapplication.code.com.greendao.identityscope.IdentityScope;
import com.fansipan.compass.bean.myapplication.code.com.greendao.identityscope.IdentityScopeLong;
import com.fansipan.compass.bean.myapplication.code.com.greendao.internal.DaoConfig;
import com.fansipan.compass.bean.myapplication.code.com.greendao.internal.SqlUtils;
import com.fansipan.compass.bean.myapplication.code.com.greendao.internal.TableStatements;
import com.fansipan.compass.bean.myapplication.code.com.greendao.query.QueryBuilder;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* loaded from: classes4.dex */
public abstract class AbstractDao<T, K> {
    public final DaoConfig config;
    public final Database db;
    public final IdentityScope<K, T> identityScope;
    public final IdentityScopeLong<T> identityScopeLong;
    public final boolean isStandardSQLite;
    public final int pkOrdinal;
    public final AbstractDaoSession session;
    public final TableStatements statements;

    public AbstractDao(DaoConfig daoConfig) {
        this(daoConfig, null);
    }

    private void deleteByKeyInsideSynchronized(K k, DatabaseStatement databaseStatement) {
        if (k instanceof Long) {
            databaseStatement.bindLong(1, ((Long) k).longValue());
        } else if (k != null) {
            databaseStatement.bindString(1, k.toString());
        } else {
            throw new DaoException("Cannot delete entity, key is null");
        }
        databaseStatement.execute();
    }

    private void deleteInTxInternal(Iterable<T> iterable, Iterable<K> iterable2) {
        ArrayList arrayList;
        IdentityScope<K, T> identityScope;
        assertSinglePk();
        DatabaseStatement deleteStatement = this.statements.getDeleteStatement();
        this.db.beginTransaction();
        try {
            synchronized (deleteStatement) {
                IdentityScope<K, T> identityScope2 = this.identityScope;
                if (identityScope2 != null) {
                    identityScope2.lock();
                    arrayList = new ArrayList();
                } else {
                    arrayList = null;
                }
                if (iterable != null) {
                    for (T t : iterable) {
                        K keyVerified = getKeyVerified(t);
                        deleteByKeyInsideSynchronized(keyVerified, deleteStatement);
                        if (arrayList != null) {
                            arrayList.add(keyVerified);
                        }
                    }
                }
                if (iterable2 != null) {
                    for (K k : iterable2) {
                        deleteByKeyInsideSynchronized(k, deleteStatement);
                        if (arrayList != null) {
                            arrayList.add(k);
                        }
                    }
                }
                IdentityScope<K, T> identityScope3 = this.identityScope;
                if (identityScope3 != null) {
                    identityScope3.unlock();
                }
            }
            this.db.setTransactionSuccessful();
            if (arrayList != null && (identityScope = this.identityScope) != null) {
                identityScope.remove(arrayList);
            }
        } finally {
            this.db.endTransaction();
        }
    }

    private long executeInsert(T t, DatabaseStatement databaseStatement, boolean z) {
        long insertInsideTx;
        if (this.db.isDbLockedByCurrentThread()) {
            insertInsideTx = insertInsideTx(t, databaseStatement);
        } else {
            this.db.beginTransaction();
            try {
                insertInsideTx = insertInsideTx(t, databaseStatement);
                this.db.setTransactionSuccessful();
            } finally {
                this.db.endTransaction();
            }
        }
        if (z) {
            updateKeyAfterInsertAndAttach(t, insertInsideTx, true);
        }
        return insertInsideTx;
    }

    private void executeInsertInTx(DatabaseStatement databaseStatement, Iterable<T> iterable, boolean z) {
        this.db.beginTransaction();
        try {
            synchronized (databaseStatement) {
                IdentityScope<K, T> identityScope = this.identityScope;
                if (identityScope != null) {
                    identityScope.lock();
                }
                if (this.isStandardSQLite) {
                    SQLiteStatement sQLiteStatement = (SQLiteStatement) databaseStatement.getRawStatement();
                    for (T t : iterable) {
                        bindValues(sQLiteStatement, (T) t);
                        if (z) {
                            updateKeyAfterInsertAndAttach(t, sQLiteStatement.executeInsert(), false);
                        } else {
                            sQLiteStatement.execute();
                        }
                    }
                } else {
                    for (T t2 : iterable) {
                        bindValues(databaseStatement, (T) t2);
                        if (z) {
                            updateKeyAfterInsertAndAttach(t2, databaseStatement.executeInsert(), false);
                        } else {
                            databaseStatement.execute();
                        }
                    }
                }
                IdentityScope<K, T> identityScope2 = this.identityScope;
                if (identityScope2 != null) {
                    identityScope2.unlock();
                }
            }
            this.db.setTransactionSuccessful();
        } finally {
            this.db.endTransaction();
        }
    }

    private long insertInsideTx(T t, DatabaseStatement databaseStatement) {
        synchronized (databaseStatement) {
            if (this.isStandardSQLite) {
                SQLiteStatement sQLiteStatement = (SQLiteStatement) databaseStatement.getRawStatement();
                bindValues(sQLiteStatement, (T) t);
                return sQLiteStatement.executeInsert();
            }
            bindValues(databaseStatement, (T) t);
            return databaseStatement.executeInsert();
        }
    }

    private CursorWindow moveToNextUnlocked(Cursor cursor) {
        CursorWindow cursorWindow;
        this.identityScope.unlock();
        try {
            if (cursor.moveToNext()) {
                cursorWindow = ((CrossProcessCursor) cursor).getWindow();
            } else {
                cursorWindow = null;
            }
            return cursorWindow;
        } finally {
            this.identityScope.lock();
        }
    }

    public void assertSinglePk() {
        if (this.config.pkColumns.length == 1) {
            return;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(this);
        sb.append(" (");
    }

    public void attachEntity(T t) {
    }

    public final void attachEntity(K k, T t, boolean z) {
        attachEntity(t);
        IdentityScope<K, T> identityScope = this.identityScope;
        if (identityScope != null && k != null) {
            if (z) {
                identityScope.put(k, t);
            } else {
                identityScope.putNoLock(k, t);
            }
        }
    }

    public abstract void bindValues(SQLiteStatement sQLiteStatement, T t);

    public abstract void bindValues(DatabaseStatement databaseStatement, T t);

    public long count() {
        TableStatements tableStatements = this.statements;
        if (tableStatements.countStatement == null) {
            String str = tableStatements.tablename;
            int i = SqlUtils.$r8$clinit;
            tableStatements.countStatement = tableStatements.db.compileStatement("SELECT COUNT(*) FROM \"" + str + '\"');
        }
        return tableStatements.countStatement.simpleQueryForLong();
    }

    public void delete(T t) {
        assertSinglePk();
//        deleteByKey(getKeyVerified(t));
    }

    public void deleteAll() {
        POBRequest$AdPosition$EnumUnboxingLocalUtility.m(new StringBuilder("DELETE FROM '"), this.config.tablename, "'", this.db);
        IdentityScope<K, T> identityScope = this.identityScope;
        if (identityScope != null) {
            identityScope.clear();
        }
    }

    public String[] getAllColumns() {
        return this.config.allColumns;
    }

    public Database getDatabase() {
        return this.db;
    }

    public abstract K getKey(T t);

    public K getKeyVerified(T t) {
        K key = getKey(t);
        if (key == null) {
            if (t == null) {
                throw new NullPointerException("Entity may not be null");
            }
            throw new DaoException("Entity has no key");
        }
        return key;
    }

    public String[] getNonPkColumns() {
        return this.config.nonPkColumns;
    }

    public String[] getPkColumns() {
        return this.config.pkColumns;
    }

    public Property getPkProperty() {
        return this.config.pkProperty;
    }

    public Property[] getProperties() {
        return this.config.properties;
    }

    public AbstractDaoSession getSession() {
        return this.session;
    }

    public TableStatements getStatements() {
        return this.config.statements;
    }

    public String getTablename() {
        return this.config.tablename;
    }

    public abstract boolean hasKey(T t);

    public long insert(T t) {
        return executeInsert(t, this.statements.getInsertStatement(), true);
    }

    public void insertInTx(Iterable<T> iterable) {
        insertInTx(iterable, isEntityUpdateable());
    }

    public long insertOrReplace(T t) {
        return executeInsert(t, this.statements.getInsertOrReplaceStatement(), true);
    }

    public void insertOrReplaceInTx(Iterable<T> iterable, boolean z) {
        executeInsertInTx(this.statements.getInsertOrReplaceStatement(), iterable, z);
    }

    public long insertWithoutSettingPk(T t) {
        return executeInsert(t, this.statements.getInsertOrReplaceStatement(), false);
    }

    public abstract boolean isEntityUpdateable();

    public T load(K k) {
        T t;
        assertSinglePk();
        if (k == null) {
            return null;
        }
        IdentityScope<K, T> identityScope = this.identityScope;
        if (identityScope != null && (t = identityScope.get(k)) != null) {
            return t;
        }
        return loadUniqueAndCloseCursor(this.db.rawQuery(this.statements.getSelectByKey(), new String[]{k.toString()}));
    }

    public List<T> loadAll() {
        return loadAllAndCloseCursor(this.db.rawQuery(this.statements.getSelectAll(), null));
    }

    public List<T> loadAllAndCloseCursor(Cursor cursor) {
        try {
            return loadAllFromCursor(cursor);
        } finally {
            cursor.close();
        }
    }


    public List<T> loadAllFromCursor(Cursor r7) {
        return new ArrayList<T>();
    }

    public T loadByRowId(long j) {
        String[] strArr = {Long.toString(j)};
        Database database = this.db;
        TableStatements tableStatements = this.statements;
        if (tableStatements.selectByRowId == null) {
            tableStatements.selectByRowId = tableStatements.getSelectAll() + "WHERE ROWID=?";
        }
        return loadUniqueAndCloseCursor(database.rawQuery(tableStatements.selectByRowId, strArr));
    }

    public final T loadCurrent(Cursor cursor, int i, boolean z) {
        T noLock;
        T t;
        T t2 = null;
        if (this.identityScopeLong != null) {
            if (i != 0 && cursor.isNull(this.pkOrdinal + i)) {
                return null;
            }
            long j = cursor.getLong(this.pkOrdinal + i);
            IdentityScopeLong<T> identityScopeLong = this.identityScopeLong;
            if (z) {
                t = identityScopeLong.get2(j);
            } else {
                Reference<T> reference = identityScopeLong.map.get(j);
                if (reference != null) {
                    t2 = reference.get();
                }
                t = t2;
            }
            if (t != null) {
                return t;
            }
            T readEntity = readEntity(cursor, i);
            attachEntity(readEntity);
            if (z) {
                IdentityScopeLong<T> identityScopeLong2 = this.identityScopeLong;
                identityScopeLong2.lock.lock();
                try {
                    identityScopeLong2.map.put(j, new WeakReference(readEntity));
                } finally {
                    identityScopeLong2.lock.unlock();
                }
            } else {
                this.identityScopeLong.map.put(j, new WeakReference(readEntity));
            }
            return readEntity;
        } else if (this.identityScope != null) {
            K readKey = readKey(cursor, i);
            if (i != 0 && readKey == null) {
                return null;
            }
            IdentityScope<K, T> identityScope = this.identityScope;
            if (z) {
                noLock = identityScope.get(readKey);
            } else {
                noLock = identityScope.getNoLock(readKey);
            }
            if (noLock != null) {
                return noLock;
            }
            T readEntity2 = readEntity(cursor, i);
            attachEntity(readKey, readEntity2, z);
            return readEntity2;
        } else if (i != 0 && readKey(cursor, i) == null) {
            return null;
        } else {
            T readEntity3 = readEntity(cursor, i);
            attachEntity(readEntity3);
            return readEntity3;
        }
    }

    public final <O> O loadCurrentOther(AbstractDao<O, ?> abstractDao, Cursor cursor, int i) {
        return abstractDao.loadCurrent(cursor, i, true);
    }

    public T loadUnique(Cursor cursor) {
        if (!cursor.moveToFirst()) {
            return null;
        }
        if (cursor.isLast()) {
            return loadCurrent(cursor, 0, true);
        }
        StringBuilder m = new StringBuilder("Expected unique result, but count was ");
        m.append(cursor.getCount());
        throw new DaoException(m.toString());
    }

    public T loadUniqueAndCloseCursor(Cursor cursor) {
        try {
            return loadUnique(cursor);
        } finally {
            cursor.close();
        }
    }

    public QueryBuilder<T> queryBuilder() {
        return new QueryBuilder<>(this);
    }

    public List<T> queryRaw(String str, String... strArr) {
        Database database = this.db;
        return loadAllAndCloseCursor(database.rawQuery(this.statements.getSelectAll() + str, strArr));
    }

    public abstract T readEntity(Cursor cursor, int i);

    public abstract void readEntity(Cursor cursor, T t, int i);

    public abstract K readKey(Cursor cursor, int i);

    public void refresh(T t) {
        assertSinglePk();
        K keyVerified = getKeyVerified(t);
        Cursor rawQuery = this.db.rawQuery(this.statements.getSelectByKey(), new String[]{keyVerified.toString()});
        try {
            if (rawQuery.moveToFirst()) {
                if (rawQuery.isLast()) {
                    readEntity(rawQuery, t, 0);
                    attachEntity(keyVerified, t, true);
                    return;
                }
                throw new DaoException("Expected unique result, but count was " + rawQuery.getCount());
            }
            throw new DaoException("Entity does not exist in the database anymore: " + t.getClass() + " with key " + keyVerified);
        } finally {
            rawQuery.close();
        }
    }

    public void save(T t) {
        if (hasKey(t)) {
            update(t);
        } else {
            insert(t);
        }
    }


    public void update(T t) {
        assertSinglePk();
    }

    public void updateInTx(Iterable<T> iterable) {
    }

    public abstract K updateKeyAfterInsert(T t, long j);

    public void updateKeyAfterInsertAndAttach(T t, long j, boolean z) {
        if (j != -1) {
            attachEntity(updateKeyAfterInsert(t, j), t, z);
        } else {
            Log.w("greenDAO", "Could not insert row (executeInsert returned -1)");
        }
    }

    public AbstractDao(DaoConfig daoConfig, AbstractDaoSession abstractDaoSession) {
        this.config = daoConfig;
        this.session = abstractDaoSession;
        Database database = daoConfig.db;
        this.db = database;
        this.isStandardSQLite = database.getRawDatabase() instanceof SQLiteDatabase;
        IdentityScope<K, T> identityScope = (IdentityScope<K, T>) daoConfig.identityScope;
        this.identityScope = identityScope;
        if (identityScope instanceof IdentityScopeLong) {
            this.identityScopeLong = (IdentityScopeLong) identityScope;
        } else {
            this.identityScopeLong = null;
        }
        this.statements = daoConfig.statements;
        Property property = daoConfig.pkProperty;
        this.pkOrdinal = property != null ? property.ordinal : -1;
    }

    public void deleteByKeyInTx(K... kArr) {
        deleteInTxInternal(null, Arrays.asList(kArr));
    }

    public void deleteInTx(T... tArr) {
        deleteInTxInternal(Arrays.asList(tArr), null);
    }

    public void insertInTx(T... tArr) {
        insertInTx(Arrays.asList(tArr), isEntityUpdateable());
    }

    public void saveInTx(Iterable<T> iterable) {
        int i = 0;
        int i2 = 0;
        for (T t : iterable) {
            if (hasKey(t)) {
                i++;
            } else {
                i2++;
            }
        }
        if (i <= 0 || i2 <= 0) {
            if (i2 > 0) {
                insertInTx(iterable);
                return;
            } else if (i > 0) {
                updateInTx(iterable);
                return;
            } else {
                return;
            }
        }
        ArrayList arrayList = new ArrayList(i);
        ArrayList arrayList2 = new ArrayList(i2);
        for (T t2 : iterable) {
            if (hasKey(t2)) {
                arrayList.add(t2);
            } else {
                arrayList2.add(t2);
            }
        }
        this.db.beginTransaction();
        try {
            updateInTx(arrayList);
            insertInTx(arrayList2);
            this.db.setTransactionSuccessful();
        } finally {
            this.db.endTransaction();
        }
    }

    public void insertInTx(Iterable<T> iterable, boolean z) {
        executeInsertInTx(this.statements.getInsertStatement(), iterable, z);
    }

    public List<GDAORadio> getRadioInDatabase() {
        Cursor cursor = db.rawQuery("SELECT * FROM radio", null);
        ArrayList<GDAORadio> data = new ArrayList<GDAORadio>();
        if (cursor != null) {
            cursor.moveToFirst();
            try {
                while (cursor.moveToNext()) {
                    long column1 = cursor.getLong(cursor.getColumnIndexOrThrow(GDAORadioDao.Properties.columnName().get(0)));
                    long column2 = cursor.getLong(cursor.getColumnIndexOrThrow(GDAORadioDao.Properties.columnName().get(1)));
                    String column3 = cursor.getString(cursor.getColumnIndexOrThrow(GDAORadioDao.Properties.columnName().get(2)));
                    String column4 = cursor.getString(cursor.getColumnIndexOrThrow(GDAORadioDao.Properties.columnName().get(3)));
                    Boolean column5 = cursor.getInt(cursor.getColumnIndexOrThrow(GDAORadioDao.Properties.columnName().get(4))) > 0;
                    Boolean column6 = cursor.getInt(cursor.getColumnIndexOrThrow(GDAORadioDao.Properties.columnName().get(5))) > 0;
                    Boolean column7 = cursor.getInt(cursor.getColumnIndexOrThrow(GDAORadioDao.Properties.columnName().get(6))) > 0;
                    String column8 = cursor.getString(cursor.getColumnIndexOrThrow(GDAORadioDao.Properties.columnName().get(7)));
                    String column9 = cursor.getString(cursor.getColumnIndexOrThrow(GDAORadioDao.Properties.columnName().get(8)));
                    long column10 = cursor.getLong(cursor.getColumnIndexOrThrow(GDAORadioDao.Properties.columnName().get(9)));
                    data.add(new GDAORadio(column1, column2, column3, column4, column5, column6, column7, column8, column9, column10));
                    // Do something with the data
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                cursor.close();
            }
        }
        return data;
    }
}
