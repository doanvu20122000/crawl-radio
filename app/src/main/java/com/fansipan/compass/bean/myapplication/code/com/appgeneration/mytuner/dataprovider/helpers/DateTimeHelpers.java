package com.appgeneration.mytuner.dataprovider.helpers;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

/* loaded from: classes.dex */
public class DateTimeHelpers {
    public static String formatDateToServer(Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.UK);
        if (date != null) {
            return simpleDateFormat.format(date);
        }
        return "";
    }

    public static Date getCurrentDate() {
        Date date = new Date();
        GregorianCalendar gregorianCalendar = new GregorianCalendar(Locale.UK);
        gregorianCalendar.setTime(date);
        return gregorianCalendar.getTime();
    }

    public static long getCurrentTimeInSeconds() {
        return System.currentTimeMillis() / 1000;
    }
}
