package com.fansipan.compass.bean.myapplication.code.convert

data class CountryEntity(
    val app_group_id: Int?,
    val code: String,
    val flag_url: String,
    val id: Int,
    val name: String,
    val show_in_list: Int,
    val use_states: Int
)