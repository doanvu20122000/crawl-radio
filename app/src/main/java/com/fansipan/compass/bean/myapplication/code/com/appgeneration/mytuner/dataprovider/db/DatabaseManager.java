package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteClosable;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao.DaoMaster;
import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao.DaoSession;
import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao.GDAOCityDao;
import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao.GDAORadio;
import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao.GDAORadioCityDao;
import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao.GDAORadioGenreDao;
import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao.GDAOSettingsDao;
import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao.GDAOStream;
import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.objects.Radio;
import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.objects.Setting;
import com.fansipan.compass.bean.myapplication.code.com.greendao.Property;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;

/* loaded from: classes.dex */
public class DatabaseManager {
    private static final String DATABASE_NAME = "ituner.sqlite";
    private static final String TABLE_PODCASTS = "podcasts";
    private static final String TABLE_USER_SELECTED_ENTITIES = "user_selected_entities";
    private static final String TAG = "DatabaseManager";

    /* loaded from: classes.dex */
    public enum ErrorCode {
        NO_ERRORS,
        DB_MISSING,
        WRONG_SCHEMA,
        TABLE_RADIO_MISSING,
        TABLE_RADIO_CITY_MISSING,
        TABLE_RADIO_GENRES_MISSING,
        TABLE_COUNTRY_MISSING,
        TABLE_CITY_MISSING,
        TABLE_STATE_MISSING,
        TABLE_SETTINGS_MISSING,
        GEOLOCATION_ERROR,
        SETTINGS_ERROR,
        OTHER
    }

    private static boolean byteCountCheck(File file, Context context) {
        try {
            if (getFileCount(context.getAssets().open(DATABASE_NAME)) != getFileCount(new FileInputStream(file))) {
                return false;
            }
            return true;
        } catch (Exception e2) {
            Log.e(TAG, "Error counting bytes");
            e2.printStackTrace();
            return false;
        }
    }

    public static void copyDataBase(Context myContext) throws IOException {

        //Open your local db as the input stream
        InputStream myInput = myContext.getAssets().open("database/" + DATABASE_NAME);

        // Path to the just created empty db
        String path = "/data/data/${context.packageName}/databases/";
        String outFileName = path + DATABASE_NAME;

        //Open the empty db as the output stream
        OutputStream myOutput = new FileOutputStream(outFileName);

        //transfer bytes from the inputfile to the outputfile
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer)) > 0) {
            myOutput.write(buffer, 0, length);
        }

        //Close the streams
        myOutput.flush();
        myOutput.close();
        myInput.close();
        Log.d("doanvv", "copyDataBase: ok");
    }

    private static boolean copyDatabase(InputStream inputStream, String str, Context context) throws IOException {
        HashMap<String, ArrayList<ContentValues>> loadMigrationCursors = loadMigrationCursors(str);
        String databaseRawPath = getDatabaseRawPath(context);
        String replaceAll = databaseRawPath.replaceAll(DATABASE_NAME, "ituner_temp.sqlite");
        File file = new File(replaceAll);
        file.delete();
        file.getParentFile().mkdirs();
        file.createNewFile();
        FileOutputStream fileOutputStream = new FileOutputStream(replaceAll);
        byte[] bArr = new byte[131072];
        while (true) {
            int read = inputStream.read(bArr);
            if (read <= 0) {
                break;
            }
            fileOutputStream.write(bArr, 0, read);
        }
        fileOutputStream.flush();
        fileOutputStream.close();
        inputStream.close();
        boolean byteCountCheck = byteCountCheck(file, context);
        saveMigrationCursors(replaceAll, loadMigrationCursors);
        if (byteCountCheck) {
            File file2 = new File(databaseRawPath);
            boolean delete = file2.delete();
            boolean mkdirs = file2.getParentFile().mkdirs();
            boolean createNewFile = file2.createNewFile();
            boolean renameTo = file.renameTo(file2);
            Log.v(TAG, "Deleting old database... " + delete);
        }
        return byteCountCheck;
    }

    private static void corruptDB(Context context) throws IOException {
        String absolutePath = context.getDatabasePath(DATABASE_NAME).getAbsolutePath();
        String replaceAll = absolutePath.replaceAll(DATABASE_NAME, "ituner2.sqlite");
        File file = new File(absolutePath);
        boolean z = false;
        FileInputStream fileInputStream = new FileInputStream(file);
        File file2 = new File(replaceAll);
        file2.createNewFile();
        FileOutputStream fileOutputStream = new FileOutputStream(file2);
        byte[] bArr = new byte[1024];
        int nextInt = new Random().nextInt(43) + 90;
        int i = 0;
        while (true) {
            int read = fileInputStream.read(bArr);
            if (read <= 0) {
                break;
            }
            i++;
            if (i > nextInt) {
                z = true;
                break;
            }
            fileOutputStream.write(bArr, 0, read);
        }
        Log.d(TAG, "copy was tampered (" + z + ") at " + i);
        fileOutputStream.close();
        fileInputStream.close();
        file.delete();
        file.getParentFile().mkdirs();
        file.createNewFile();
        file2.renameTo(file);
    }

    public static DaoSession createSession(Context context) {
        File file = new File(getDatabaseRawPath(context));
        File file2 = new File(file, "ituner.sqlite-wal");
        File file3 = new File(file, "ituner.sqlite-shm");
        if (file2.exists() || file3.exists()) {
            SQLiteDatabase openDatabase = openDatabase(getDatabaseRawPath(context));
            try {
                if (openDatabase != null) {
                    openDatabase.close();
                }
            } catch (Throwable th) {
                if (openDatabase != null) {
                    try {
                        openDatabase.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                }
                throw th;
            }
        }
        SQLiteDatabase writableDatabase = new DatabaseOpenHelper(context, DATABASE_NAME).getWritableDatabase();
        writableDatabase.enableWriteAheadLogging();
        writableDatabase.execSQL("PRAGMA synchronous=NORMAL;");
        writableDatabase.execSQL("PRAGMA auto_vacuum=INCREMENTAL;");
        return new DaoMaster(writableDatabase).newSession();
    }

    private static String getDatabaseRawPath(Context context) {
        return context.getDatabasePath(DATABASE_NAME).getAbsolutePath();
    }

    /* JADX WARN: Removed duplicated region for block: B:27:0x004f  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    private static int getDatabaseVersion(android.database.sqlite.SQLiteDatabase r8) {
        /*
            r0 = 0
            java.lang.String r1 = "SELECT value FROM settings WHERE key LIKE 'setting_key.database_version'"
            r2 = 0
            java.lang.String[] r3 = new java.lang.String[r2]     // Catch: java.lang.Throwable -> L3b java.lang.Exception -> L40
            android.database.Cursor r8 = r8.rawQuery(r1, r3)     // Catch: java.lang.Throwable -> L3b java.lang.Exception -> L40
            r8.moveToFirst()     // Catch: java.lang.Exception -> L39 java.lang.Throwable -> L4c
            java.lang.String r1 = r8.getString(r2)     // Catch: java.lang.Exception -> L39 java.lang.Throwable -> L4c
            java.lang.String r3 = "\\."
            java.lang.String[] r1 = r1.split(r3)     // Catch: java.lang.Exception -> L39 java.lang.Throwable -> L4c
            int r3 = r1.length     // Catch: java.lang.Exception -> L39 java.lang.Throwable -> L4c
            r4 = 0
            r5 = 0
        L1a:
            r6 = 3
            int r6 = java.lang.Math.max(r6, r3)     // Catch: java.lang.Exception -> L39 java.lang.Throwable -> L4c
            if (r4 >= r6) goto L35
            if (r4 >= r3) goto L26
            r6 = r1[r4]     // Catch: java.lang.Exception -> L39 java.lang.Throwable -> L4c
            goto L27
        L26:
            r6 = r0
        L27:
            if (r6 == 0) goto L2e
            int r6 = java.lang.Integer.parseInt(r6)     // Catch: java.lang.Exception -> L39 java.lang.Throwable -> L4c
            goto L2f
        L2e:
            r6 = 0
        L2f:
            int r5 = r5 * 1000
            int r5 = r5 + r6
            int r4 = r4 + 1
            goto L1a
        L35:
            r8.close()
            return r5
        L39:
            r0 = move-exception
            goto L44
        L3b:
            r8 = move-exception
            r7 = r0
            r0 = r8
            r8 = r7
            goto L4d
        L40:
            r8 = move-exception
            r7 = r0
            r0 = r8
            r8 = r7
        L44:
            java.lang.String r1 = "DatabaseManager"
            java.lang.String r2 = "Unexpected error while retrieving version"
            android.util.Log.e(r1, r2, r0)     // Catch: java.lang.Throwable -> L4c
            throw r0     // Catch: java.lang.Throwable -> L4c
        L4c:
            r0 = move-exception
        L4d:
            if (r8 == 0) goto L52
            r8.close()
        L52:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appgeneration.mytuner.dataprovider.db.DatabaseManager.getDatabaseVersion(android.database.sqlite.SQLiteDatabase):int");
    }

    private static int getFileCount(InputStream inputStream) throws IOException {
        byte[] bArr = new byte[131072];
        int i = 0;
        while (true) {
            int read = inputStream.read(bArr);
            if (read != -1) {
                i += read;
            } else {
                inputStream.close();
                return i;
            }
        }
    }

    public static boolean isDatabaseInitialized(Context context) {
        if (context == null) {
            return false;
        }
        ErrorCode shouldCopyDb = shouldCopyDb(getDatabaseRawPath(context));
        ErrorCode errorCode = ErrorCode.NO_ERRORS;
        if (shouldCopyDb != errorCode) {
            return false;
        }
        return true;
    }

    private static boolean isSchemaUpdated(SQLiteDatabase sQLiteDatabase) {
        if (getDatabaseVersion(sQLiteDatabase) == 1046000) {
            return true;
        }
        return false;
    }

    private static boolean isServerTimestampValid(SQLiteDatabase sQLiteDatabase) {
        boolean z = true;
        String[] strArr = {GDAOSettingsDao.Properties.Value.columnName};
        String[] strArr2 = {Setting.SETTING_SERVER_TIMESTAMP};
        try {
            Cursor query = sQLiteDatabase.query(GDAOSettingsDao.TABLENAME, strArr, GDAOSettingsDao.Properties.Key.columnName + "=?", strArr2, null, null, null, null);
            z = (!query.moveToFirst() || query.getLong(0) <= 0) ? false : false;
            query.close();
            return z;
        } catch (Throwable unused) {
            return false;
        }
    }

    private static boolean isTableBroken(SQLiteDatabase sQLiteDatabase, String str) {
        boolean isTableMissing = isTableMissing(sQLiteDatabase, str);
        if (!isTableMissing) {
            return isTableEmpty(sQLiteDatabase, str);
        }
        return isTableMissing;
    }

    private static boolean isTableEmpty(SQLiteDatabase sQLiteDatabase, String str) {
        boolean z = true;
        Cursor cursor = null;
        try {
            cursor = sQLiteDatabase.rawQuery("SELECT COUNT(*) FROM '" + str + "'", null);
            if (cursor.moveToFirst()) {
                if (cursor.getInt(0) != 0) {
                    z = false;
                }
            }
            cursor.close();
            return z;
        } catch (Exception unused) {
            if (cursor != null) {
                cursor.close();
            }
            return true;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    private static boolean isTableMissing(SQLiteDatabase sQLiteDatabase, String str) {
        try {
            Cursor rawQuery = sQLiteDatabase.rawQuery("SELECT DISTINCT tbl_name FROM sqlite_master WHERE tbl_name LIKE '" + str + "'", null);
            if (rawQuery == null) {
                return true;
            }
            int count = rawQuery.getCount();
            rawQuery.close();
            if (count == 0) {
                return true;
            }
            return false;
        } catch (Exception unused) {
            return true;
        }
    }

    public static ArrayList<ContentValues> loadDataFromCursor(Cursor cursor) {
        ArrayList<ContentValues> arrayList = new ArrayList<>();
        if (cursor != null) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                ContentValues contentValues = new ContentValues();
                for (int i = 0; i < cursor.getColumnCount(); i++) {
                    contentValues.put(cursor.getColumnName(i), cursor.getString(i));
                }
                arrayList.add(contentValues);
                cursor.moveToNext();
            }
        }
        return arrayList;
    }

    public static final Property Id = new Property(0, Long.class, "id", true, "ID");
    public static final Property Ord = new Property(1, Long.class, "ord", false, "ORD");
    public static final Property Name = new Property(2, String.class, "name", false, "NAME");
    public static final Property Image_url = new Property(3, String.class, "image_url", false, "IMAGE_URL");
    public static final Property Hidden = new Property(4, Boolean.class, "hidden", false, "HIDDEN");
    public static final Property Has_metadata = new Property(5, Boolean.class, "has_metadata", false, "HAS_METADATA");
    public static final Property Ignore_metadata = new Property(6, Boolean.class, "ignore_metadata", false, "IGNORE_METADATA");
    public static final Property Geolocation_codes = new Property(7, String.class, "geolocation_codes", false, "GEOLOCATION_CODES");
    public static final Property Status = new Property(8, String.class, "status", false, "STATUS");
    public static final Property Country = new Property(9, Long.class, "country", false, "COUNTRY");

    public static ArrayList<Radio> loadRadio(DaoSession daoSession) {
        ArrayList<Radio> list = new ArrayList<>();
        String DB_PATH = "/data/data/com.fansipan.compass.bean.myapplication/databases/";
        String DB_NAME = "ituner.sqlite";
        Cursor cursor = DatabaseManager.openDatabase(DB_PATH + DB_NAME).query("radio", null, null, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Long id = cursor.getLong(0);
                Long ord = cursor.getLong(1);
                String name = cursor.getString(2);
                String imageUrl = cursor.getString(3);
                boolean hidden = cursor.getInt(4) > 0;
                boolean hasMetadata = cursor.getInt(5) > 0;
                boolean ignoreMetadata = cursor.getInt(6) > 0;
                String geoLocation = cursor.getString(7);
                String status = cursor.getString(8);
                Long country = cursor.getLong(9);
                GDAORadio gdaoRadio = new GDAORadio(id, ord, name, imageUrl, hidden, hasMetadata, ignoreMetadata, geoLocation,
                        status, country);
                gdaoRadio.__setDaoSession(daoSession);
                list.add(new Radio(gdaoRadio));
                cursor.moveToNext();
            }
        }
        return list;
    }

    public static ArrayList<GDAOStream> loadStream(Long l) {
        ArrayList<GDAOStream> list = new ArrayList<>();
        String DB_PATH = "/data/data/com.fansipan.compass.bean.myapplication/databases/";
        String DB_NAME = "ituner.sqlite";
        String sql = "SELECT * FROM \"stream\" WHERE radio = " + l;
        Cursor cursor = DatabaseManager.openDatabase(DB_PATH + DB_NAME).rawQuery(sql, null);
        if (cursor != null) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Long id = cursor.getLong(0);
                Long radio = cursor.getLong(1);
                Long quality = cursor.getLong(2);
                String url = cursor.getString(3);
                String paramJson = cursor.getString(4);
                Long rank = cursor.getLong(5);
                GDAOStream gdaoStream = new GDAOStream(id, url, rank, quality, radio, paramJson);
//                gdaoStream.__setDaoSession(daoSession);
                list.add(gdaoStream);
                cursor.moveToNext();
            }
        }
        return list;
    }

    private static HashMap<String, ArrayList<ContentValues>> loadMigrationCursors(String str) {
        HashMap<String, ArrayList<ContentValues>> hashMap = new HashMap<>();
        if (!new File(str).exists()) {
            return hashMap;
        }
        try {
            SQLiteDatabase openDatabase = openDatabase(str);
            try {
                hashMap.put("user_selected_entities", loadDataFromCursor(openDatabase.query("user_selected_entities", new String[]{"*"}, null, null, null, null, null)));
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            try {
                hashMap.put("podcasts", loadDataFromCursor(openDatabase.query("podcasts", new String[]{"*"}, null, null, null, null, null)));
            } catch (Exception e3) {
                e3.printStackTrace();
            }
            openDatabase.close();
        } catch (Exception e4) {
            Log.d(TAG, "Exception loadMigrationCursors");
            e4.printStackTrace();
        }
        return hashMap;
    }

    public static SQLiteDatabase openDatabase(String str) {
        return SQLiteDatabase.openDatabase(str, null, SQLiteDatabase.ENABLE_WRITE_AHEAD_LOGGING);
    }

    private static void saveMigrationCursors(String str, HashMap<String, ArrayList<ContentValues>> hashMap) {
        if (hashMap != null) {
            try {
                if (!hashMap.isEmpty()) {
                    SQLiteDatabase openDatabase = openDatabase(str);
                    for (String str2 : hashMap.keySet()) {
                        Iterator<ContentValues> it = hashMap.get(str2).iterator();
                        while (it.hasNext()) {
                            try {
                                openDatabase.replace(str2, null, it.next());
                                Log.d(TAG, "saving table: " + str2);
                            } catch (Exception unused) {
                            }
                        }
                    }
                    openDatabase.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    private static ErrorCode shouldCopyDb(String str) {
        if (!new File(str).exists()) {
            return ErrorCode.DB_MISSING;
        }
        SQLiteClosable sQLiteClosable = null;
        try {
            try {
                SQLiteDatabase openDatabase = openDatabase(str);
                if (!isSchemaUpdated(openDatabase)) {
                    ErrorCode errorCode = ErrorCode.WRONG_SCHEMA;
                    if (openDatabase != null) {
                        openDatabase.close();
                    }
                    return errorCode;
                } else if (!isServerTimestampValid(openDatabase)) {
                    ErrorCode errorCode2 = ErrorCode.SETTINGS_ERROR;
                    if (openDatabase != null) {
                        openDatabase.close();
                    }
                    return errorCode2;
                } else if (isTableBroken(openDatabase, "radio")) {
                    ErrorCode errorCode3 = ErrorCode.TABLE_RADIO_MISSING;
                    if (openDatabase != null) {
                        openDatabase.close();
                    }
                    return errorCode3;
                } else if (isTableBroken(openDatabase, "radio")) {
                    ErrorCode errorCode4 = ErrorCode.TABLE_RADIO_MISSING;
                    if (openDatabase != null) {
                        openDatabase.close();
                    }
                    return errorCode4;
                } else if (isTableBroken(openDatabase, GDAORadioCityDao.TABLENAME)) {
                    ErrorCode errorCode5 = ErrorCode.TABLE_RADIO_CITY_MISSING;
                    if (openDatabase != null) {
                        openDatabase.close();
                    }
                    return errorCode5;
                } else if (isTableBroken(openDatabase, GDAORadioGenreDao.TABLENAME)) {
                    ErrorCode errorCode6 = ErrorCode.TABLE_RADIO_GENRES_MISSING;
                    if (openDatabase != null) {
                        openDatabase.close();
                    }
                    return errorCode6;
                } else if (isTableBroken(openDatabase, "country")) {
                    ErrorCode errorCode7 = ErrorCode.TABLE_COUNTRY_MISSING;
                    if (openDatabase != null) {
                        openDatabase.close();
                    }
                    return errorCode7;
                } else if (isTableBroken(openDatabase, GDAOCityDao.TABLENAME)) {
                    ErrorCode errorCode8 = ErrorCode.TABLE_CITY_MISSING;
                    if (openDatabase != null) {
                        openDatabase.close();
                    }
                    return errorCode8;
                } else if (isTableBroken(openDatabase, "state")) {
                    ErrorCode errorCode9 = ErrorCode.TABLE_STATE_MISSING;
                    if (openDatabase != null) {
                        openDatabase.close();
                    }
                    return errorCode9;
                } else if (isTableBroken(openDatabase, GDAOSettingsDao.TABLENAME)) {
                    ErrorCode errorCode10 = ErrorCode.TABLE_SETTINGS_MISSING;
                    if (openDatabase != null) {
                        openDatabase.close();
                    }
                    return errorCode10;
                } else {
                    if (openDatabase != null) {
                        openDatabase.close();
                    }
                    return ErrorCode.NO_ERRORS;
                }
            } catch (Exception e2) {
                e2.printStackTrace();
                ErrorCode errorCode12 = ErrorCode.OTHER;
                return errorCode12;
            }
        } catch (Throwable th) {
            throw th;
        }
    }
}
