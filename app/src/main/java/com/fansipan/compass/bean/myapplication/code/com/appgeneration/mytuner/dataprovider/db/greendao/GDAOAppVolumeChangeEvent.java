package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao;

import java.io.Serializable;

/* loaded from: classes.dex */
public class GDAOAppVolumeChangeEvent implements Serializable {
    private Float end_volume;
    private Long id;
    private String start_date;
    private Float start_volume;

    public GDAOAppVolumeChangeEvent() {
    }

    public Float getEnd_volume() {
        return this.end_volume;
    }

    public Long getId() {
        return this.id;
    }

    public String getStart_date() {
        return this.start_date;
    }

    public Float getStart_volume() {
        return this.start_volume;
    }

    public void setEnd_volume(Float f2) {
        this.end_volume = f2;
    }

    public void setId(Long l) {
        this.id = l;
    }

    public void setStart_date(String str) {
        this.start_date = str;
    }

    public void setStart_volume(Float f2) {
        this.start_volume = f2;
    }

    public GDAOAppVolumeChangeEvent(Long l) {
        this.id = l;
    }

    public GDAOAppVolumeChangeEvent(Long l, String str, Float f2, Float f3) {
        this.id = l;
        this.start_date = str;
        this.start_volume = f2;
        this.end_volume = f3;
    }
}
