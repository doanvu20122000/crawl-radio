package com.appgeneration.mytuner.dataprovider.helpers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

/* loaded from: classes.dex */
public class EventsHelper {
    public static final String EVENT_FINISH_UPDATE_DB = "com.appgeneration.mytuner.events.FINISH_UPDATE_DB";
    public static final String EVENT_HOME_TABS_LOADED = "com.appgeneration.mytuner.events.HOME_TABS_LOADED";
    public static final String EVENT_LIST_PRESENTATION_TYPE_CHANGED = "com.appgeneration.mytuner.events.LIST_PRESENTATION_TYPE_CHANGED";
    public static final String EVENT_LOCATION_UPDATED = "com.appgeneration.mytuner.location.LOCATION_UPDATED";
    public static final String EVENT_PREF_DEFAULT_COUNTRY_CHANGED = "com.appgeneration.mytuner.events.PREF_DEFAULT_COUNTRY_CHANGED";
    public static final String EVENT_PREF_EQUALIZER_PRESET_CHANGED = "com.appgeneration.mytuner.events.PREF_EQUALIZER_PRESET_CHANGED";
    public static final String EVENT_SHOW_RATER = "com.appgeneration.mytuner.events.SHOW_RATER";
    public static final String EVENT_USER_SELECTED_UPDATE = "com.appgeneration.mytuner.events.USER_SELECTED_UPDATE";
    private static final String TAG = "EventsHelper";

    public static void registerReceiver(Context context, BroadcastReceiver broadcastReceiver, String... strArr) {
        if (context != null && broadcastReceiver != null && strArr != null && strArr.length != 0) {
            IntentFilter intentFilter = new IntentFilter();
            for (String str : strArr) {
                intentFilter.addAction(str);
            }
            LocalBroadcastManager.getInstance(context.getApplicationContext()).registerReceiver(broadcastReceiver, intentFilter);
        }
    }

    public static void sendEvent(final Context context, String str, Bundle bundle) {
        if (context == null || str == null) {
            return;
        }
        final Intent intent = new Intent(str);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        try {
            new Thread(new Runnable() { // from class: com.appgeneration.mytuner.dataprovider.helpers.EventsHelper.1
                @Override // java.lang.Runnable
                public void run() {
                    LocalBroadcastManager.getInstance(context.getApplicationContext()).sendBroadcast(intent);
                }
            }).start();
        } catch (Throwable th) {
            Log.e(TAG, "Unexpected error while sending event", th);
        }
    }

    public static void unregisterReceiver(Context context, BroadcastReceiver broadcastReceiver) {
        if (context != null && broadcastReceiver != null) {
            LocalBroadcastManager.getInstance(context.getApplicationContext()).unregisterReceiver(broadcastReceiver);
        }
    }

    public static void sendEvent(Context context, String str) {
        sendEvent(context, str, null);
    }

    public static void sendEvent(Context context, String str, String str2, String str3) {
        Bundle bundle = new Bundle();
        bundle.putString(str2, str3);
        sendEvent(context, str, bundle);
    }

    public static void sendEvent(Context context, String str, String str2, int i) {
        Bundle bundle = new Bundle();
        bundle.putInt(str2, i);
        sendEvent(context, str, bundle);
    }
}
