package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.objects;

import android.net.Uri;

import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao.DaoSession;
import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao.GDAORadio;
import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao.GDAORadioDao;
import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao.GDAORadioListDetail;
import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao.GDAORadioListDetailDao;
import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao.GDAOStream;
import com.fansipan.compass.bean.myapplication.code.com.greendao.Property;
import com.fansipan.compass.bean.myapplication.code.com.greendao.query.QueryBuilder;
import com.fansipan.compass.bean.myapplication.code.com.greendao.query.WhereCondition;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* loaded from: classes.dex */
public class Radio {
    private final String mCustomSubtitle;
    private final GDAORadio mDbRadio;

    public Radio(GDAORadio gDAORadio) {
        this(gDAORadio, null);
    }

    private static List<Radio> convertList(List<GDAORadio> list) {
        if (list == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (GDAORadio gDAORadio : list) {
            if (gDAORadio != null) {
                arrayList.add(new Radio(gDAORadio));
            }
        }
        return arrayList;
    }

    public static long countForRadioList(DaoSession daoSession, long j) {
        QueryBuilder<GDAORadio> queryBuilder = daoSession.getGDAORadioDao().queryBuilder();
        queryBuilder.whereCollector.add(GDAORadioDao.Properties.Hidden.eq(Boolean.FALSE), new WhereCondition[0]);
        queryBuilder.join(GDAORadioDao.Properties.Id, GDAORadioListDetail.class, GDAORadioListDetailDao.Properties.Radio).where(GDAORadioListDetailDao.Properties.Radio_list.eq(Long.valueOf(j)), new WhereCondition[0]);
        return queryBuilder.count();
    }

    public static Radio get(DaoSession daoSession, long j) {
        GDAORadio loadByRowId = daoSession.getGDAORadioDao().loadByRowId(j);
        if (loadByRowId != null) {
            return new Radio(loadByRowId);
        }
        return null;
    }

    public static List<Radio> getAll(DaoSession daoSession) {
        QueryBuilder<GDAORadio> queryBuilder = daoSession.getGDAORadioDao().queryBuilder();
//        queryBuilder.whereCollector.add(GDAORadioDao.Properties.Hidden.eq(Boolean.FALSE), new WhereCondition[0]);
//        queryBuilder.orderAscOrDesc(" ASC", GDAORadioDao.Properties.Ord);
        return convertList(queryBuilder.list());
    }

    public static HashMap<Long, Radio> getAllForIds(DaoSession daoSession, List<Long> list) {
        HashMap<Long, Radio> hashMap = new HashMap<>();
        int i = 0;
        while (i < list.size()) {
            int i2 = i + 100;
            List<Long> subList = list.subList(i, Math.min(i2, list.size()));
            QueryBuilder<GDAORadio> queryBuilder = daoSession.getGDAORadioDao().queryBuilder();
            Property property = GDAORadioDao.Properties.Id;
            property.getClass();
            queryBuilder.whereCollector.add(property.in(subList.toArray()), new WhereCondition[0]);
            for (GDAORadio gDAORadio : queryBuilder.list()) {
                if (gDAORadio != null) {
                    hashMap.put(gDAORadio.getId(), new Radio(gDAORadio));
                }
            }
            i = i2;
        }
        return hashMap;
    }

    public static List<Radio> getAllForIdsSorted(DaoSession daoSession, List<Long> list) {
        HashMap<Long, Radio> allForIds = getAllForIds(daoSession, list);
        ArrayList arrayList = new ArrayList(allForIds.size());
        for (Long l : list) {
            Radio radio2 = allForIds.get(Long.valueOf(l.longValue()));
            if (radio2 != null) {
                arrayList.add(radio2);
            }
        }
        return arrayList;
    }

    public static List<Radio> getTop(DaoSession daoSession, int i) {
        QueryBuilder<GDAORadio> queryBuilder = daoSession.getGDAORadioDao().queryBuilder();
        queryBuilder.whereCollector.add(GDAORadioDao.Properties.Hidden.eq(Boolean.FALSE), new WhereCondition[0]);
        queryBuilder.orderAscOrDesc(" ASC", GDAORadioDao.Properties.Ord);
        queryBuilder.limit(i);
        return convertList(queryBuilder.list());
    }

    public static List<Radio> getTopForCountry(DaoSession daoSession, long j, int i) {
        QueryBuilder<GDAORadio> queryBuilder = daoSession.getGDAORadioDao().queryBuilder();
        queryBuilder.whereCollector.add(GDAORadioDao.Properties.Country.eq(Long.valueOf(j)), new WhereCondition[0]);
        queryBuilder.whereCollector.add(GDAORadioDao.Properties.Hidden.eq(Boolean.FALSE), new WhereCondition[0]);
        queryBuilder.orderAscOrDesc(" ASC", GDAORadioDao.Properties.Ord);
        if (i != -1) {
            queryBuilder.limit(i);
        }
        return convertList(queryBuilder.list());
    }

    public static /* synthetic */ int lambda$getAllForRadioList$0(Radio radio2, Radio radio3) {
        return Long.compare(radio2.getOrd(), radio3.getOrd());
    }

    public static Radio searchRadio(DaoSession daoSession, String str, Country country) {
        if (daoSession == null) {
            return null;
        }
        QueryBuilder<GDAORadio> queryBuilder = daoSession.getGDAORadioDao().queryBuilder();
        queryBuilder.orderAscOrDesc(" ASC", GDAORadioDao.Properties.Ord);
        queryBuilder.limit(1);
        String[] split = str.split("\\s+");
        int length = split.length;
        for (int i = 0; i < length; i++) {
            String format = String.format("%%%s%%", split[i].replace("'", ""));
            WhereCondition.PropertyCondition eq = GDAORadioDao.Properties.Country.eq(Long.valueOf(country.getId()));
            Property property = GDAORadioDao.Properties.Name;
            property.getClass();
            queryBuilder.whereCollector.add(eq, new WhereCondition.PropertyCondition(property, " LIKE ?", format));
        }
        List<Radio> convertList = convertList(queryBuilder.list());
        if (convertList.isEmpty()) {
            return null;
        }
        return convertList.get(0);
    }

    @Override // com.appgeneration.mytuner.dataprovider.db.objects.Playable
    public boolean equals(Object obj) {
        if ((obj instanceof Radio) && getId() == ((Radio) obj).getId()) {
            return true;
        }
        return false;
    }

    public long getCountry() {
        return this.mDbRadio.getCountry().longValue();
    }

    public String getGeolocationCodes() {
        return this.mDbRadio.getGeolocation_codes();
    }

    public boolean getHasMetadata() {
        return this.mDbRadio.getHas_metadata().booleanValue();
    }

    public boolean getHidden() {
        return this.mDbRadio.getHidden().booleanValue();
    }

    public long getId() {
        return this.mDbRadio.getId().longValue();
    }

    public boolean getIgnoreMetadata() {
        return this.mDbRadio.getIgnore_metadata().booleanValue();
    }


    public String getImageUrl() {
        return this.mDbRadio.getImage_url();
    }

    public String getName() {
        return this.mDbRadio.getName();
    }

    // com.appgeneration.mytuner.dataprovider.db.objects.NavigationEntityItem
    public long getObjectId() {
        return getId();
    }

    public long getOrd() {
        return this.mDbRadio.getOrd().longValue();
    }


    public String getTitle() {
        return getName();
    }

    public List<StreamWrapper> getUrls(List<GDAOStream> gdaoStreamList) {
        this.mDbRadio.resetGDAOStreamList();
//        List<GDAOStream> gDAOStreamList = this.mDbRadio.getGDAOStreamList();
        ArrayList arrayList = new ArrayList();
        if (gdaoStreamList != null) {
            for (GDAOStream gDAOStream : gdaoStreamList) {
                Uri.Builder buildUpon = Uri.parse(gDAOStream.deObfuscate()).buildUpon();
                for (Map.Entry<String, String> entry : gDAOStream.getOrderedParamsJson()) {
                    buildUpon.appendQueryParameter(entry.getKey(), entry.getValue());
                }
                arrayList.add(new StreamWrapper(gDAOStream.getId().longValue(), new URLWrapper(buildUpon.build().toString())));
            }
        }
        return arrayList;
    }

    public int hashCode() {
        return Long.valueOf(getId()).hashCode();
    }

    public boolean isStatusUnavailable() {
        return this.mDbRadio.getStatus().equals("UNAVAILABLE");
    }

    private Radio(GDAORadio gDAORadio, String str) {
        this.mDbRadio = gDAORadio;
        this.mCustomSubtitle = str;
    }

}
