package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import com.fansipan.compass.bean.myapplication.code.com.greendao.AbstractDao;
import com.fansipan.compass.bean.myapplication.code.com.greendao.Property;
import com.fansipan.compass.bean.myapplication.code.com.greendao.database.Database;
import com.fansipan.compass.bean.myapplication.code.com.greendao.database.DatabaseStatement;
import com.fansipan.compass.bean.myapplication.code.com.greendao.internal.DaoConfig;
import com.fansipan.compass.bean.myapplication.code.com.greendao.query.AbstractQuery;
import com.fansipan.compass.bean.myapplication.code.com.greendao.query.Query;
import com.fansipan.compass.bean.myapplication.code.com.greendao.query.QueryBuilder;
import com.fansipan.compass.bean.myapplication.code.com.greendao.query.WhereCondition;

import java.util.List;

/* loaded from: classes.dex */
public class GDAOStreamDao extends AbstractDao<GDAOStream, Long> {
    public static final String TABLENAME = "stream";
    private DaoSession daoSession;
    private Query<GDAOStream> gDAORadio_GDAOStreamListQuery;
    private String selectDeep;

    /* loaded from: classes.dex */
    public static class Properties {
        public static final Property Id = new Property(0, Long.class, "id", true, "ID");
        public static final Property Url = new Property(1, String.class, "url", false, "URL");
        public static final Property Rank = new Property(2, Long.class, "rank", false, "RANK");
        public static final Property Quality = new Property(3, Long.class, "quality", false, "QUALITY");
        public static final Property Radio = new Property(4, Long.class, "radio", false, "RADIO");
        public static final Property Params_json = new Property(5, String.class, "params_json", false, "PARAMS_JSON");
    }

    public GDAOStreamDao(DaoConfig daoConfig) {
        super(daoConfig);
    }

    public static void createTable(Database database, boolean z) {
        String str;
        if (z) {
            str = "IF NOT EXISTS ";
        } else {
            str = "";
        }
        database.execSQL("CREATE TABLE " + str + "\"stream\" (\"ID\" INTEGER PRIMARY KEY ,\"URL\" TEXT,\"RANK\" INTEGER,\"QUALITY\" INTEGER,\"RADIO\" INTEGER,\"PARAMS_JSON\" TEXT);");
    }

    public static void dropTable(Database database, boolean z) {
        String str;
        StringBuilder m = new StringBuilder("DROP TABLE ");
        if (z) {
            str = "IF EXISTS ";
        } else {
            str = "";
        }
        m.append(str);
        m.append("\"stream\"");
        database.execSQL(m.toString());
    }
    public List<GDAOStream> queryGDAORadio_GDAOStreamList(Long l) {
        AbstractQuery abstractQuery;
        synchronized (this) {
            try {
                if (this.gDAORadio_GDAOStreamListQuery == null) {
                    QueryBuilder<GDAOStream> queryBuilder = queryBuilder();
                    queryBuilder.whereCollector.add(Properties.Radio.eq(null), new WhereCondition[0]);
                    StringBuilder sb = queryBuilder.orderBuilder;
                    if (sb == null) {
                        queryBuilder.orderBuilder = new StringBuilder();
                    } else if (sb.length() > 0) {
                        queryBuilder.orderBuilder.append(",");
                    }
                    queryBuilder.orderBuilder.append("T.'RANK' ASC");
                    this.gDAORadio_GDAOStreamListQuery = queryBuilder.build();
                }
            } catch (Throwable th) {
                throw th;
            }
        }
        Query<GDAOStream> query = this.gDAORadio_GDAOStreamListQuery;
        Query.QueryData<GDAOStream> queryData = query.queryData;
        queryData.getClass();
        if (Thread.currentThread() == query.ownerThread) {
            String[] strArr = queryData.initialValues;
            System.arraycopy(strArr, 0, query.parameters, 0, strArr.length);
            abstractQuery = query;
        } else {
            abstractQuery = queryData.forCurrentThread$1();
        }
        Query<GDAOStream> query2 = (Query) abstractQuery;
        query2.setParameter(l);
        query2.checkThread();
        return ((AbstractDao) query2.dao).loadAllAndCloseCursor(query2.dao.getDatabase().rawQuery(query2.sql, query2.parameters));
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final boolean isEntityUpdateable() {
        return true;
    }

    public GDAOStreamDao(DaoConfig daoConfig, DaoSession daoSession) {
        super(daoConfig, daoSession);
        this.daoSession = daoSession;
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final void attachEntity(GDAOStream gDAOStream) {
//        super.attachEntity((GDAOStreamDao) gDAOStream);
        gDAOStream.__setDaoSession(this.daoSession);
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public Long getKey(GDAOStream gDAOStream) {
        if (gDAOStream != null) {
            return gDAOStream.getId();
        }
        return null;
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public boolean hasKey(GDAOStream gDAOStream) {
        return gDAOStream.getId() != null;
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // org.greenrobot.greendao.AbstractDao
    public Long readKey(Cursor cursor, int i) {
        int i2 = i + 0;
        if (cursor.isNull(i2)) {
            return null;
        }
        return Long.valueOf(cursor.getLong(i2));
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final Long updateKeyAfterInsert(GDAOStream gDAOStream, long j) {
        gDAOStream.setId(Long.valueOf(j));
        return Long.valueOf(j);
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final void bindValues(DatabaseStatement databaseStatement, GDAOStream gDAOStream) {
        databaseStatement.clearBindings();
        Long id = gDAOStream.getId();
        if (id != null) {
            databaseStatement.bindLong(1, id.longValue());
        }
        String url = gDAOStream.getUrl();
        if (url != null) {
            databaseStatement.bindString(2, url);
        }
        Long rank = gDAOStream.getRank();
        if (rank != null) {
            databaseStatement.bindLong(3, rank.longValue());
        }
        Long quality = gDAOStream.getQuality();
        if (quality != null) {
            databaseStatement.bindLong(4, quality.longValue());
        }
        Long radio2 = gDAOStream.getRadio();
        if (radio2 != null) {
            databaseStatement.bindLong(5, radio2.longValue());
        }
        String params_json = gDAOStream.getParams_json();
        if (params_json != null) {
            databaseStatement.bindString(6, params_json);
        }
    }

    /* JADX WARN: Can't rename method to resolve collision */
    @Override // org.greenrobot.greendao.AbstractDao
    public GDAOStream readEntity(Cursor cursor, int i) {
        int i2 = i + 0;
        Long valueOf = cursor.isNull(i2) ? null : Long.valueOf(cursor.getLong(i2));
        int i3 = i + 1;
        String string = cursor.isNull(i3) ? null : cursor.getString(i3);
        int i4 = i + 2;
        Long valueOf2 = cursor.isNull(i4) ? null : Long.valueOf(cursor.getLong(i4));
        int i5 = i + 3;
        Long valueOf3 = cursor.isNull(i5) ? null : Long.valueOf(cursor.getLong(i5));
        int i6 = i + 4;
        int i7 = i + 5;
        return new GDAOStream(valueOf, string, valueOf2, valueOf3, cursor.isNull(i6) ? null : Long.valueOf(cursor.getLong(i6)), cursor.isNull(i7) ? null : cursor.getString(i7));
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public void readEntity(Cursor cursor, GDAOStream gDAOStream, int i) {
        int i2 = i + 0;
        gDAOStream.setId(cursor.isNull(i2) ? null : Long.valueOf(cursor.getLong(i2)));
        int i3 = i + 1;
        gDAOStream.setUrl(cursor.isNull(i3) ? null : cursor.getString(i3));
        int i4 = i + 2;
        gDAOStream.setRank(cursor.isNull(i4) ? null : Long.valueOf(cursor.getLong(i4)));
        int i5 = i + 3;
        gDAOStream.setQuality(cursor.isNull(i5) ? null : Long.valueOf(cursor.getLong(i5)));
        int i6 = i + 4;
        gDAOStream.setRadio(cursor.isNull(i6) ? null : Long.valueOf(cursor.getLong(i6)));
        int i7 = i + 5;
        gDAOStream.setParams_json(cursor.isNull(i7) ? null : cursor.getString(i7));
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final void bindValues(SQLiteStatement sQLiteStatement, GDAOStream gDAOStream) {
        sQLiteStatement.clearBindings();
        Long id = gDAOStream.getId();
        if (id != null) {
            sQLiteStatement.bindLong(1, id.longValue());
        }
        String url = gDAOStream.getUrl();
        if (url != null) {
            sQLiteStatement.bindString(2, url);
        }
        Long rank = gDAOStream.getRank();
        if (rank != null) {
            sQLiteStatement.bindLong(3, rank.longValue());
        }
        Long quality = gDAOStream.getQuality();
        if (quality != null) {
            sQLiteStatement.bindLong(4, quality.longValue());
        }
        Long radio2 = gDAOStream.getRadio();
        if (radio2 != null) {
            sQLiteStatement.bindLong(5, radio2.longValue());
        }
        String params_json = gDAOStream.getParams_json();
        if (params_json != null) {
            sQLiteStatement.bindString(6, params_json);
        }
    }
}
