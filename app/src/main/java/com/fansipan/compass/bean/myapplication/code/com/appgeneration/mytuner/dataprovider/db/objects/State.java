package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.objects;

import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao.DaoSession;
import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao.GDAOState;
import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao.GDAOStateDao;
import com.fansipan.compass.bean.myapplication.code.com.greendao.query.QueryBuilder;
import com.fansipan.compass.bean.myapplication.code.com.greendao.query.WhereCondition;

import java.util.ArrayList;
import java.util.List;

/* loaded from: classes.dex */
public class State implements NavigationEntityItem {
    private final GDAOState mDbState;

    private State(GDAOState gDAOState) {
        this.mDbState = gDAOState;
    }

    private static List<State> convertList(List<GDAOState> list) {
        if (list == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (GDAOState gDAOState : list) {
            if (gDAOState != null) {
                arrayList.add(new State(gDAOState));
            }
        }
        return arrayList;
    }

    public static State get(DaoSession daoSession, long j) {
        GDAOState loadByRowId = daoSession.getGDAOStateDao().loadByRowId(j);
        if (loadByRowId != null) {
            return new State(loadByRowId);
        }
        return null;
    }

    public static List<State> getAll(DaoSession daoSession) {
        QueryBuilder<GDAOState> queryBuilder = daoSession.getGDAOStateDao().queryBuilder();
        queryBuilder.orderAscOrDesc(" ASC", GDAOStateDao.Properties.Name);
        return convertList(queryBuilder.list());
    }

    public static List<State> getAllForCountry(DaoSession daoSession, long j) {
        QueryBuilder<GDAOState> queryBuilder = daoSession.getGDAOStateDao().queryBuilder();
        queryBuilder.whereCollector.add(GDAOStateDao.Properties.Country.eq(Long.valueOf(j)), new WhereCondition[0]);
        queryBuilder.orderAscOrDesc(" ASC", GDAOStateDao.Properties.Name);
        return convertList(queryBuilder.list());
    }

    public long getCountry() {
        GDAOState gDAOState = this.mDbState;
        if (gDAOState != null) {
            return gDAOState.getCountry().longValue();
        }
        return 0L;
    }

    public long getId() {
        GDAOState gDAOState = this.mDbState;
        if (gDAOState != null) {
            return gDAOState.getId().longValue();
        }
        return 0L;
    }

    public String getName() {
        GDAOState gDAOState = this.mDbState;
        if (gDAOState != null) {
            return gDAOState.getName();
        }
        return null;
    }

    @Override // com.appgeneration.mytuner.dataprovider.db.objects.NavigationEntityItem
    public long getObjectId() {
        return getId();
    }

    public void setCountry(long j) {
        this.mDbState.setCountry(Long.valueOf(j));
    }

    public void setId(long j) {
        this.mDbState.setId(Long.valueOf(j));
    }

    public void setName(String str) {
        this.mDbState.setName(str);
    }
}
