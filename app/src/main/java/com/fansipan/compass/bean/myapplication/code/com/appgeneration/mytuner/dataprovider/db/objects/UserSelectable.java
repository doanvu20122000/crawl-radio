package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.objects;

import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.objects.userdata.UserSelectedEntity;

/* loaded from: classes.dex */
public interface UserSelectable extends NavigationEntityItem {
    String getImageURL();

    String getMediaID();

    UserSelectedEntity.MediaType getSelectedEntityType();

    String getSubTitle(UserLocation userLocation);

    String getTitle();
}
