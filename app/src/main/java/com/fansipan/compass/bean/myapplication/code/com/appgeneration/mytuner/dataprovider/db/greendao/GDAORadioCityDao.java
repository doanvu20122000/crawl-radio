package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import com.fansipan.compass.bean.myapplication.code.com.POBRequest$AdPosition$EnumUnboxingLocalUtility;
import com.fansipan.compass.bean.myapplication.code.com.greendao.AbstractDao;
import com.fansipan.compass.bean.myapplication.code.com.greendao.Property;
import com.fansipan.compass.bean.myapplication.code.com.greendao.database.Database;
import com.fansipan.compass.bean.myapplication.code.com.greendao.database.DatabaseStatement;
import com.fansipan.compass.bean.myapplication.code.com.greendao.internal.DaoConfig;

/* loaded from: classes.dex */
public class GDAORadioCityDao extends AbstractDao<GDAORadioCity, Void> {
    public static final String TABLENAME = "radios_cities";

    /* loaded from: classes.dex */
    public static class Properties {
        public static final Property Radio = new Property(0, Long.class, "radio", false, "RADIO");
        public static final Property City = new Property(1, Long.class, GDAOCityDao.TABLENAME, false, "CITY");
        public static final Property Frequency = new Property(2, String.class, "frequency", false, "FREQUENCY");
    }

    public GDAORadioCityDao(DaoConfig daoConfig) {
        super(daoConfig);
    }

    public static void createTable(Database database, boolean z) {
        String str;
        if (z) {
            str = "IF NOT EXISTS ";
        } else {
            str = "";
        }
        database.execSQL("CREATE TABLE " + str + "\"radios_cities\" (\"RADIO\" INTEGER,\"CITY\" INTEGER,\"FREQUENCY\" TEXT);");
    }

    public static void dropTable(Database database, boolean z) {
        String str;
        StringBuilder m = new StringBuilder("DROP TABLE ");
        if (z) {
            str = "IF EXISTS ";
        } else {
            str = "";
        }
        POBRequest$AdPosition$EnumUnboxingLocalUtility.m(m, str, "\"radios_cities\"", database);
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public Void getKey(GDAORadioCity gDAORadioCity) {
        return null;
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public boolean hasKey(GDAORadioCity gDAORadioCity) {
        return false;
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final boolean isEntityUpdateable() {
        return true;
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public Void readKey(Cursor cursor, int i) {
        return null;
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final Void updateKeyAfterInsert(GDAORadioCity gDAORadioCity, long j) {
        return null;
    }

    public GDAORadioCityDao(DaoConfig daoConfig, DaoSession daoSession) {
        super(daoConfig, daoSession);
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final void bindValues(DatabaseStatement databaseStatement, GDAORadioCity gDAORadioCity) {
        databaseStatement.clearBindings();
        Long radio2 = gDAORadioCity.getRadio();
        if (radio2 != null) {
            databaseStatement.bindLong(1, radio2.longValue());
        }
        Long city = gDAORadioCity.getCity();
        if (city != null) {
            databaseStatement.bindLong(2, city.longValue());
        }
        String frequency = gDAORadioCity.getFrequency();
        if (frequency != null) {
            databaseStatement.bindString(3, frequency);
        }
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public GDAORadioCity readEntity(Cursor cursor, int i) {
        int i2 = i + 0;
        int i3 = i + 1;
        int i4 = i + 2;
        return new GDAORadioCity(cursor.isNull(i2) ? null : Long.valueOf(cursor.getLong(i2)), cursor.isNull(i3) ? null : Long.valueOf(cursor.getLong(i3)), cursor.isNull(i4) ? null : cursor.getString(i4));
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public void readEntity(Cursor cursor, GDAORadioCity gDAORadioCity, int i) {
        int i2 = i + 0;
        gDAORadioCity.setRadio(cursor.isNull(i2) ? null : Long.valueOf(cursor.getLong(i2)));
        int i3 = i + 1;
        gDAORadioCity.setCity(cursor.isNull(i3) ? null : Long.valueOf(cursor.getLong(i3)));
        int i4 = i + 2;
        gDAORadioCity.setFrequency(cursor.isNull(i4) ? null : cursor.getString(i4));
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final void bindValues(SQLiteStatement sQLiteStatement, GDAORadioCity gDAORadioCity) {
        sQLiteStatement.clearBindings();
        Long radio2 = gDAORadioCity.getRadio();
        if (radio2 != null) {
            sQLiteStatement.bindLong(1, radio2.longValue());
        }
        Long city = gDAORadioCity.getCity();
        if (city != null) {
            sQLiteStatement.bindLong(2, city.longValue());
        }
        String frequency = gDAORadioCity.getFrequency();
        if (frequency != null) {
            sQLiteStatement.bindString(3, frequency);
        }
    }
}
