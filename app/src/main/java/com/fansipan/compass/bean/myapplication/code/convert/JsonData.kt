package com.fansipan.compass.bean.myapplication.code.convert

import android.content.Context
import android.util.Log
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.json.JSONArray
import java.io.IOException
import java.io.InputStream

object JsonData {
    var listRadio: MutableList<RadioEntity> = mutableListOf()
    var listStream: MutableList<StreamEntity> = mutableListOf()
    var listCountry: MutableList<CountryEntity> = mutableListOf()
    var listCity: MutableList<CityEntity> = mutableListOf()
    var listState: MutableList<StateEntity> = mutableListOf()
    var listGenre: MutableList<GenreEntity> = mutableListOf()
    var listRadioGenre: MutableList<RadioGenreEntity> = mutableListOf()
    var listRadioCity: MutableList<RadioCityEntity> = mutableListOf()

    private fun loadJsonFromAsset(path: String, context: Context): String? {
        var json: String? = null
        try {
            val ios: InputStream = context.assets.open(path)
            val size = ios.available()
            val buffer = ByteArray(size)
            ios.read(buffer)
            ios.close()
            json = String(buffer, Charsets.UTF_8)
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return json
    }

    private suspend fun loadRadioFromJson(context: Context) = withContext(Dispatchers.IO) {
        val start = System.currentTimeMillis()
        listRadio.clear()
        val jsonArray = JSONArray(loadJsonFromAsset("database/radio_json/radio.json", context).toString())
        val length = jsonArray.length()
        for (index in 0 until length) {
            val jsonObject = jsonArray.getJSONObject(index)
            val id: Int = jsonObject.getInt("id")
            val country: Int = jsonObject.getInt("country")
            val geolocation_codes: String = jsonObject.getString("geolocation_codes")
            val has_metadata: Int = jsonObject.getInt("has_metadata")
            val hidden: Int = jsonObject.getInt("hidden")
            val ignore_metadata: Int = jsonObject.getInt("ignore_metadata")
            val image_url: String = jsonObject.getString("image_url")
            val name: String = jsonObject.getString("name")
            val ord: Int = jsonObject.getInt("ord")
            val player_webpage: String = jsonObject.getString("player_webpage")
            val status: String = jsonObject.getString("status")
            val universal_radio: Int = jsonObject.getInt("universal_radio")
            val use_external_player: Int = jsonObject.getInt("use_external_player")
            listRadio.add(
                RadioEntity(
                    country, geolocation_codes, has_metadata,
                    hidden, id, ignore_metadata, image_url, name,
                    ord, player_webpage, status, universal_radio, use_external_player
                )
            )
        }
        val timeLoad = System.currentTimeMillis() - start
        Log.d("doanvv", "loadRadioFromJson: ok $timeLoad ms")
        return@withContext timeLoad
    }

    private suspend fun loadStreamFromJson(context: Context) = withContext(Dispatchers.IO) {
        val start = System.currentTimeMillis()
        listStream.clear()
        val jsonArray = JSONArray(loadJsonFromAsset("database/radio_json/stream.json", context).toString())
        val length = jsonArray.length()
        for (index in 0 until length) {
            val jsonObject = jsonArray.getJSONObject(index)
            val id: Int = jsonObject.getInt("id")
            val params_json: String = jsonObject.getString("params_json")
            val quality: Int = jsonObject.getInt("quality")
            val radio: Int = jsonObject.getInt("radio")
            val rank: Int = jsonObject.getInt("rank")
            val url: String = jsonObject.getString("url")
            listStream.add(StreamEntity(id, params_json, quality, radio, rank, url))
        }
        val timeLoad = System.currentTimeMillis() - start
        Log.d("doanvv", "loadStreamFromJson: ok $timeLoad ms")
        return@withContext timeLoad
    }

    private suspend fun loadCountryFromJson(context: Context) = withContext(Dispatchers.IO) {
        val start = System.currentTimeMillis()
        listCountry.clear()
        val jsonArray = JSONArray(loadJsonFromAsset("database/radio_json/country.json", context).toString())
        val length = jsonArray.length()
        for (index in 0 until length) {
            val jsonObject = jsonArray.getJSONObject(index)
            val value = jsonObject.opt("app_group_id")
            val app_group_id: Int? = jsonObject.get("app_group_id") as? Int?
            val code: String = jsonObject.getString("code")
            val flag_url: String = jsonObject.getString("flag_url")
            val id: Int = jsonObject.getInt("id")
            val name: String = jsonObject.getString("name")
            val show_in_list: Int = jsonObject.getInt("show_in_list")
            val use_states: Int = jsonObject.getInt("use_states")
            listCountry.add(CountryEntity(app_group_id, code, flag_url, id, name, show_in_list, use_states))
        }
        val timeLoad = System.currentTimeMillis() - start
        Log.d("doanvv", "loadCountryFromJson: ok $timeLoad ms")
        return@withContext timeLoad
    }

    private suspend fun loadCityFromJson(context: Context) = withContext(Dispatchers.IO) {
        val start = System.currentTimeMillis()
        listCity.clear()
        val jsonArray = JSONArray(loadJsonFromAsset("database/radio_json/city.json", context).toString())
        val length = jsonArray.length()
        for (index in 0 until length) {
            val jsonObject = jsonArray.getJSONObject(index)
            val country: Int = jsonObject.getInt("country")
            val id: Int = jsonObject.getInt("id")
            val latitude: Double = jsonObject.getDouble("latitude")
            val longitude: Double = jsonObject.getDouble("longitude")
            val name: String = jsonObject.getString("name")
            val state: Int = jsonObject.getInt("state")
            listCity.add(CityEntity(country, id, latitude, longitude, name, state))
        }
        val timeLoad = System.currentTimeMillis() - start
        Log.d("doanvv", "loadCityFromJson: ok $timeLoad ms")
        return@withContext timeLoad
    }

    private suspend fun loadRadioCityFromJson(context: Context) = withContext(Dispatchers.IO) {
        val start = System.currentTimeMillis()
        listRadioCity.clear()
        val jsonArray = JSONArray(loadJsonFromAsset("database/radio_json/radios_cities.json", context).toString())
        val length = jsonArray.length()
        for (index in 0 until length) {
            val jsonObject = jsonArray.getJSONObject(index)
            val city: Int = jsonObject.getInt("city")
            val frequency: String = jsonObject.getString("frequency")
            val radio: Int = jsonObject.getInt("radio")
            listRadioCity.add(RadioCityEntity(city, frequency, radio))
        }
        val timeLoad = System.currentTimeMillis() - start
        Log.d("doanvv", "loadRadioCityFromJson: ok $timeLoad ms")
        return@withContext timeLoad
    }

    private suspend fun loadRadioGenresFromJson(context: Context) = withContext(Dispatchers.IO) {
        val start = System.currentTimeMillis()
        listRadioGenre.clear()
        val jsonArray = JSONArray(loadJsonFromAsset("database/radio_json/radios_genres.json", context).toString())
        val length = jsonArray.length()
        for (index in 0 until length) {
            val jsonObject = jsonArray.getJSONObject(index)
            val genre: Int = jsonObject.getInt("genre")
            val radio: Int = jsonObject.getInt("radio")
            listRadioGenre.add(RadioGenreEntity(genre, radio))
        }
        val timeLoad = System.currentTimeMillis() - start
        Log.d("doanvv", "loadRadioGenresFromJson: ok $timeLoad ms")
        return@withContext timeLoad
    }

    private suspend fun loadGenreFromJson(context: Context) = withContext(Dispatchers.IO) {
        val start = System.currentTimeMillis()
        listGenre.clear()
        val jsonArray = JSONArray(loadJsonFromAsset("database/radio_json/genre.json", context).toString())
        val length = jsonArray.length()
        for (index in 0 until length) {
            val jsonObject = jsonArray.getJSONObject(index)
            val id: Int = jsonObject.getInt("id")
            val image_url: String = jsonObject.getString("image_url")
            val name: String = jsonObject.getString("name")
            listGenre.add(GenreEntity(id.toString(), image_url, name))
        }
        val timeLoad = System.currentTimeMillis() - start
        Log.d("doanvv", "loadGenreFromJson: ok $timeLoad ms")
        return@withContext timeLoad
    }

    private suspend fun loadStateFromJson(context: Context) = withContext(Dispatchers.IO) {
        val start = System.currentTimeMillis()
        listState.clear()
        val jsonArray = JSONArray(loadJsonFromAsset("database/radio_json/state.json", context).toString())
        val length = jsonArray.length()
        for (index in 0 until length) {
            val jsonObject = jsonArray.getJSONObject(index)
            val country: Int = jsonObject.getInt("country")
            val id: Int = jsonObject.getInt("id")
            val name: String = jsonObject.getString("name")
            listState.add(StateEntity(country, id, name))
        }
        val timeLoad = System.currentTimeMillis() - start
        Log.d("doanvv", "loadStateFromJson: ok $timeLoad ms")
        return@withContext timeLoad
    }

    fun loadAllDataFromJson(
        context: Context, onSuccess: (
            stream: Long, radio: Long, country: Long, city: Long, state: Long, genre: Long, radioGenre: Long, radioCity: Long
        ) -> Unit
    ) {
        Log.d("doanvv", "--------------------loadAllDataFromJson--------------------")
        CoroutineScope(Dispatchers.IO).launch {
            val loadStream = async { loadStreamFromJson(context) }
            val loadRadio = async { loadRadioFromJson(context) }
            val loadCountry = async { loadCountryFromJson(context) }
            val loadCity = async { loadCityFromJson(context) }
            val loadState = async { loadStateFromJson(context) }
            val loadGenre = async { loadGenreFromJson(context) }
            val loadRadioGenre = async { loadRadioGenresFromJson(context) }
            val loadRadioCity = async { loadRadioCityFromJson(context) }

            val timeLoadStream = loadStream.await()
            val timeLoadRadio = loadRadio.await()
            val timeLoadCountry = loadCountry.await()
            val timeLoadCity = loadCity.await()
            val timeLoadState = loadState.await()
            val timeLoadGenre = loadGenre.await()
            val timeLoadRadioGenre = loadRadioGenre.await()
            val timeLoadRadioCity = loadRadioCity.await()

            if (timeLoadStream > 0 && timeLoadRadio > 0 && timeLoadCountry > 0 && timeLoadCity > 0 && timeLoadState > 0
                && timeLoadGenre > 0 && timeLoadRadioGenre > 0 && timeLoadRadioCity > 0
            ) {
                withContext(Dispatchers.Main) {
                    Log.d("doanvv", "--------------------done load data--------------------")
                    onSuccess.invoke(
                        timeLoadStream, timeLoadRadio, timeLoadCountry, timeLoadCity, timeLoadState, timeLoadGenre,
                        timeLoadRadioGenre, timeLoadRadioCity
                    )
                }
            }
        }
    }
}