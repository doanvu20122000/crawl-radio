package com.fansipan.compass.bean.myapplication.code.com.greendao.internal;

import com.fansipan.compass.bean.myapplication.code.com.greendao.Property;

/* loaded from: classes4.dex */
public final class SqlUtils {
    public static final /* synthetic */ int $r8$clinit = 0;

    static {
        "0123456789ABCDEF".toCharArray();
    }

    public static void appendColumns(StringBuilder sb, String str, String[] strArr) {
        int length = strArr.length;
        for (int i = 0; i < length; i++) {
            String str2 = strArr[i];
            sb.append(str);
            sb.append(".\"");
            sb.append(str2);
            sb.append('\"');
            if (i < length - 1) {
                sb.append(',');
            }
        }
    }

    public static void appendColumnsEqValue(StringBuilder sb, String str, String[] strArr) {
        for (int i = 0; i < strArr.length; i++) {
            String str2 = strArr[i];
            sb.append(str);
            sb.append(".\"");
            sb.append(str2);
            sb.append('\"');
            sb.append("=?");
            if (i < strArr.length - 1) {
                sb.append(',');
            }
        }
    }

    public static void appendProperty(StringBuilder sb, String str, Property property) {
        if (str != null) {
            sb.append(str);
            sb.append('.');
        }
        sb.append('\"');
        sb.append(property.columnName);
        sb.append('\"');
    }

    public static String createSqlDelete(String str, String[] strArr) {
        String str2 = '\"' + str + '\"';
        StringBuilder sb = new StringBuilder("DELETE FROM ");
        sb.append(str2);
        if (strArr != null && strArr.length > 0) {
            sb.append(" WHERE ");
            appendColumnsEqValue(sb, str2, strArr);
        }
        return sb.toString();
    }

    public static String createSqlInsert(String str, String str2, String[] strArr) {
        StringBuilder sb = new StringBuilder(str);
        sb.append('\"');
        sb.append(str2);
        sb.append('\"');
        sb.append(" (");
        int length = strArr.length;
        for (int i = 0; i < length; i++) {
            sb.append('\"');
            sb.append(strArr[i]);
            sb.append('\"');
            if (i < length - 1) {
                sb.append(',');
            }
        }
        sb.append(") VALUES (");
        int length2 = strArr.length;
        for (int i2 = 0; i2 < length2; i2++) {
            if (i2 < length2 - 1) {
                sb.append("?,");
            } else {
                sb.append('?');
            }
        }
        sb.append(')');
        return sb.toString();
    }

    public static String createSqlSelect(String str, String[] strArr, boolean z) {
        String str2;
        if (z) {
            str2 = "SELECT DISTINCT ";
        } else {
            str2 = "SELECT ";
        }
        StringBuilder sb = new StringBuilder(str2);
        sb.append("* FROM ");
        sb.append('\"');
        sb.append(str);
        sb.append('\"');
        sb.append(' ');
        sb.append(' ');
        return sb.toString();
    }
}
