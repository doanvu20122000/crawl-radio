package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import com.fansipan.compass.bean.myapplication.code.com.POBRequest$AdPosition$EnumUnboxingLocalUtility;
import com.fansipan.compass.bean.myapplication.code.com.greendao.AbstractDao;
import com.fansipan.compass.bean.myapplication.code.com.greendao.Property;
import com.fansipan.compass.bean.myapplication.code.com.greendao.database.Database;
import com.fansipan.compass.bean.myapplication.code.com.greendao.database.DatabaseStatement;
import com.fansipan.compass.bean.myapplication.code.com.greendao.internal.DaoConfig;


/* loaded from: classes.dex */
public class GDAORadioListDetailDao extends AbstractDao<GDAORadioListDetail, Void> {
    public static final String TABLENAME = "radio_list_detail";

    /* loaded from: classes.dex */
    public static class Properties {
        public static final Property Rank = new Property(0, Long.class, "rank", false, "RANK");
        public static final Property Radio = new Property(1, Long.class, "radio", false, "");
        public static final Property Radio_list = new Property(2, Long.class, GDAORadioListDao.TABLENAME, false, "RADIO_LIST");
    }

    public GDAORadioListDetailDao(DaoConfig daoConfig) {
        super(daoConfig);
    }

    public static void createTable(Database database, boolean z) {
        String str;
        if (z) {
            str = "IF NOT EXISTS ";
        } else {
            str = "";
        }
        database.execSQL("CREATE TABLE " + str + "\"radio_list_detail\" (\"RANK\" INTEGER,\"RADIO\" INTEGER,\"RADIO_LIST\" INTEGER);");
    }

    public static void dropTable(Database database, boolean z) {
        String str;
        StringBuilder m = new StringBuilder("DROP TABLE ");
        if (z) {
            str = "IF EXISTS ";
        } else {
            str = "";
        }
        POBRequest$AdPosition$EnumUnboxingLocalUtility.m(m, str, "\"radio_list_detail\"", database);
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public Void getKey(GDAORadioListDetail gDAORadioListDetail) {
        return null;
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public boolean hasKey(GDAORadioListDetail gDAORadioListDetail) {
        return false;
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final boolean isEntityUpdateable() {
        return true;
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public Void readKey(Cursor cursor, int i) {
        return null;
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final Void updateKeyAfterInsert(GDAORadioListDetail gDAORadioListDetail, long j) {
        return null;
    }

    public GDAORadioListDetailDao(DaoConfig daoConfig, DaoSession daoSession) {
        super(daoConfig, daoSession);
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final void bindValues(DatabaseStatement databaseStatement, GDAORadioListDetail gDAORadioListDetail) {
        databaseStatement.clearBindings();
        Long rank = gDAORadioListDetail.getRank();
        if (rank != null) {
            databaseStatement.bindLong(1, rank.longValue());
        }
        Long radio2 = gDAORadioListDetail.getRadio();
        if (radio2 != null) {
            databaseStatement.bindLong(2, radio2.longValue());
        }
        Long radio_list = gDAORadioListDetail.getRadio_list();
        if (radio_list != null) {
            databaseStatement.bindLong(3, radio_list.longValue());
        }
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public GDAORadioListDetail readEntity(Cursor cursor, int i) {
        int i2 = i + 0;
        int i3 = i + 1;
        int i4 = i + 2;
        return new GDAORadioListDetail(cursor.isNull(i2) ? null : Long.valueOf(cursor.getLong(i2)), cursor.isNull(i3) ? null : Long.valueOf(cursor.getLong(i3)), cursor.isNull(i4) ? null : Long.valueOf(cursor.getLong(i4)));
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public void readEntity(Cursor cursor, GDAORadioListDetail gDAORadioListDetail, int i) {
        int i2 = i + 0;
        gDAORadioListDetail.setRank(cursor.isNull(i2) ? null : Long.valueOf(cursor.getLong(i2)));
        int i3 = i + 1;
        gDAORadioListDetail.setRadio(cursor.isNull(i3) ? null : Long.valueOf(cursor.getLong(i3)));
        int i4 = i + 2;
        gDAORadioListDetail.setRadio_list(cursor.isNull(i4) ? null : Long.valueOf(cursor.getLong(i4)));
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final void bindValues(SQLiteStatement sQLiteStatement, GDAORadioListDetail gDAORadioListDetail) {
        sQLiteStatement.clearBindings();
        Long rank = gDAORadioListDetail.getRank();
        if (rank != null) {
            sQLiteStatement.bindLong(1, rank.longValue());
        }
        Long radio2 = gDAORadioListDetail.getRadio();
        if (radio2 != null) {
            sQLiteStatement.bindLong(2, radio2.longValue());
        }
        Long radio_list = gDAORadioListDetail.getRadio_list();
        if (radio_list != null) {
            sQLiteStatement.bindLong(3, radio_list.longValue());
        }
    }
}
