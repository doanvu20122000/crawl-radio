package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.objects;

import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao.DaoSession;
import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao.GDAOStream;

/* loaded from: classes.dex */
public class Stream {
    private final GDAOStream mDbStream;

    private Stream(GDAOStream gDAOStream) {
        this.mDbStream = gDAOStream;
    }

    public static Stream get(DaoSession daoSession, long j) {
        GDAOStream loadByRowId = daoSession.getGDAOStreamDao().loadByRowId(j);
        if (loadByRowId != null) {
            return new Stream(loadByRowId);
        }
        return null;
    }

    public String deObfuscate() {
        return this.mDbStream.deObfuscate();
    }

    public long getId() {
        return this.mDbStream.getId().longValue();
    }

    public long getQuality() {
        return this.mDbStream.getQuality().longValue();
    }

    public long getRadio() {
        return this.mDbStream.getRadio().longValue();
    }

    public long getRank() {
        return this.mDbStream.getRank().longValue();
    }

    public String getUrl() {
        return this.mDbStream.getUrl();
    }
}
