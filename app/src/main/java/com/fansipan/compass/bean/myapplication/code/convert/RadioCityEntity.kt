package com.fansipan.compass.bean.myapplication.code.convert

data class RadioCityEntity(
    val city: Int,
    val frequency: String,
    val radio: Int
)