package com.fansipan.compass.bean.myapplication.code.com;

import android.util.Base64;

public class DecodeBase64 {
    private static final String KEY = "EjM93GIyMehKgi8ZZW8NZiee6WfOoadlH6vmBfXMNUehIazzpXaJBJxEAOAdF4d5mJd6Dn8tmoSSNFUATnMl206Ro3zxrqKq9uHaKp1ZQuiG42GKcMVwIvvODRbGIxsfAIgUkfVv2rjZYkDCpJAHzShCBibvzXSIgvptQsW3cqsyH8vhUjlSfxhtOMSSl2F4f4zpLg7zxMEcuxWIbJKS4WkeYt7yP2xEopf55zUfpdH6tAzPwmXA0HD7D2KAc0QLPoo7kr2OJ7GILek9yH73EBXIL1FVEZ42czXqp7cESITtPuCyLVAUR4dJ6snfGUHdvpBb4RYqVEmgyicMHnW03kJfXCCzHgxHWJQk1nYVlJTpJQzYKdD1f1hPHYV1Ux3bKAn1FbbNqmy8pEIMdxpcXFEcKNKMOgoaTO4zN1unxCPXAQ2p4pSR5j7GSc2Q1RDRQVAageQYOfEFXgCObaazXYNu3LDC4a4nPVxyKABGaXqAq1ndKqDwbKnZBCvasE71WYI0sPIkpVAcUpfsM6VKY6miJc3Ua8EuJMYSSBUqce4O3yGOpKtUqvWEtHOtFJNlxjNM7fulrjkFxhc5fCaAfSwJ8jyrDQi3p0yqvQpuHXrd3LvEEwdKpTK0pu8oEO8MkJ5FRLq1TZXVg0WPBuwmAtdBspXLWE3cERyf2bA6Om1VvIvOeVXvPFzbqLeb67VjL4P1VvK2SOWAqZfkHmCK0Q6s7vn0yfI2E1mKC92ysDjQGz8wja4R65LkjLQHOgTjL65P1cTCKI9G0Lctz9lof3t5QlRGWqKnGxRNWR3VhfsEf1jWsBmLaEGvYg9WJqeTIGndZ4DPiG2bfG97xOGj3a8RCN5qXa6b2WDm7DzI752lOQykcSdrtkJTAgO6TWwsJDsSUIWbu7ptdeDrG7yaMbKJvt5xlkjWEYpHJ89p7WgO4zcRmyJxp3L3HHiVNXJtehN26gDFrymp2KMJRwydPnJoNrxqrSk5J7cBbuAYXYnO5OJss0oUsnW7AHWIF4iq";

    static final byte LOAD_LIMIT = -1;

    public static String deObfuscate(Long id, Long f538radio, String url) {
        byte[] decode = Base64.decode(url, 0);
        long j = 1024;
        int abs = (int) (Math.abs((id * 5) + (f538radio * 3)) % j);
        int abs2 = (int) (Math.abs(id * 11) % 256);
        int abs3 = (int) (Math.abs(f538radio - id) % j);
        String str = "";
        for (byte b2 : decode) {
            int i = b2 & LOAD_LIMIT;
            char charAt = KEY.charAt(abs);
            StringBuilder m = new StringBuilder(str);
            m.append((char) ((i ^ charAt) ^ abs2));
            str = m.toString();
            abs += abs3;
            if (abs >= 1024) {
                abs %= 1024;
            }
        }
        return new String(Base64.decode(str, 0));
    }
}
