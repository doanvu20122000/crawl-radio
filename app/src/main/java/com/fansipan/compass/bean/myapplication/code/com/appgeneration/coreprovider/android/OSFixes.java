package com.fansipan.compass.bean.myapplication.code.com.appgeneration.coreprovider.android;

import java.util.Locale;

import kotlin.jvm.internal.DefaultConstructorMarker;

/* compiled from: OSFixes.kt */
/* loaded from: classes.dex */
public final class OSFixes {
    public static final Companion Companion = new Companion(null);
    private static final String HEBREW_LANGUAGE = "he";
    private static final String HEBREW_LEGACY_LANGUAGE = "iw";
    private static final String INDONESIAN_LANGUAGE = "id";
    private static final String INDONESIAN_LEGACY_LANGUAGE = "in";
    private static final String YIDDISH_LANGUAGE = "yi";
    private static final String YIDDISH_LEGACY_LANGUAGE = "ji";

    /* compiled from: OSFixes.kt */
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        public final String localeToSimplifiedString(Locale locale) {
            String language = "";
            String language2 = locale.getLanguage();
            if (language2 != null) {
                int hashCode = language2.hashCode();
                if (hashCode != 3365) {
                    if (hashCode != 3374) {
                        if (hashCode == 3391 && language2.equals(OSFixes.YIDDISH_LEGACY_LANGUAGE)) {
                            language = OSFixes.YIDDISH_LANGUAGE;
                        }
                    } else if (language2.equals(OSFixes.HEBREW_LEGACY_LANGUAGE)) {
                        language = OSFixes.HEBREW_LANGUAGE;
                    }
                } else if (language2.equals("in")) {
                    language = "id";
                }
                return language + '_' + locale.getCountry();
            }
            language = locale.getLanguage();
            return language + '_' + locale.getCountry();
        }
    }

    public static final String localeToSimplifiedString(Locale locale) {
        return Companion.localeToSimplifiedString(locale);
    }
}
