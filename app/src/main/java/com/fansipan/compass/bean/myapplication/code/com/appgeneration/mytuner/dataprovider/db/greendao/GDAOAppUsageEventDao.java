package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import com.fansipan.compass.bean.myapplication.code.com.greendao.AbstractDao;
import com.fansipan.compass.bean.myapplication.code.com.greendao.Property;
import com.fansipan.compass.bean.myapplication.code.com.greendao.database.Database;
import com.fansipan.compass.bean.myapplication.code.com.greendao.database.DatabaseStatement;
import com.fansipan.compass.bean.myapplication.code.com.greendao.internal.DaoConfig;

/* loaded from: classes.dex */
public class GDAOAppUsageEventDao extends AbstractDao<GDAOAppUsageEvent, Long> {
    public static final String TABLENAME = "app_usage_events";

    /* loaded from: classes.dex */
    public static class Properties {
        public static final Property Id = new Property(0, Long.class, "id", true, "ID");
        public static final Property Session_id = new Property(1, String.class, "session_id", false, "SESSION_ID");
        public static final Property Date = new Property(2, String.class, "date", false, "DATE");
        public static final Property Startup = new Property(3, Boolean.class, "startup", false, "STARTUP");
    }

    public GDAOAppUsageEventDao(DaoConfig daoConfig) {
        super(daoConfig);
    }

    public static void createTable(Database database, boolean z) {
        String str;
        if (z) {
            str = "IF NOT EXISTS ";
        } else {
            str = "";
        }
        database.execSQL("CREATE TABLE " + str + "\"app_usage_events\" (\"ID\" INTEGER PRIMARY KEY AUTOINCREMENT ,\"SESSION_ID\" TEXT UNIQUE ,\"DATE\" TEXT,\"STARTUP\" INTEGER);");
    }

    public static void dropTable(Database database, boolean z) {
        String str;
        StringBuilder m = new StringBuilder("DROP TABLE ");
        if (z) {
            str = "IF EXISTS ";
        } else {
            str = "";
        }
        m.append(str);
        m.append("\"app_usage_events\"");
        database.execSQL(m.toString());
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final boolean isEntityUpdateable() {
        return true;
    }

    public GDAOAppUsageEventDao(DaoConfig daoConfig, DaoSession daoSession) {
        super(daoConfig, daoSession);
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public Long getKey(GDAOAppUsageEvent gDAOAppUsageEvent) {
        if (gDAOAppUsageEvent != null) {
            return gDAOAppUsageEvent.getId();
        }
        return null;
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public boolean hasKey(GDAOAppUsageEvent gDAOAppUsageEvent) {
        return gDAOAppUsageEvent.getId() != null;
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public Long readKey(Cursor cursor, int i) {
        int i2 = i + 0;
        if (cursor.isNull(i2)) {
            return null;
        }
        return Long.valueOf(cursor.getLong(i2));
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final Long updateKeyAfterInsert(GDAOAppUsageEvent gDAOAppUsageEvent, long j) {
        gDAOAppUsageEvent.setId(Long.valueOf(j));
        return Long.valueOf(j);
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final void bindValues(DatabaseStatement databaseStatement, GDAOAppUsageEvent gDAOAppUsageEvent) {
        databaseStatement.clearBindings();
        Long id = gDAOAppUsageEvent.getId();
        if (id != null) {
            databaseStatement.bindLong(1, id.longValue());
        }
        String session_id = gDAOAppUsageEvent.getSession_id();
        if (session_id != null) {
            databaseStatement.bindString(2, session_id);
        }
        String date = gDAOAppUsageEvent.getDate();
        if (date != null) {
            databaseStatement.bindString(3, date);
        }
        Boolean startup = gDAOAppUsageEvent.getStartup();
        if (startup != null) {
            databaseStatement.bindLong(4, startup.booleanValue() ? 1L : 0L);
        }
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public GDAOAppUsageEvent readEntity(Cursor cursor, int i) {
        int i2 = i + 0;
        Boolean bool = null;
        Long valueOf = cursor.isNull(i2) ? null : Long.valueOf(cursor.getLong(i2));
        int i3 = i + 1;
        String string = cursor.isNull(i3) ? null : cursor.getString(i3);
        int i4 = i + 2;
        String string2 = cursor.isNull(i4) ? null : cursor.getString(i4);
        int i5 = i + 3;
        if (!cursor.isNull(i5)) {
            bool = Boolean.valueOf(cursor.getShort(i5) != 0);
        }
        return new GDAOAppUsageEvent(valueOf, string, string2, bool);
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public void readEntity(Cursor cursor, GDAOAppUsageEvent gDAOAppUsageEvent, int i) {
        int i2 = i + 0;
        Boolean bool = null;
        gDAOAppUsageEvent.setId(cursor.isNull(i2) ? null : Long.valueOf(cursor.getLong(i2)));
        int i3 = i + 1;
        gDAOAppUsageEvent.setSession_id(cursor.isNull(i3) ? null : cursor.getString(i3));
        int i4 = i + 2;
        gDAOAppUsageEvent.setDate(cursor.isNull(i4) ? null : cursor.getString(i4));
        int i5 = i + 3;
        if (!cursor.isNull(i5)) {
            bool = Boolean.valueOf(cursor.getShort(i5) != 0);
        }
        gDAOAppUsageEvent.setStartup(bool);
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final void bindValues(SQLiteStatement sQLiteStatement, GDAOAppUsageEvent gDAOAppUsageEvent) {
        sQLiteStatement.clearBindings();
        Long id = gDAOAppUsageEvent.getId();
        if (id != null) {
            sQLiteStatement.bindLong(1, id.longValue());
        }
        String session_id = gDAOAppUsageEvent.getSession_id();
        if (session_id != null) {
            sQLiteStatement.bindString(2, session_id);
        }
        String date = gDAOAppUsageEvent.getDate();
        if (date != null) {
            sQLiteStatement.bindString(3, date);
        }
        Boolean startup = gDAOAppUsageEvent.getStartup();
        if (startup != null) {
            sQLiteStatement.bindLong(4, startup.booleanValue() ? 1L : 0L);
        }
    }
}
