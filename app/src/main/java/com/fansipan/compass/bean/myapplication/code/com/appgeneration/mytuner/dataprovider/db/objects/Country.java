package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.objects;

import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao.DaoSession;
import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao.GDAOCountry;
import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao.GDAOCountryDao;
import com.fansipan.compass.bean.myapplication.code.com.greendao.AbstractDao;
import com.fansipan.compass.bean.myapplication.code.com.greendao.Property;
import com.fansipan.compass.bean.myapplication.code.com.greendao.query.Query;
import com.fansipan.compass.bean.myapplication.code.com.greendao.query.QueryBuilder;
import com.fansipan.compass.bean.myapplication.code.com.greendao.query.WhereCondition;
import java.util.ArrayList;
import java.util.List;

/* loaded from: classes.dex */
public class Country implements NavigationEntityItem {
    private final GDAOCountry mDbCountry;

    public Country(GDAOCountry gDAOCountry) {
        this.mDbCountry = gDAOCountry;
    }

    private static List<Country> convertList(List<GDAOCountry> list) {
        if (list == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (GDAOCountry gDAOCountry : list) {
            if (gDAOCountry != null) {
                arrayList.add(new Country(gDAOCountry));
            }
        }
        return arrayList;
    }

    public static List<Country> getAll(DaoSession daoSession) {
        QueryBuilder<GDAOCountry> queryBuilder = daoSession.getGDAOCountryDao().queryBuilder();
        queryBuilder.whereCollector.add(GDAOCountryDao.Properties.Show_in_list.eq(Boolean.TRUE), new WhereCondition[0]);
        queryBuilder.orderAscOrDesc(" ASC", GDAOCountryDao.Properties.Name);
        return convertList(queryBuilder.list());
    }

    public static Country getByCountryCode(DaoSession daoSession, String str) {
        QueryBuilder<GDAOCountry> queryBuilder = daoSession.getGDAOCountryDao().queryBuilder();
        Property property = GDAOCountryDao.Properties.Code;
        property.getClass();
        queryBuilder.whereCollector.add(new WhereCondition.PropertyCondition(property, " LIKE ?", str), new WhereCondition[0]);
        Query<GDAOCountry> build = queryBuilder.build();
        build.checkThread();
        GDAOCountry gDAOCountry =
                (GDAOCountry) ((AbstractDao) build.dao).loadUniqueAndCloseCursor(build.dao.getDatabase().rawQuery(build.sql,
                        build.parameters));
        if (gDAOCountry != null) {
            return new Country(gDAOCountry);
        }
        return null;
    }

    public static Country getById(DaoSession daoSession, long j) {
        GDAOCountry loadByRowId = daoSession.getGDAOCountryDao().loadByRowId(j);
        if (loadByRowId != null) {
            return new Country(loadByRowId);
        }
        return null;
    }

    public static Country getClosestCountry(DaoSession daoSession, UserLocation userLocation) {
        City closest;
        if (userLocation == null || (closest = City.getClosest(daoSession, userLocation)) == null) {
            return null;
        }
        return closest.getCountryObject();
    }

    public String getCode() {
        return this.mDbCountry.getCode();
    }

    public boolean getEnabled() {
        return this.mDbCountry.getShow_in_list().booleanValue();
    }

    public String getFlagUrl() {
        return this.mDbCountry.getFlag_url();
    }

    public long getId() {
        return this.mDbCountry.getId().longValue();
    }

    public String getLocalFlagUrl() {
        return String.format("drawable/country_%s", getCode());
    }

    public String getName() {
        return this.mDbCountry.getName();
    }

    @Override // com.appgeneration.mytuner.dataprovider.db.objects.NavigationEntityItem
    public long getObjectId() {
        return getId();
    }

    public boolean getUseStates() {
        return this.mDbCountry.getUse_states().booleanValue();
    }
}
