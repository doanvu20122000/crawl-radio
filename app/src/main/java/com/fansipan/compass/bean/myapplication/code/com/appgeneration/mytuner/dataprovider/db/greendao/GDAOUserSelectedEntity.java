package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao;

import java.io.Serializable;

/* loaded from: classes.dex */
public class GDAOUserSelectedEntity implements Serializable {
    private Long id;
    private Integer n_ord;
    private Integer subtype;
    private Long timestamp;
    private Integer type;

    public GDAOUserSelectedEntity() {
    }

    public Long getId() {
        return this.id;
    }

    public Integer getN_ord() {
        return this.n_ord;
    }

    public Integer getSubtype() {
        return this.subtype;
    }

    public Long getTimestamp() {
        return this.timestamp;
    }

    public Integer getType() {
        return this.type;
    }

    public void setId(Long l) {
        this.id = l;
    }

    public void setN_ord(Integer num) {
        this.n_ord = num;
    }

    public void setSubtype(Integer num) {
        this.subtype = num;
    }

    public void setTimestamp(Long l) {
        this.timestamp = l;
    }

    public void setType(Integer num) {
        this.type = num;
    }

    public GDAOUserSelectedEntity(Long l, Integer num, Integer num2, Long l2, Integer num3) {
        this.id = l;
        this.type = num;
        this.subtype = num2;
        this.timestamp = l2;
        this.n_ord = num3;
    }
}
