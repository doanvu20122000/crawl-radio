package com.fansipan.compass.bean.myapplication.code.com.greendao.internal;

import com.fansipan.compass.bean.myapplication.code.com.greendao.database.Database;
import com.fansipan.compass.bean.myapplication.code.com.greendao.database.DatabaseStatement;

/* loaded from: classes4.dex */
public final class TableStatements {
    public final String[] allColumns;
    public DatabaseStatement countStatement;
    public final Database db;
    public DatabaseStatement deleteStatement;
    public DatabaseStatement insertOrReplaceStatement;
    public DatabaseStatement insertStatement;
    public final String[] pkColumns;
    public volatile String selectAll;
    public volatile String selectByKey;
    public volatile String selectByRowId;
    public final String tablename;
    public DatabaseStatement updateStatement;

    public TableStatements(Database database, String str, String[] strArr, String[] strArr2) {
        this.db = database;
        this.tablename = str;
        this.allColumns = strArr;
        this.pkColumns = strArr2;
    }

    public final DatabaseStatement getDeleteStatement() {
        if (this.deleteStatement == null) {
            DatabaseStatement compileStatement = this.db.compileStatement(SqlUtils.createSqlDelete(this.tablename, this.pkColumns));
            synchronized (this) {
                if (this.deleteStatement == null) {
                    this.deleteStatement = compileStatement;
                }
            }
            if (this.deleteStatement != compileStatement) {
                compileStatement.close();
            }
        }
        return this.deleteStatement;
    }

    public final DatabaseStatement getInsertOrReplaceStatement() {
        if (this.insertOrReplaceStatement == null) {
            DatabaseStatement compileStatement = this.db.compileStatement(SqlUtils.createSqlInsert("INSERT OR REPLACE INTO ", this.tablename, this.allColumns));
            synchronized (this) {
                if (this.insertOrReplaceStatement == null) {
                    this.insertOrReplaceStatement = compileStatement;
                }
            }
            if (this.insertOrReplaceStatement != compileStatement) {
                compileStatement.close();
            }
        }
        return this.insertOrReplaceStatement;
    }

    public final DatabaseStatement getInsertStatement() {
        if (this.insertStatement == null) {
            DatabaseStatement compileStatement = this.db.compileStatement(SqlUtils.createSqlInsert("INSERT INTO ", this.tablename, this.allColumns));
            synchronized (this) {
                if (this.insertStatement == null) {
                    this.insertStatement = compileStatement;
                }
            }
            if (this.insertStatement != compileStatement) {
                compileStatement.close();
            }
        }
        return this.insertStatement;
    }

    public final String getSelectAll() {
        if (this.selectAll == null) {
            this.selectAll = SqlUtils.createSqlSelect(this.tablename, this.allColumns, false);
        }
        return this.selectAll;
    }

    public final String getSelectByKey() {
        if (this.selectByKey == null) {
            StringBuilder sb = new StringBuilder(getSelectAll());
            sb.append("WHERE ");
            this.selectByKey = sb.toString();
        }
        return this.selectByKey;
    }

    public final DatabaseStatement getUpdateStatement() {
        if (this.updateStatement == null) {
            String str = this.tablename;
            String[] strArr = this.allColumns;
            String[] strArr2 = this.pkColumns;
            int i = SqlUtils.$r8$clinit;
            String str2 = '\"' + str + '\"';
            StringBuilder sb = new StringBuilder("UPDATE ");
            sb.append(str2);
            sb.append(" SET ");
            for (int i2 = 0; i2 < strArr.length; i2++) {
                String str3 = strArr[i2];
                sb.append('\"');
                sb.append(str3);
                sb.append('\"');
                sb.append("=?");
                if (i2 < strArr.length - 1) {
                    sb.append(',');
                }
            }
            sb.append(" WHERE ");
            SqlUtils.appendColumnsEqValue(sb, str2, strArr2);
            DatabaseStatement compileStatement = this.db.compileStatement(sb.toString());
            synchronized (this) {
                if (this.updateStatement == null) {
                    this.updateStatement = compileStatement;
                }
            }
            if (this.updateStatement != compileStatement) {
                compileStatement.close();
            }
        }
        return this.updateStatement;
    }
}
