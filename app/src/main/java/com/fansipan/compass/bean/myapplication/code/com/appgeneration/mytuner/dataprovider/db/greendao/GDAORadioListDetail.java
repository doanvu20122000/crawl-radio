package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao;

import java.io.Serializable;

/* loaded from: classes.dex */
public class GDAORadioListDetail implements Serializable {

    /* renamed from: radio  reason: collision with root package name */
    private Long f537radio;
    private Long radio_list;
    private Long rank;

    public GDAORadioListDetail() {
    }

    public Long getRadio() {
        return this.f537radio;
    }

    public Long getRadio_list() {
        return this.radio_list;
    }

    public Long getRank() {
        return this.rank;
    }

    public void setRadio(Long l) {
        this.f537radio = l;
    }

    public void setRadio_list(Long l) {
        this.radio_list = l;
    }

    public void setRank(Long l) {
        this.rank = l;
    }

    public GDAORadioListDetail(Long l, Long l2, Long l3) {
        this.rank = l;
        this.f537radio = l2;
        this.radio_list = l3;
    }
}
