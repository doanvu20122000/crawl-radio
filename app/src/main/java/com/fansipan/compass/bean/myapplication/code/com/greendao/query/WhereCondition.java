package com.fansipan.compass.bean.myapplication.code.com.greendao.query;

import com.fansipan.compass.bean.myapplication.code.com.greendao.DaoException;
import com.fansipan.compass.bean.myapplication.code.com.greendao.Property;
import com.fansipan.compass.bean.myapplication.code.com.greendao.internal.SqlUtils;

import java.util.ArrayList;
import java.util.Date;

/* loaded from: classes4.dex */
public interface WhereCondition {
    void appendTo(String str, StringBuilder sb);

    void appendValuesTo(ArrayList arrayList);

    /* loaded from: classes4.dex */
    public static abstract class AbstractCondition implements WhereCondition {
        public final boolean hasSingleValue;
        public final Object value;
        public final Object[] values;

        public AbstractCondition(Object obj) {
            this.value = obj;
            this.hasSingleValue = true;
            this.values = null;
        }

        @Override // org.greenrobot.greendao.query.WhereCondition
        public final void appendValuesTo(ArrayList arrayList) {
            if (this.hasSingleValue) {
                arrayList.add(this.value);
                return;
            }
            Object[] objArr = this.values;
            if (objArr != null) {
                for (Object obj : objArr) {
                    arrayList.add(obj);
                }
            }
        }

        public AbstractCondition(Object[] objArr) {
            this.value = null;
            this.hasSingleValue = false;
            this.values = objArr;
        }
    }

    /* loaded from: classes4.dex */
    public static class PropertyCondition extends AbstractCondition {
        public final String op;
        public final Property property;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public PropertyCondition(Property property, String str, Object[] objArr) {
            super(objArr);
            for (int i = 0; i < objArr.length; i++) {
                objArr[i] = checkValueForType(property, objArr[i]);
            }
            this.property = property;
            this.op = str;
        }

        public static Object checkValueForType(Property property, Object obj) {
            if (obj != null && obj.getClass().isArray()) {
                throw new DaoException("Illegal value: found array, but simple object required");
            }
            Class<?> cls = property.type;
            if (cls == Date.class) {
                if (obj instanceof Date) {
                    return Long.valueOf(((Date) obj).getTime());
                }
                if (obj instanceof Long) {
                    return obj;
                }
                throw new DaoException("Illegal date value: expected java.util.Date or Long for value " + obj);
            }
            if (cls == Boolean.TYPE || cls == Boolean.class) {
                if (obj instanceof Boolean) {
                    return Integer.valueOf(((Boolean) obj).booleanValue() ? 1 : 0);
                }
                if (obj instanceof Number) {
                    int intValue = ((Number) obj).intValue();
                    if (intValue != 0 && intValue != 1) {
                        throw new DaoException("Illegal boolean value: numbers must be 0 or 1, but was " + obj);
                    }
                } else if (obj instanceof String) {
                    String str = (String) obj;
                    if ("TRUE".equalsIgnoreCase(str)) {
                        return 1;
                    }
                    if ("FALSE".equalsIgnoreCase(str)) {
                        return 0;
                    }
                    throw new DaoException("Illegal boolean value: Strings must be \"TRUE\" or \"FALSE\" (case insensitive), but was " + obj);
                }
            }
            return obj;
        }

        @Override // org.greenrobot.greendao.query.WhereCondition
        public final void appendTo(String str, StringBuilder sb) {
            SqlUtils.appendProperty(sb, str, this.property);
            sb.append(this.op);
        }

        public PropertyCondition(Property property, String str, Object obj) {
            super(checkValueForType(property, obj));
            this.property = property;
            this.op = str;
        }
    }
}
