package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import com.fansipan.compass.bean.myapplication.code.com.greendao.AbstractDao;
import com.fansipan.compass.bean.myapplication.code.com.greendao.Property;
import com.fansipan.compass.bean.myapplication.code.com.greendao.database.Database;
import com.fansipan.compass.bean.myapplication.code.com.greendao.database.DatabaseStatement;
import com.fansipan.compass.bean.myapplication.code.com.greendao.internal.DaoConfig;

/* loaded from: classes.dex */
public class GDAOAppSongEventDao extends AbstractDao<GDAOAppSongEvent, Long> {
    public static final String TABLENAME = "app_song_events";

    /* loaded from: classes.dex */
    public static class Properties {
        public static final Property Id = new Property(0, Long.class, "id", true, "ID");
        public static final Property Radio = new Property(1, Long.class, "radio", false, "RADIO");
        public static final Property Song = new Property(2, Long.class, "song", false, "SONG");
        public static final Property Metadata = new Property(3, String.class, "metadata", false, "METADATA");
        public static final Property Start_date = new Property(4, String.class, "start_date", false, "START_DATE");
        public static final Property End_date = new Property(5, String.class, "end_date", false, "END_DATE");
        public static final Property Was_zapping = new Property(6, Boolean.class, "was_zapping", false, "WAS_ZAPPING");
        public static final Property Increased_volume = new Property(7, Boolean.class, "increased_volume", false, "INCREASED_VOLUME");
    }

    public GDAOAppSongEventDao(DaoConfig daoConfig) {
        super(daoConfig);
    }

    public static void createTable(Database database, boolean z) {
        String str;
        if (z) {
            str = "IF NOT EXISTS ";
        } else {
            str = "";
        }
        database.execSQL("CREATE TABLE " + str + "\"app_song_events\" (\"ID\" INTEGER PRIMARY KEY AUTOINCREMENT ,\"RADIO\" INTEGER,\"SONG\" INTEGER,\"METADATA\" TEXT,\"START_DATE\" TEXT,\"END_DATE\" TEXT,\"WAS_ZAPPING\" INTEGER,\"INCREASED_VOLUME\" INTEGER);");
    }

    public static void dropTable(Database database, boolean z) {
        String str;
        StringBuilder m = new StringBuilder("DROP TABLE ");
        if (z) {
            str = "IF EXISTS ";
        } else {
            str = "";
        }
        m.append(str);
        m.append("\"app_song_events\"");
        database.execSQL(m.toString());
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final boolean isEntityUpdateable() {
        return true;
    }

    public GDAOAppSongEventDao(DaoConfig daoConfig, DaoSession daoSession) {
        super(daoConfig, daoSession);
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public Long getKey(GDAOAppSongEvent gDAOAppSongEvent) {
        if (gDAOAppSongEvent != null) {
            return gDAOAppSongEvent.getId();
        }
        return null;
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public boolean hasKey(GDAOAppSongEvent gDAOAppSongEvent) {
        return gDAOAppSongEvent.getId() != null;
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public Long readKey(Cursor cursor, int i) {
        int i2 = i + 0;
        if (cursor.isNull(i2)) {
            return null;
        }
        return Long.valueOf(cursor.getLong(i2));
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final Long updateKeyAfterInsert(GDAOAppSongEvent gDAOAppSongEvent, long j) {
        gDAOAppSongEvent.setId(Long.valueOf(j));
        return Long.valueOf(j);
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final void bindValues(DatabaseStatement databaseStatement, GDAOAppSongEvent gDAOAppSongEvent) {
        databaseStatement.clearBindings();
        Long id = gDAOAppSongEvent.getId();
        if (id != null) {
            databaseStatement.bindLong(1, id.longValue());
        }
        Long radio2 = gDAOAppSongEvent.getRadio();
        if (radio2 != null) {
            databaseStatement.bindLong(2, radio2.longValue());
        }
        Long song = gDAOAppSongEvent.getSong();
        if (song != null) {
            databaseStatement.bindLong(3, song.longValue());
        }
        String metadata = gDAOAppSongEvent.getMetadata();
        if (metadata != null) {
            databaseStatement.bindString(4, metadata);
        }
        String start_date = gDAOAppSongEvent.getStart_date();
        if (start_date != null) {
            databaseStatement.bindString(5, start_date);
        }
        String end_date = gDAOAppSongEvent.getEnd_date();
        if (end_date != null) {
            databaseStatement.bindString(6, end_date);
        }
        Boolean was_zapping = gDAOAppSongEvent.getWas_zapping();
        if (was_zapping != null) {
            databaseStatement.bindLong(7, was_zapping.booleanValue() ? 1L : 0L);
        }
        Boolean increased_volume = gDAOAppSongEvent.getIncreased_volume();
        if (increased_volume != null) {
            databaseStatement.bindLong(8, increased_volume.booleanValue() ? 1L : 0L);
        }
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public GDAOAppSongEvent readEntity(Cursor cursor, int i) {
        Boolean valueOf;
        Boolean valueOf2;
        int i2 = i + 0;
        Long valueOf3 = cursor.isNull(i2) ? null : Long.valueOf(cursor.getLong(i2));
        int i3 = i + 1;
        Long valueOf4 = cursor.isNull(i3) ? null : Long.valueOf(cursor.getLong(i3));
        int i4 = i + 2;
        Long valueOf5 = cursor.isNull(i4) ? null : Long.valueOf(cursor.getLong(i4));
        int i5 = i + 3;
        String string = cursor.isNull(i5) ? null : cursor.getString(i5);
        int i6 = i + 4;
        String string2 = cursor.isNull(i6) ? null : cursor.getString(i6);
        int i7 = i + 5;
        String string3 = cursor.isNull(i7) ? null : cursor.getString(i7);
        int i8 = i + 6;
        if (cursor.isNull(i8)) {
            valueOf = null;
        } else {
            valueOf = Boolean.valueOf(cursor.getShort(i8) != 0);
        }
        int i9 = i + 7;
        if (cursor.isNull(i9)) {
            valueOf2 = null;
        } else {
            valueOf2 = Boolean.valueOf(cursor.getShort(i9) != 0);
        }
        return new GDAOAppSongEvent(valueOf3, valueOf4, valueOf5, string, string2, string3, valueOf, valueOf2);
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public void readEntity(Cursor cursor, GDAOAppSongEvent gDAOAppSongEvent, int i) {
        Boolean valueOf;
        int i2 = i + 0;
        Boolean bool = null;
        gDAOAppSongEvent.setId(cursor.isNull(i2) ? null : Long.valueOf(cursor.getLong(i2)));
        int i3 = i + 1;
        gDAOAppSongEvent.setRadio(cursor.isNull(i3) ? null : Long.valueOf(cursor.getLong(i3)));
        int i4 = i + 2;
        gDAOAppSongEvent.setSong(cursor.isNull(i4) ? null : Long.valueOf(cursor.getLong(i4)));
        int i5 = i + 3;
        gDAOAppSongEvent.setMetadata(cursor.isNull(i5) ? null : cursor.getString(i5));
        int i6 = i + 4;
        gDAOAppSongEvent.setStart_date(cursor.isNull(i6) ? null : cursor.getString(i6));
        int i7 = i + 5;
        gDAOAppSongEvent.setEnd_date(cursor.isNull(i7) ? null : cursor.getString(i7));
        int i8 = i + 6;
        if (cursor.isNull(i8)) {
            valueOf = null;
        } else {
            valueOf = Boolean.valueOf(cursor.getShort(i8) != 0);
        }
        gDAOAppSongEvent.setWas_zapping(valueOf);
        int i9 = i + 7;
        if (!cursor.isNull(i9)) {
            bool = Boolean.valueOf(cursor.getShort(i9) != 0);
        }
        gDAOAppSongEvent.setIncreased_volume(bool);
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final void bindValues(SQLiteStatement sQLiteStatement, GDAOAppSongEvent gDAOAppSongEvent) {
        sQLiteStatement.clearBindings();
        Long id = gDAOAppSongEvent.getId();
        if (id != null) {
            sQLiteStatement.bindLong(1, id.longValue());
        }
        Long radio2 = gDAOAppSongEvent.getRadio();
        if (radio2 != null) {
            sQLiteStatement.bindLong(2, radio2.longValue());
        }
        Long song = gDAOAppSongEvent.getSong();
        if (song != null) {
            sQLiteStatement.bindLong(3, song.longValue());
        }
        String metadata = gDAOAppSongEvent.getMetadata();
        if (metadata != null) {
            sQLiteStatement.bindString(4, metadata);
        }
        String start_date = gDAOAppSongEvent.getStart_date();
        if (start_date != null) {
            sQLiteStatement.bindString(5, start_date);
        }
        String end_date = gDAOAppSongEvent.getEnd_date();
        if (end_date != null) {
            sQLiteStatement.bindString(6, end_date);
        }
        Boolean was_zapping = gDAOAppSongEvent.getWas_zapping();
        if (was_zapping != null) {
            sQLiteStatement.bindLong(7, was_zapping.booleanValue() ? 1L : 0L);
        }
        Boolean increased_volume = gDAOAppSongEvent.getIncreased_volume();
        if (increased_volume != null) {
            sQLiteStatement.bindLong(8, increased_volume.booleanValue() ? 1L : 0L);
        }
    }
}
