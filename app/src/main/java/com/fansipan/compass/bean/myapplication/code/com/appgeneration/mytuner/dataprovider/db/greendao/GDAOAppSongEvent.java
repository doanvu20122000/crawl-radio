package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao;

import java.io.Serializable;

/* loaded from: classes.dex */
public class GDAOAppSongEvent implements Serializable {
    private String end_date;
    private Long id;
    private Boolean increased_volume;
    private String metadata;

    /* renamed from: radio  reason: collision with root package name */
    private Long f534radio;
    private Long song;
    private String start_date;
    private Boolean was_zapping;

    public GDAOAppSongEvent() {
    }

    public String getEnd_date() {
        return this.end_date;
    }

    public Long getId() {
        return this.id;
    }

    public Boolean getIncreased_volume() {
        return this.increased_volume;
    }

    public String getMetadata() {
        return this.metadata;
    }

    public Long getRadio() {
        return this.f534radio;
    }

    public Long getSong() {
        return this.song;
    }

    public String getStart_date() {
        return this.start_date;
    }

    public Boolean getWas_zapping() {
        return this.was_zapping;
    }

    public void setEnd_date(String str) {
        this.end_date = str;
    }

    public void setId(Long l) {
        this.id = l;
    }

    public void setIncreased_volume(Boolean bool) {
        this.increased_volume = bool;
    }

    public void setMetadata(String str) {
        this.metadata = str;
    }

    public void setRadio(Long l) {
        this.f534radio = l;
    }

    public void setSong(Long l) {
        this.song = l;
    }

    public void setStart_date(String str) {
        this.start_date = str;
    }

    public void setWas_zapping(Boolean bool) {
        this.was_zapping = bool;
    }

    public GDAOAppSongEvent(Long l) {
        this.id = l;
    }

    public GDAOAppSongEvent(Long l, Long l2, Long l3, String str, String str2, String str3, Boolean bool, Boolean bool2) {
        this.id = l;
        this.f534radio = l2;
        this.song = l3;
        this.metadata = str;
        this.start_date = str2;
        this.end_date = str3;
        this.was_zapping = bool;
        this.increased_volume = bool2;
    }
}
