package com.fansipan.compass.bean.myapplication.code.com.greendao.identityscope;

import com.fansipan.compass.bean.myapplication.code.com.greendao.internal.LongHashMap;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.concurrent.locks.ReentrantLock;

/* loaded from: classes4.dex */
public final class IdentityScopeLong<T> implements IdentityScope<Long, T> {
    public final LongHashMap<Reference<T>> map = new LongHashMap<>();
    public final ReentrantLock lock = new ReentrantLock();

    @Override // org.greenrobot.greendao.identityscope.IdentityScope
    public final void clear() {
        this.lock.lock();
        try {
            LongHashMap<Reference<T>> longHashMap = this.map;
            longHashMap.size = 0;
            Arrays.fill(longHashMap.table, (Object) null);
        } finally {
            this.lock.unlock();
        }
    }

    @Override // org.greenrobot.greendao.identityscope.IdentityScope
    public final boolean detach(Long l, Object obj) {
        boolean z;
        Long l2 = l;
        this.lock.lock();
        try {
            if (get2(l2.longValue()) == obj && obj != null) {
                this.lock.lock();
                this.map.remove(l2.longValue());
                this.lock.unlock();
                z = true;
            } else {
                z = false;
            }
            return z;
        } finally {
            this.lock.unlock();
        }
    }

    @Override // org.greenrobot.greendao.identityscope.IdentityScope
    public final T get(Long l) {
        return get2(l.longValue());
    }

    public final T get2(long j) {
        this.lock.lock();
        try {
            Reference<T> reference = this.map.get(j);
            if (reference != null) {
                return reference.get();
            }
            return null;
        } finally {
            this.lock.unlock();
        }
    }

    @Override // org.greenrobot.greendao.identityscope.IdentityScope
    public final T getNoLock(Long l) {
        Reference<T> reference = this.map.get(l.longValue());
        if (reference != null) {
            return reference.get();
        }
        return null;
    }

    @Override // org.greenrobot.greendao.identityscope.IdentityScope
    public final void lock() {
        this.lock.lock();
    }

    @Override // org.greenrobot.greendao.identityscope.IdentityScope
    public final void put(Long l, Object obj) {
        long longValue = l.longValue();
        this.lock.lock();
        try {
            this.map.put(longValue, new WeakReference(obj));
        } finally {
            this.lock.unlock();
        }
    }

    @Override // org.greenrobot.greendao.identityscope.IdentityScope
    public final void putNoLock(Long l, Object obj) {
        this.map.put(l.longValue(), new WeakReference(obj));
    }

    @Override // org.greenrobot.greendao.identityscope.IdentityScope
    public final void remove(Long l) {
        Long l2 = l;
        this.lock.lock();
        try {
            this.map.remove(l2.longValue());
        } finally {
            this.lock.unlock();
        }
    }

    @Override // org.greenrobot.greendao.identityscope.IdentityScope
    public final void reserveRoom(int i) {
        LongHashMap<Reference<T>> longHashMap = this.map;
        longHashMap.getClass();
        longHashMap.setCapacity((i * 5) / 3);
    }

    @Override // org.greenrobot.greendao.identityscope.IdentityScope
    public final void unlock() {
        this.lock.unlock();
    }

    @Override // org.greenrobot.greendao.identityscope.IdentityScope
    public final void remove(ArrayList arrayList) {
        this.lock.lock();
        try {
            Iterator<T> it = arrayList.iterator();
            while (it.hasNext()) {
                this.map.remove(((Long) it.next()).longValue());
            }
        } finally {
            this.lock.unlock();
        }
    }
}
