package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.objects;

import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao.DaoSession;
import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao.GDAORadio;
import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao.GDAORadioDao;
import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao.GDAORadioList;
import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao.GDAORadioListDao;
import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao.GDAORadioListDetail;
import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao.GDAORadioListDetailDao;
import com.fansipan.compass.bean.myapplication.code.com.greendao.query.QueryBuilder;
import com.fansipan.compass.bean.myapplication.code.com.greendao.query.WhereCondition;

import java.util.ArrayList;
import java.util.List;

/* loaded from: classes.dex */
public class RadioList implements NavigationEntityItem {
    private final GDAORadioList mDbRadioList;

    private RadioList(GDAORadioList gDAORadioList) {
        this.mDbRadioList = gDAORadioList;
    }

    private static List<RadioList> convertList(List<GDAORadioList> list) {
        if (list == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (GDAORadioList gDAORadioList : list) {
            if (gDAORadioList != null) {
                arrayList.add(new RadioList(gDAORadioList));
            }
        }
        return arrayList;
    }

    public static RadioList get(DaoSession daoSession, long j) {
        GDAORadioList loadByRowId = daoSession.getGDAORadioListDao().loadByRowId(j);
        if (loadByRowId != null) {
            return new RadioList(loadByRowId);
        }
        return null;
    }

    public static List<RadioList> getAll(DaoSession daoSession) {
        QueryBuilder<GDAORadioList> queryBuilder = daoSession.getGDAORadioListDao().queryBuilder();
        queryBuilder.orderAscOrDesc(" ASC", GDAORadioListDao.Properties.Rank);
        return convertList(queryBuilder.list());
    }

    public static List<RadioList> getAllWithRadios(DaoSession daoSession) {
        QueryBuilder<GDAORadioList> queryBuilder = daoSession.getGDAORadioListDao().queryBuilder();
        queryBuilder.orderAscOrDesc(" ASC", GDAORadioListDao.Properties.Rank);
        queryBuilder.distinct = true;
        queryBuilder.join(queryBuilder.join(GDAORadioListDao.Properties.Id, GDAORadioListDetail.class, GDAORadioListDetailDao.Properties.Radio_list), GDAORadioListDetailDao.Properties.Radio, GDAORadio.class, GDAORadioDao.Properties.Id).where(GDAORadioDao.Properties.Hidden.eq(Boolean.FALSE), new WhereCondition[0]);
        return convertList(queryBuilder.list());
    }

    public static boolean hasStates(DaoSession daoSession) {
        if (daoSession.getGDAORadioListDao().count() > 1) {
            return true;
        }
        return false;
    }

    public long getId() {
        return this.mDbRadioList.getId().longValue();
    }

    public String getName() {
        return this.mDbRadioList.getName();
    }

    @Override // com.appgeneration.mytuner.dataprovider.db.objects.NavigationEntityItem
    public long getObjectId() {
        return getId();
    }

    public long getRank() {
        return this.mDbRadioList.getRank().intValue();
    }
}
