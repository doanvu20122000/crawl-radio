package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao;

import com.fansipan.compass.bean.myapplication.code.com.greendao.DaoException;

import java.io.Serializable;

/* loaded from: classes.dex */
public class GDAOCity implements Serializable {
    private Long country;
    private GDAOCountry countryObject;
    private transient Long countryObject__resolvedKey;
    private transient DaoSession daoSession;
    private Long id;
    private Float latitude;
    private Float longitude;
    private transient GDAOCityDao myDao;
    private String name;
    private Long state;
    private GDAOState stateObject;
    private transient Long stateObject__resolvedKey;

    public GDAOCity() {
    }

    private void __throwIfDetached() {
        if (this.myDao != null) {
            return;
        }
        throw new DaoException("Entity is detached from DAO context");
    }

    public void __setDaoSession(DaoSession daoSession) {
        GDAOCityDao gDAOCityDao;
        this.daoSession = daoSession;
        if (daoSession != null) {
            gDAOCityDao = daoSession.getGDAOCityDao();
        } else {
            gDAOCityDao = null;
        }
        this.myDao = gDAOCityDao;
    }

    public void delete() {
        __throwIfDetached();
        this.myDao.delete(this);
    }

    public Long getCountry() {
        return this.country;
    }

    public GDAOCountry getCountryObject() {
        Long l = this.country;
        Long l2 = this.countryObject__resolvedKey;
        if (l2 == null || !l2.equals(l)) {
            __throwIfDetached();
            GDAOCountry load = this.daoSession.getGDAOCountryDao().load(l);
            synchronized (this) {
                this.countryObject = load;
                this.countryObject__resolvedKey = l;
            }
        }
        return this.countryObject;
    }

    public Long getId() {
        return this.id;
    }

    public Float getLatitude() {
        return this.latitude;
    }

    public Float getLongitude() {
        return this.longitude;
    }

    public String getName() {
        return this.name;
    }

    public Long getState() {
        return this.state;
    }

    public GDAOState getStateObject() {
        Long l = this.state;
        Long l2 = this.stateObject__resolvedKey;
        if (l2 == null || !l2.equals(l)) {
            __throwIfDetached();
            GDAOState load = this.daoSession.getGDAOStateDao().load(l);
            synchronized (this) {
                this.stateObject = load;
                this.stateObject__resolvedKey = l;
            }
        }
        return this.stateObject;
    }

    public void refresh() {
        __throwIfDetached();
        this.myDao.refresh(this);
    }

    public void setCountry(Long l) {
        this.country = l;
    }

    public void setCountryObject(GDAOCountry gDAOCountry) {
        Long id;
        synchronized (this) {
            this.countryObject = gDAOCountry;
            if (gDAOCountry == null) {
                id = null;
            } else {
                id = gDAOCountry.getId();
            }
            this.country = id;
            this.countryObject__resolvedKey = id;
        }
    }

    public void setId(Long l) {
        this.id = l;
    }

    public void setLatitude(Float f2) {
        this.latitude = f2;
    }

    public void setLongitude(Float f2) {
        this.longitude = f2;
    }

    public void setName(String str) {
        this.name = str;
    }

    public void setState(Long l) {
        this.state = l;
    }

    public void setStateObject(GDAOState gDAOState) {
        Long id;
        synchronized (this) {
            this.stateObject = gDAOState;
            if (gDAOState == null) {
                id = null;
            } else {
                id = gDAOState.getId();
            }
            this.state = id;
            this.stateObject__resolvedKey = id;
        }
    }

    public void update() {
        __throwIfDetached();
        this.myDao.update(this);
    }

    public GDAOCity(Long l) {
        this.id = l;
    }

    public GDAOCity(Long l, String str, Float f2, Float f3, Long l2, Long l3) {
        this.id = l;
        this.name = str;
        this.latitude = f2;
        this.longitude = f3;
        this.state = l2;
        this.country = l3;
    }
}
