package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import com.fansipan.compass.bean.myapplication.code.com.POBRequest$AdPosition$EnumUnboxingLocalUtility;
import com.fansipan.compass.bean.myapplication.code.com.greendao.AbstractDao;
import com.fansipan.compass.bean.myapplication.code.com.greendao.Property;
import com.fansipan.compass.bean.myapplication.code.com.greendao.database.Database;
import com.fansipan.compass.bean.myapplication.code.com.greendao.database.DatabaseStatement;
import com.fansipan.compass.bean.myapplication.code.com.greendao.internal.DaoConfig;

/* loaded from: classes.dex */
public class GDAOAppVolumeChangeEventDao extends AbstractDao<GDAOAppVolumeChangeEvent, Long> {
    public static final String TABLENAME = "app_volume_change_events";

    /* loaded from: classes.dex */
    public static class Properties {
        public static final Property Id = new Property(0, Long.class, "id", true, "ID");
        public static final Property Start_date = new Property(1, String.class, "start_date", false, "START_DATE");
        public static final Property Start_volume = new Property(2, Float.class, "start_volume", false, "START_VOLUME");
        public static final Property End_volume = new Property(3, Float.class, "end_volume", false, "END_VOLUME");
    }

    public GDAOAppVolumeChangeEventDao(DaoConfig daoConfig) {
        super(daoConfig);
    }

    public static void createTable(Database database, boolean z) {
        String str;
        if (z) {
            str = "IF NOT EXISTS ";
        } else {
            str = "";
        }
        database.execSQL("CREATE TABLE " + str + "\"app_volume_change_events\" (\"ID\" INTEGER PRIMARY KEY AUTOINCREMENT ,\"START_DATE\" TEXT,\"START_VOLUME\" REAL,\"END_VOLUME\" REAL);");
    }

    public static void dropTable(Database database, boolean z) {
        String str;
        StringBuilder m = new StringBuilder("DROP TABLE ");
        if (z) {
            str = "IF EXISTS ";
        } else {
            str = "";
        }
        POBRequest$AdPosition$EnumUnboxingLocalUtility.m(m, str, "\"app_volume_change_events\"", database);
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final boolean isEntityUpdateable() {
        return true;
    }

    public GDAOAppVolumeChangeEventDao(DaoConfig daoConfig, DaoSession daoSession) {
        super(daoConfig, daoSession);
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public Long getKey(GDAOAppVolumeChangeEvent gDAOAppVolumeChangeEvent) {
        if (gDAOAppVolumeChangeEvent != null) {
            return gDAOAppVolumeChangeEvent.getId();
        }
        return null;
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public boolean hasKey(GDAOAppVolumeChangeEvent gDAOAppVolumeChangeEvent) {
        return gDAOAppVolumeChangeEvent.getId() != null;
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public Long readKey(Cursor cursor, int i) {
        int i2 = i + 0;
        if (cursor.isNull(i2)) {
            return null;
        }
        return Long.valueOf(cursor.getLong(i2));
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final Long updateKeyAfterInsert(GDAOAppVolumeChangeEvent gDAOAppVolumeChangeEvent, long j) {
        gDAOAppVolumeChangeEvent.setId(Long.valueOf(j));
        return Long.valueOf(j);
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final void bindValues(DatabaseStatement databaseStatement, GDAOAppVolumeChangeEvent gDAOAppVolumeChangeEvent) {
        databaseStatement.clearBindings();
        Long id = gDAOAppVolumeChangeEvent.getId();
        if (id != null) {
            databaseStatement.bindLong(1, id.longValue());
        }
        String start_date = gDAOAppVolumeChangeEvent.getStart_date();
        if (start_date != null) {
            databaseStatement.bindString(2, start_date);
        }
        Float start_volume = gDAOAppVolumeChangeEvent.getStart_volume();
        if (start_volume != null) {
            databaseStatement.bindDouble(3, start_volume.floatValue());
        }
        Float end_volume = gDAOAppVolumeChangeEvent.getEnd_volume();
        if (end_volume != null) {
            databaseStatement.bindDouble(4, end_volume.floatValue());
        }
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public GDAOAppVolumeChangeEvent readEntity(Cursor cursor, int i) {
        int i2 = i + 0;
        int i3 = i + 1;
        int i4 = i + 2;
        int i5 = i + 3;
        return new GDAOAppVolumeChangeEvent(cursor.isNull(i2) ? null : Long.valueOf(cursor.getLong(i2)), cursor.isNull(i3) ? null : cursor.getString(i3), cursor.isNull(i4) ? null : Float.valueOf(cursor.getFloat(i4)), cursor.isNull(i5) ? null : Float.valueOf(cursor.getFloat(i5)));
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public void readEntity(Cursor cursor, GDAOAppVolumeChangeEvent gDAOAppVolumeChangeEvent, int i) {
        int i2 = i + 0;
        gDAOAppVolumeChangeEvent.setId(cursor.isNull(i2) ? null : Long.valueOf(cursor.getLong(i2)));
        int i3 = i + 1;
        gDAOAppVolumeChangeEvent.setStart_date(cursor.isNull(i3) ? null : cursor.getString(i3));
        int i4 = i + 2;
        gDAOAppVolumeChangeEvent.setStart_volume(cursor.isNull(i4) ? null : Float.valueOf(cursor.getFloat(i4)));
        int i5 = i + 3;
        gDAOAppVolumeChangeEvent.setEnd_volume(cursor.isNull(i5) ? null : Float.valueOf(cursor.getFloat(i5)));
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final void bindValues(SQLiteStatement sQLiteStatement, GDAOAppVolumeChangeEvent gDAOAppVolumeChangeEvent) {
        sQLiteStatement.clearBindings();
        Long id = gDAOAppVolumeChangeEvent.getId();
        if (id != null) {
            sQLiteStatement.bindLong(1, id.longValue());
        }
        String start_date = gDAOAppVolumeChangeEvent.getStart_date();
        if (start_date != null) {
            sQLiteStatement.bindString(2, start_date);
        }
        Float start_volume = gDAOAppVolumeChangeEvent.getStart_volume();
        if (start_volume != null) {
            sQLiteStatement.bindDouble(3, start_volume.floatValue());
        }
        Float end_volume = gDAOAppVolumeChangeEvent.getEnd_volume();
        if (end_volume != null) {
            sQLiteStatement.bindDouble(4, end_volume.floatValue());
        }
    }
}
