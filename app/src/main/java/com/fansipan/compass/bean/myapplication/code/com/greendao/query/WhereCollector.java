package com.fansipan.compass.bean.myapplication.code.com.greendao.query;

import com.fansipan.compass.bean.myapplication.code.com.greendao.AbstractDao;
import com.fansipan.compass.bean.myapplication.code.com.greendao.DaoException;
import com.fansipan.compass.bean.myapplication.code.com.greendao.Property;

import java.util.ArrayList;
import java.util.ListIterator;

/* loaded from: classes4.dex */
public final class WhereCollector<T> {
    public final AbstractDao<T, ?> dao;
    public final ArrayList whereConditions = new ArrayList();

    public WhereCollector(AbstractDao abstractDao) {
        this.dao = abstractDao;
    }

    public final void add(WhereCondition.PropertyCondition propertyCondition, WhereCondition... whereConditionArr) {
        checkProperty(propertyCondition.property);
        this.whereConditions.add(propertyCondition);
        for (WhereCondition whereCondition : whereConditionArr) {
            if (whereCondition instanceof WhereCondition.PropertyCondition) {
                checkProperty(((WhereCondition.PropertyCondition) whereCondition).property);
            }
            this.whereConditions.add(whereCondition);
        }
    }

    public final void appendWhereClause(StringBuilder sb, String str, ArrayList arrayList) {
        ListIterator listIterator = this.whereConditions.listIterator();
        while (listIterator.hasNext()) {
            if (listIterator.hasPrevious()) {
                sb.append(" AND ");
            }
            WhereCondition whereCondition = (WhereCondition) listIterator.next();
            whereCondition.appendTo(str, sb);
            whereCondition.appendValuesTo(arrayList);
        }
    }

    public final void checkProperty(Property property) {
        AbstractDao<T, ?> abstractDao = this.dao;
        if (abstractDao != null) {
            Property[] properties = abstractDao.getProperties();
            int length = properties.length;
            boolean z = false;
            int i = 0;
            while (true) {
                if (i >= length) {
                    break;
                } else if (property == properties[i]) {
                    z = true;
                    break;
                } else {
                    i++;
                }
            }
            if (!z) {
                StringBuilder m = new StringBuilder("Property '");
                m.append(property.name);
                m.append("' is not part of ");
                m.append(this.dao);
                throw new DaoException(m.toString());
            }
        }
    }
}
