package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import com.fansipan.compass.bean.myapplication.code.com.greendao.AbstractDao;
import com.fansipan.compass.bean.myapplication.code.com.greendao.Property;
import com.fansipan.compass.bean.myapplication.code.com.greendao.database.Database;
import com.fansipan.compass.bean.myapplication.code.com.greendao.database.DatabaseStatement;
import com.fansipan.compass.bean.myapplication.code.com.greendao.internal.DaoConfig;

/* loaded from: classes.dex */
public class GDAOAppPodcastEventDao extends AbstractDao<GDAOAppPodcastEvent, Long> {
    public static final String TABLENAME = "app_podcast_events";

    /* loaded from: classes.dex */
    public static class Properties {
        public static final Property Id = new Property(0, Long.class, "id", true, "ID");
        public static final Property Podcast = new Property(1, Long.class, "podcast", false, "PODCAST");
        public static final Property Episode = new Property(2, Long.class, "episode", false, "EPISODE");
        public static final Property Time_played = new Property(3, Long.class, "time_played", false, "TIME_PLAYED");
        public static final Property Start_date = new Property(4, String.class, "start_date", false, "START_DATE");
        public static final Property Play_date = new Property(5, String.class, "play_date", false, "PLAY_DATE");
        public static final Property Success = new Property(6, Boolean.class, "success", false, "SUCCESS");
    }

    public GDAOAppPodcastEventDao(DaoConfig daoConfig) {
        super(daoConfig);
    }

    public static void createTable(Database database, boolean z) {
        String str;
        if (z) {
            str = "IF NOT EXISTS ";
        } else {
            str = "";
        }
        database.execSQL("CREATE TABLE " + str + "\"app_podcast_events\" (\"ID\" INTEGER PRIMARY KEY AUTOINCREMENT ,\"PODCAST\" INTEGER,\"EPISODE\" INTEGER,\"TIME_PLAYED\" INTEGER,\"START_DATE\" TEXT,\"PLAY_DATE\" TEXT,\"SUCCESS\" INTEGER);");
    }

    public static void dropTable(Database database, boolean z) {
        String str;
        StringBuilder m = new StringBuilder("DROP TABLE ");
        if (z) {
            str = "IF EXISTS ";
        } else {
            str = "";
        }
        m.append(str);
        m.append("\"app_podcast_events\"");
        database.execSQL(m.toString());
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final boolean isEntityUpdateable() {
        return true;
    }

    public GDAOAppPodcastEventDao(DaoConfig daoConfig, DaoSession daoSession) {
        super(daoConfig, daoSession);
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public Long getKey(GDAOAppPodcastEvent gDAOAppPodcastEvent) {
        if (gDAOAppPodcastEvent != null) {
            return gDAOAppPodcastEvent.getId();
        }
        return null;
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public boolean hasKey(GDAOAppPodcastEvent gDAOAppPodcastEvent) {
        return gDAOAppPodcastEvent.getId() != null;
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public Long readKey(Cursor cursor, int i) {
        int i2 = i + 0;
        if (cursor.isNull(i2)) {
            return null;
        }
        return Long.valueOf(cursor.getLong(i2));
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final Long updateKeyAfterInsert(GDAOAppPodcastEvent gDAOAppPodcastEvent, long j) {
        gDAOAppPodcastEvent.setId(Long.valueOf(j));
        return Long.valueOf(j);
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final void bindValues(DatabaseStatement databaseStatement, GDAOAppPodcastEvent gDAOAppPodcastEvent) {
        databaseStatement.clearBindings();
        Long id = gDAOAppPodcastEvent.getId();
        if (id != null) {
            databaseStatement.bindLong(1, id.longValue());
        }
        Long podcast = gDAOAppPodcastEvent.getPodcast();
        if (podcast != null) {
            databaseStatement.bindLong(2, podcast.longValue());
        }
        Long episode = gDAOAppPodcastEvent.getEpisode();
        if (episode != null) {
            databaseStatement.bindLong(3, episode.longValue());
        }
        Long time_played = gDAOAppPodcastEvent.getTime_played();
        if (time_played != null) {
            databaseStatement.bindLong(4, time_played.longValue());
        }
        String start_date = gDAOAppPodcastEvent.getStart_date();
        if (start_date != null) {
            databaseStatement.bindString(5, start_date);
        }
        String play_date = gDAOAppPodcastEvent.getPlay_date();
        if (play_date != null) {
            databaseStatement.bindString(6, play_date);
        }
        Boolean success = gDAOAppPodcastEvent.getSuccess();
        if (success != null) {
            databaseStatement.bindLong(7, success.booleanValue() ? 1L : 0L);
        }
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public GDAOAppPodcastEvent readEntity(Cursor cursor, int i) {
        Boolean valueOf;
        int i2 = i + 0;
        Long valueOf2 = cursor.isNull(i2) ? null : Long.valueOf(cursor.getLong(i2));
        int i3 = i + 1;
        Long valueOf3 = cursor.isNull(i3) ? null : Long.valueOf(cursor.getLong(i3));
        int i4 = i + 2;
        Long valueOf4 = cursor.isNull(i4) ? null : Long.valueOf(cursor.getLong(i4));
        int i5 = i + 3;
        Long valueOf5 = cursor.isNull(i5) ? null : Long.valueOf(cursor.getLong(i5));
        int i6 = i + 4;
        String string = cursor.isNull(i6) ? null : cursor.getString(i6);
        int i7 = i + 5;
        String string2 = cursor.isNull(i7) ? null : cursor.getString(i7);
        int i8 = i + 6;
        if (cursor.isNull(i8)) {
            valueOf = null;
        } else {
            valueOf = Boolean.valueOf(cursor.getShort(i8) != 0);
        }
        return new GDAOAppPodcastEvent(valueOf2, valueOf3, valueOf4, valueOf5, string, string2, valueOf);
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public void readEntity(Cursor cursor, GDAOAppPodcastEvent gDAOAppPodcastEvent, int i) {
        int i2 = i + 0;
        Boolean bool = null;
        gDAOAppPodcastEvent.setId(cursor.isNull(i2) ? null : Long.valueOf(cursor.getLong(i2)));
        int i3 = i + 1;
        gDAOAppPodcastEvent.setPodcast(cursor.isNull(i3) ? null : Long.valueOf(cursor.getLong(i3)));
        int i4 = i + 2;
        gDAOAppPodcastEvent.setEpisode(cursor.isNull(i4) ? null : Long.valueOf(cursor.getLong(i4)));
        int i5 = i + 3;
        gDAOAppPodcastEvent.setTime_played(cursor.isNull(i5) ? null : Long.valueOf(cursor.getLong(i5)));
        int i6 = i + 4;
        gDAOAppPodcastEvent.setStart_date(cursor.isNull(i6) ? null : cursor.getString(i6));
        int i7 = i + 5;
        gDAOAppPodcastEvent.setPlay_date(cursor.isNull(i7) ? null : cursor.getString(i7));
        int i8 = i + 6;
        if (!cursor.isNull(i8)) {
            bool = Boolean.valueOf(cursor.getShort(i8) != 0);
        }
        gDAOAppPodcastEvent.setSuccess(bool);
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final void bindValues(SQLiteStatement sQLiteStatement, GDAOAppPodcastEvent gDAOAppPodcastEvent) {
        sQLiteStatement.clearBindings();
        Long id = gDAOAppPodcastEvent.getId();
        if (id != null) {
            sQLiteStatement.bindLong(1, id.longValue());
        }
        Long podcast = gDAOAppPodcastEvent.getPodcast();
        if (podcast != null) {
            sQLiteStatement.bindLong(2, podcast.longValue());
        }
        Long episode = gDAOAppPodcastEvent.getEpisode();
        if (episode != null) {
            sQLiteStatement.bindLong(3, episode.longValue());
        }
        Long time_played = gDAOAppPodcastEvent.getTime_played();
        if (time_played != null) {
            sQLiteStatement.bindLong(4, time_played.longValue());
        }
        String start_date = gDAOAppPodcastEvent.getStart_date();
        if (start_date != null) {
            sQLiteStatement.bindString(5, start_date);
        }
        String play_date = gDAOAppPodcastEvent.getPlay_date();
        if (play_date != null) {
            sQLiteStatement.bindString(6, play_date);
        }
        Boolean success = gDAOAppPodcastEvent.getSuccess();
        if (success != null) {
            sQLiteStatement.bindLong(7, success.booleanValue() ? 1L : 0L);
        }
    }
}
