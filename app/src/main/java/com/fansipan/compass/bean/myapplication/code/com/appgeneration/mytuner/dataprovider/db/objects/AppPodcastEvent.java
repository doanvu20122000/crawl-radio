package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.objects;

import com.appgeneration.mytuner.dataprovider.helpers.DateTimeHelpers;
import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao.DaoSession;
import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao.GDAOAppPodcastEvent;

import java.util.ArrayList;
import java.util.List;

/* loaded from: classes.dex */
public class AppPodcastEvent {
    private Long mEpisodeId;
    private Long mId;
    private String mPlayDate;
    private Long mPodcastId;
    private String mStartDate;
    private boolean mSuccess;
    private Long mTimePlayed;

    private AppPodcastEvent(GDAOAppPodcastEvent gDAOAppPodcastEvent) {
        if (gDAOAppPodcastEvent != null) {
            this.mId = gDAOAppPodcastEvent.getId();
            this.mPodcastId = gDAOAppPodcastEvent.getPodcast();
            this.mEpisodeId = gDAOAppPodcastEvent.getEpisode();
            this.mTimePlayed = gDAOAppPodcastEvent.getTime_played();
            this.mStartDate = gDAOAppPodcastEvent.getStart_date();
            this.mPlayDate = gDAOAppPodcastEvent.getPlay_date();
            this.mSuccess = gDAOAppPodcastEvent.getSuccess().booleanValue();
        }
    }

    public static void deleteAll(DaoSession daoSession) {
        daoSession.getGDAOAppPodcastEventDao().deleteAll();
    }

    public static List<AppPodcastEvent> getAll(DaoSession daoSession) {
        ArrayList arrayList = new ArrayList();
        List<GDAOAppPodcastEvent> loadAll = daoSession.getGDAOAppPodcastEventDao().loadAll();
        if (loadAll != null) {
            for (GDAOAppPodcastEvent gDAOAppPodcastEvent : loadAll) {
                if (gDAOAppPodcastEvent != null) {
                    arrayList.add(new AppPodcastEvent(gDAOAppPodcastEvent));
                }
            }
        }
        return arrayList;
    }

    public static void reportPodcastError(PodcastEpisode podcastEpisode, DaoSession daoSession) {
        String str;
        String str2;
        if (podcastEpisode.getStartDate() != null) {
            str = DateTimeHelpers.formatDateToServer(podcastEpisode.getStartDate());
        } else {
            str = null;
        }
        if (podcastEpisode.getPlayDate() != null) {
            str2 = DateTimeHelpers.formatDateToServer(podcastEpisode.getPlayDate());
        } else {
            str2 = str;
        }
        if (str != null) {
            GDAOAppPodcastEvent gDAOAppPodcastEvent = new GDAOAppPodcastEvent();
            gDAOAppPodcastEvent.setPodcast(Long.valueOf(podcastEpisode.getPodcastId()));
            gDAOAppPodcastEvent.setEpisode(Long.valueOf(podcastEpisode.getEpisodeId()));
            gDAOAppPodcastEvent.setSuccess(Boolean.FALSE);
            gDAOAppPodcastEvent.setStart_date(str);
            gDAOAppPodcastEvent.setPlay_date(str2);
            daoSession.getGDAOAppPodcastEventDao().insertOrReplace(gDAOAppPodcastEvent);
        }
    }

    public static void reportPodcastSuccess(PodcastEpisode podcastEpisode, DaoSession daoSession, long j) {
        String str;
        String str2 = null;
        if (podcastEpisode.getStartDate() != null) {
            str = DateTimeHelpers.formatDateToServer(podcastEpisode.getStartDate());
        } else {
            str = null;
        }
        if (podcastEpisode.getPlayDate() != null) {
            str2 = DateTimeHelpers.formatDateToServer(podcastEpisode.getPlayDate());
        }
        if (str != null && str2 != null) {
            GDAOAppPodcastEvent gDAOAppPodcastEvent = new GDAOAppPodcastEvent();
            gDAOAppPodcastEvent.setPodcast(Long.valueOf(podcastEpisode.getPodcastId()));
            gDAOAppPodcastEvent.setEpisode(Long.valueOf(podcastEpisode.getEpisodeId()));
            gDAOAppPodcastEvent.setTime_played(Long.valueOf(j / 1000));
            gDAOAppPodcastEvent.setStart_date(str);
            gDAOAppPodcastEvent.setPlay_date(str2);
            gDAOAppPodcastEvent.setSuccess(Boolean.TRUE);
            daoSession.getGDAOAppPodcastEventDao().insertOrReplace(gDAOAppPodcastEvent);
        }
    }

    public Long getEpisodeId() {
        return this.mEpisodeId;
    }

    public Long getId() {
        return this.mId;
    }

    public String getPlayDate() {
        return this.mPlayDate;
    }

    public Long getPodcastId() {
        return this.mPodcastId;
    }

    public String getStartDate() {
        return this.mStartDate;
    }

    public Boolean getSuccess() {
        return Boolean.valueOf(this.mSuccess);
    }

    public Long getTimePlayed() {
        return this.mTimePlayed;
    }
}
