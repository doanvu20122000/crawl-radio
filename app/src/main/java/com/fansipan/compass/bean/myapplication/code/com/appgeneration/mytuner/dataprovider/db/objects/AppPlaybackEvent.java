package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.objects;

import com.appgeneration.mytuner.dataprovider.helpers.DateTimeHelpers;
import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao.DaoSession;
import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao.GDAOAppPlaybackEvent;

import java.util.ArrayList;
import java.util.List;

/* loaded from: classes.dex */
public class AppPlaybackEvent {
    private final GDAOAppPlaybackEvent mDbAppPlaybackEvent;

    private AppPlaybackEvent(GDAOAppPlaybackEvent gDAOAppPlaybackEvent) {
        this.mDbAppPlaybackEvent = gDAOAppPlaybackEvent;
    }

    private static List<AppPlaybackEvent> convertList(List<GDAOAppPlaybackEvent> list) {
        if (list == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (GDAOAppPlaybackEvent gDAOAppPlaybackEvent : list) {
            if (gDAOAppPlaybackEvent != null) {
                arrayList.add(new AppPlaybackEvent(gDAOAppPlaybackEvent));
            }
        }
        return arrayList;
    }

    public static void deleteAll(DaoSession daoSession) {
        daoSession.getGDAOAppPlaybackEventDao().deleteAll();
    }

    public static List<AppPlaybackEvent> getAll(DaoSession daoSession) {
        return convertList(daoSession.getGDAOAppPlaybackEventDao().loadAll());
    }

    public static void reportStreamError(StreamWrapper streamWrapper, DaoSession daoSession, String str, String str2, String str3) {
        long streamId = streamWrapper.getStreamId();
        if (streamId == -1) {
            return;
        }
        Stream stream = Stream.get(daoSession, streamId);
        if (streamWrapper.didFail() && stream != null) {
            streamWrapper.setEndDateToNow();
            String formatDateToServer = DateTimeHelpers.formatDateToServer(streamWrapper.getStartDate());
            String formatDateToServer2 = DateTimeHelpers.formatDateToServer(streamWrapper.getEndDate());
            String formatDateToServer3 = DateTimeHelpers.formatDateToServer(streamWrapper.getPlayDate());
            GDAOAppPlaybackEvent gDAOAppPlaybackEvent = new GDAOAppPlaybackEvent();
            gDAOAppPlaybackEvent.setError_code("");
            gDAOAppPlaybackEvent.setError_domain(str);
            gDAOAppPlaybackEvent.setError_description(str2);
            gDAOAppPlaybackEvent.setStream(Long.valueOf(streamId));
            gDAOAppPlaybackEvent.setSuccess(Boolean.FALSE);
            gDAOAppPlaybackEvent.setRadio(Long.valueOf(stream.getRadio()));
            gDAOAppPlaybackEvent.setStart_date(formatDateToServer);
            gDAOAppPlaybackEvent.setEnd_date(formatDateToServer2);
            if (!formatDateToServer3.equals("")) {
                formatDateToServer = formatDateToServer3;
            }
            gDAOAppPlaybackEvent.setPlay_date(formatDateToServer);
            gDAOAppPlaybackEvent.setMetadata(Boolean.valueOf(streamWrapper.hasMetadata()));
            gDAOAppPlaybackEvent.setSource(str3);
            daoSession.getGDAOAppPlaybackEventDao().insertOrReplace(gDAOAppPlaybackEvent);
        }
    }

    public static void reportStreamSuccess(StreamWrapper streamWrapper, DaoSession daoSession, String str) {
        Stream stream;
        long streamId = streamWrapper.getStreamId();
        if (streamId == -1 || (stream = Stream.get(daoSession, streamId)) == null) {
            return;
        }
        streamWrapper.setEndDateToNow();
        String formatDateToServer = DateTimeHelpers.formatDateToServer(streamWrapper.getStartDate());
        String formatDateToServer2 = DateTimeHelpers.formatDateToServer(streamWrapper.getEndDate());
        String formatDateToServer3 = DateTimeHelpers.formatDateToServer(streamWrapper.getPlayDate());
        GDAOAppPlaybackEvent gDAOAppPlaybackEvent = new GDAOAppPlaybackEvent();
        gDAOAppPlaybackEvent.setError_code("");
        gDAOAppPlaybackEvent.setError_domain("");
        gDAOAppPlaybackEvent.setError_description("");
        gDAOAppPlaybackEvent.setStream(Long.valueOf(streamId));
        gDAOAppPlaybackEvent.setSuccess(Boolean.TRUE);
        gDAOAppPlaybackEvent.setRadio(Long.valueOf(stream.getRadio()));
        gDAOAppPlaybackEvent.setStart_date(formatDateToServer);
        gDAOAppPlaybackEvent.setEnd_date(formatDateToServer2);
        if (!formatDateToServer3.equals("")) {
            formatDateToServer = formatDateToServer3;
        }
        gDAOAppPlaybackEvent.setPlay_date(formatDateToServer);
        gDAOAppPlaybackEvent.setMetadata(Boolean.valueOf(streamWrapper.hasMetadata()));
        gDAOAppPlaybackEvent.setSource(str);
        daoSession.getGDAOAppPlaybackEventDao().insertOrReplace(gDAOAppPlaybackEvent);
    }

    public String getEndDate() {
        return this.mDbAppPlaybackEvent.getEnd_date();
    }

    public String getErrorDescription() {
        return this.mDbAppPlaybackEvent.getError_description();
    }

    public boolean getMetadata() {
        return this.mDbAppPlaybackEvent.getMetadata().booleanValue();
    }

    public String getPlayDate() {
        return this.mDbAppPlaybackEvent.getPlay_date();
    }

    public long getRadio() {
        return this.mDbAppPlaybackEvent.getRadio().longValue();
    }

    public String getSource() {
        return this.mDbAppPlaybackEvent.getSource();
    }

    public String getStartDate() {
        return this.mDbAppPlaybackEvent.getStart_date();
    }

    public long getStream() {
        return this.mDbAppPlaybackEvent.getStream().longValue();
    }

    public boolean getSuccess() {
        return this.mDbAppPlaybackEvent.getSuccess().booleanValue();
    }
}
