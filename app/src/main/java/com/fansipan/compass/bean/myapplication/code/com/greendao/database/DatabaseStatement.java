package com.fansipan.compass.bean.myapplication.code.com.greendao.database;

/* loaded from: classes4.dex */
public interface DatabaseStatement {
    void bindDouble(int i, double d2);

    void bindLong(int i, long j);

    void bindString(int i, String str);

    void clearBindings();

    void close();

    void execute();

    long executeInsert();

    Object getRawStatement();

    long simpleQueryForLong();
}
