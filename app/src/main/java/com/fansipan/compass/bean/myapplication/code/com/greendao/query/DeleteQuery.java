package com.fansipan.compass.bean.myapplication.code.com.greendao.query;

import com.fansipan.compass.bean.myapplication.code.com.greendao.AbstractDao;
import com.fansipan.compass.bean.myapplication.code.com.greendao.database.Database;


/* loaded from: classes4.dex */
public final class DeleteQuery<T> extends AbstractQuery<T> {

    /* loaded from: classes4.dex */
    public static final class QueryData<T2> extends AbstractQueryData<T2, DeleteQuery<T2>> {
        public QueryData(AbstractDao abstractDao, String str, String[] strArr) {
            super(abstractDao, str, strArr);
        }

        @Override // org.greenrobot.greendao.query.AbstractQueryData
        public final DeleteQuery<T2> createQuery() {
            return new DeleteQuery(this.dao, this.sql, (String[]) this.initialValues.clone());
        }
    }

    public DeleteQuery(AbstractDao abstractDao, String str, String[] strArr) {
        super(abstractDao, str, strArr);
    }

    public final void executeDeleteWithoutDetachingEntities() {
        checkThread();
        Database database = this.dao.getDatabase();
        if (database.isDbLockedByCurrentThread()) {
            this.dao.getDatabase().execSQL(this.sql, this.parameters);
            return;
        }
        database.beginTransaction();
        try {
            this.dao.getDatabase().execSQL(this.sql, this.parameters);
            database.setTransactionSuccessful();
        } finally {
            database.endTransaction();
        }
    }
}
