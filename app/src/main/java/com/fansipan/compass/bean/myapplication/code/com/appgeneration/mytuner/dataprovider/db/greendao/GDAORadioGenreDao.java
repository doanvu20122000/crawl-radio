package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import com.fansipan.compass.bean.myapplication.code.com.POBRequest$AdPosition$EnumUnboxingLocalUtility;
import com.fansipan.compass.bean.myapplication.code.com.greendao.AbstractDao;
import com.fansipan.compass.bean.myapplication.code.com.greendao.Property;
import com.fansipan.compass.bean.myapplication.code.com.greendao.database.Database;
import com.fansipan.compass.bean.myapplication.code.com.greendao.database.DatabaseStatement;
import com.fansipan.compass.bean.myapplication.code.com.greendao.internal.DaoConfig;

/* loaded from: classes.dex */
public class GDAORadioGenreDao extends AbstractDao<GDAORadioGenre, Void> {
    public static final String TABLENAME = "radios_genres";

    /* loaded from: classes.dex */
    public static class Properties {
        public static final Property Radio = new Property(0, Long.class, "radio", false, "RADIO");
        public static final Property Genre = new Property(1, Long.class, GDAOGenreDao.TABLENAME, false, "GENRE");
    }

    public GDAORadioGenreDao(DaoConfig daoConfig) {
        super(daoConfig);
    }

    public static void createTable(Database database, boolean z) {
        String str;
        if (z) {
            str = "IF NOT EXISTS ";
        } else {
            str = "";
        }
        database.execSQL("CREATE TABLE " + str + "\"radios_genres\" (\"RADIO\" INTEGER,\"GENRE\" INTEGER);");
    }

    public static void dropTable(Database database, boolean z) {
        String str;
        StringBuilder m = new StringBuilder("DROP TABLE ");
        if (z) {
            str = "IF EXISTS ";
        } else {
            str = "";
        }
        POBRequest$AdPosition$EnumUnboxingLocalUtility.m(m, str, "\"radios_genres\"", database);
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public Void getKey(GDAORadioGenre gDAORadioGenre) {
        return null;
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public boolean hasKey(GDAORadioGenre gDAORadioGenre) {
        return false;
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final boolean isEntityUpdateable() {
        return true;
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public Void readKey(Cursor cursor, int i) {
        return null;
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final Void updateKeyAfterInsert(GDAORadioGenre gDAORadioGenre, long j) {
        return null;
    }

    public GDAORadioGenreDao(DaoConfig daoConfig, DaoSession daoSession) {
        super(daoConfig, daoSession);
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final void bindValues(DatabaseStatement databaseStatement, GDAORadioGenre gDAORadioGenre) {
        databaseStatement.clearBindings();
        Long radio2 = gDAORadioGenre.getRadio();
        if (radio2 != null) {
            databaseStatement.bindLong(1, radio2.longValue());
        }
        Long genre = gDAORadioGenre.getGenre();
        if (genre != null) {
            databaseStatement.bindLong(2, genre.longValue());
        }
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public GDAORadioGenre readEntity(Cursor cursor, int i) {
        int i2 = i + 0;
        int i3 = i + 1;
        return new GDAORadioGenre(cursor.isNull(i2) ? null : Long.valueOf(cursor.getLong(i2)), cursor.isNull(i3) ? null : Long.valueOf(cursor.getLong(i3)));
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public void readEntity(Cursor cursor, GDAORadioGenre gDAORadioGenre, int i) {
        int i2 = i + 0;
        gDAORadioGenre.setRadio(cursor.isNull(i2) ? null : Long.valueOf(cursor.getLong(i2)));
        int i3 = i + 1;
        gDAORadioGenre.setGenre(cursor.isNull(i3) ? null : Long.valueOf(cursor.getLong(i3)));
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final void bindValues(SQLiteStatement sQLiteStatement, GDAORadioGenre gDAORadioGenre) {
        sQLiteStatement.clearBindings();
        Long radio2 = gDAORadioGenre.getRadio();
        if (radio2 != null) {
            sQLiteStatement.bindLong(1, radio2.longValue());
        }
        Long genre = gDAORadioGenre.getGenre();
        if (genre != null) {
            sQLiteStatement.bindLong(2, genre.longValue());
        }
    }
}
