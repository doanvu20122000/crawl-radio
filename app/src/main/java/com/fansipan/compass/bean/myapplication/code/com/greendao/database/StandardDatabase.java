package com.fansipan.compass.bean.myapplication.code.com.greendao.database;

import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

/* loaded from: classes4.dex */
public final class StandardDatabase implements Database {
    public final SQLiteDatabase delegate;

    public StandardDatabase(SQLiteDatabase sQLiteDatabase) {
        this.delegate = sQLiteDatabase;
    }

    @Override // org.greenrobot.greendao.database.Database
    public final void beginTransaction() {
        this.delegate.beginTransaction();
    }

    @Override // org.greenrobot.greendao.database.Database
    public final DatabaseStatement compileStatement(String str) {
        return null;
    }

    @Override // org.greenrobot.greendao.database.Database
    public final void endTransaction() {
        this.delegate.endTransaction();
    }

    @Override // org.greenrobot.greendao.database.Database
    public final void execSQL(String str) throws SQLException {
        this.delegate.execSQL(str);
    }

    @Override // org.greenrobot.greendao.database.Database
    public final Object getRawDatabase() {
        return this.delegate;
    }

    @Override // org.greenrobot.greendao.database.Database
    public final boolean isDbLockedByCurrentThread() {
        return this.delegate.isDbLockedByCurrentThread();
    }

    @Override // org.greenrobot.greendao.database.Database
    public final boolean isOpen() {
        return this.delegate.isOpen();
    }

    @Override // org.greenrobot.greendao.database.Database
    public final Cursor rawQuery(String str, String[] strArr) {
        return this.delegate.rawQuery(str, strArr);
    }

    @Override // org.greenrobot.greendao.database.Database
    public final void setTransactionSuccessful() {
        this.delegate.setTransactionSuccessful();
    }

    @Override // org.greenrobot.greendao.database.Database
    public final void execSQL(String str, Object[] objArr) throws SQLException {
        this.delegate.execSQL(str, objArr);
    }
}
