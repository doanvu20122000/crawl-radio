package com.fansipan.compass.bean.myapplication.code.com.greendao.identityscope;

import java.util.ArrayList;

/* loaded from: classes4.dex */
public interface IdentityScope<K, T> {
    void clear();

    boolean detach(K k, T t);

    T get(K k);

    T getNoLock(K k);

    void lock();

    void put(K k, T t);

    void putNoLock(K k, T t);

    void remove(K k);

    void remove(ArrayList arrayList);

    void reserveRoom(int i);

    void unlock();
}
