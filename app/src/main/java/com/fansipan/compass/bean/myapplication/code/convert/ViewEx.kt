package com.fansipan.compass.bean.myapplication.code.convert

import android.view.View
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*

fun View.hide() {
    this.visibility = View.INVISIBLE
}

fun View.show() {
    this.visibility = View.VISIBLE
}

fun View.gone() {
    this.visibility = View.GONE
}

fun Long.formatNumber(): String {
    val decimalFormat = DecimalFormat("#,###")
    return decimalFormat.format(this)
}

fun Int.formatNumber() = this.toLong().formatNumber()