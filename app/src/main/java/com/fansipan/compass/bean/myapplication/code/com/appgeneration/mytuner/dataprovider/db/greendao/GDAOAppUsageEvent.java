package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao;

import java.io.Serializable;

/* loaded from: classes.dex */
public class GDAOAppUsageEvent implements Serializable {
    private String date;
    private Long id;
    private String session_id;
    private Boolean startup;

    public GDAOAppUsageEvent() {
    }

    public String getDate() {
        return this.date;
    }

    public Long getId() {
        return this.id;
    }

    public String getSession_id() {
        return this.session_id;
    }

    public Boolean getStartup() {
        return this.startup;
    }

    public void setDate(String str) {
        this.date = str;
    }

    public void setId(Long l) {
        this.id = l;
    }

    public void setSession_id(String str) {
        this.session_id = str;
    }

    public void setStartup(Boolean bool) {
        this.startup = bool;
    }

    public GDAOAppUsageEvent(Long l) {
        this.id = l;
    }

    public GDAOAppUsageEvent(Long l, String str, String str2, Boolean bool) {
        this.id = l;
        this.session_id = str;
        this.date = str2;
        this.startup = bool;
    }
}
