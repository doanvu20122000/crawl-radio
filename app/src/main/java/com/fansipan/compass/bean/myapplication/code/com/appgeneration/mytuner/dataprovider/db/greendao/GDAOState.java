package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao;

import com.fansipan.compass.bean.myapplication.code.com.greendao.DaoException;

import java.io.Serializable;

/* loaded from: classes.dex */
public class GDAOState implements Serializable {
    private Long country;
    private GDAOCountry countryObject;
    private transient Long countryObject__resolvedKey;
    private transient DaoSession daoSession;
    private Long id;
    private transient GDAOStateDao myDao;
    private String name;

    public GDAOState() {
    }

    private void __throwIfDetached() {
        if (this.myDao != null) {
            return;
        }
        throw new DaoException("Entity is detached from DAO context");
    }

    public void __setDaoSession(DaoSession daoSession) {
        GDAOStateDao gDAOStateDao;
        this.daoSession = daoSession;
        if (daoSession != null) {
            gDAOStateDao = daoSession.getGDAOStateDao();
        } else {
            gDAOStateDao = null;
        }
        this.myDao = gDAOStateDao;
    }

    public void delete() {
//        __throwIfDetached();
//        this.myDao.delete(this);
    }

    public Long getCountry() {
        return this.country;
    }

    public GDAOCountry getCountryObject() {
        Long l = this.country;
        Long l2 = this.countryObject__resolvedKey;
        if (l2 == null || !l2.equals(l)) {
            __throwIfDetached();
            GDAOCountry load = this.daoSession.getGDAOCountryDao().load(l);
            synchronized (this) {
                this.countryObject = load;
                this.countryObject__resolvedKey = l;
            }
        }
        return this.countryObject;
    }

    public Long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public void refresh() {
//        __throwIfDetached();
//        this.myDao.refresh(this);
    }

    public void setCountry(Long l) {
        this.country = l;
    }

    public void setCountryObject(GDAOCountry gDAOCountry) {
        Long id;
        synchronized (this) {
            this.countryObject = gDAOCountry;
            if (gDAOCountry == null) {
                id = null;
            } else {
                id = gDAOCountry.getId();
            }
            this.country = id;
            this.countryObject__resolvedKey = id;
        }
    }

    public void setId(Long l) {
        this.id = l;
    }

    public void setName(String str) {
        this.name = str;
    }

    public void update() {
//        __throwIfDetached();
//        this.myDao.update(this);
    }

    public GDAOState(Long l) {
        this.id = l;
    }

    public GDAOState(Long l, String str, Long l2) {
        this.id = l;
        this.name = str;
        this.country = l2;
    }
}
