package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.fansipan.compass.bean.myapplication.code.com.greendao.AbstractDaoMaster;
import com.fansipan.compass.bean.myapplication.code.com.greendao.database.Database;
import com.fansipan.compass.bean.myapplication.code.com.greendao.database.DatabaseOpenHelper;
import com.fansipan.compass.bean.myapplication.code.com.greendao.database.StandardDatabase;
import com.fansipan.compass.bean.myapplication.code.com.greendao.identityscope.IdentityScopeType;

/* loaded from: classes.dex */
public class DaoMaster extends AbstractDaoMaster {
    public static final int SCHEMA_VERSION = 1046000;

    /* loaded from: classes.dex */
    public static class DevOpenHelper extends OpenHelper {
        public DevOpenHelper(Context context, String str) {
            super(context, str);
        }

        @Override // org.greenrobot.greendao.database.DatabaseOpenHelper
        public void onUpgrade(Database database, int i, int i2) {
            DaoMaster.dropAllTables(database, true);
            onCreate(database);
        }

        public DevOpenHelper(Context context, String str, SQLiteDatabase.CursorFactory cursorFactory) {
            super(context, str, cursorFactory);
        }
    }

    /* loaded from: classes.dex */
    public static abstract class OpenHelper extends DatabaseOpenHelper {
        public OpenHelper(Context context, String str) {
            super(context, str, DaoMaster.SCHEMA_VERSION);
        }

        @Override // org.greenrobot.greendao.database.DatabaseOpenHelper
        public void onCreate(Database database) {
            Log.i("greenDAO", "Creating tables for schema version 1046000");
            DaoMaster.createAllTables(database, false);
        }

        public OpenHelper(Context context, String str, SQLiteDatabase.CursorFactory cursorFactory) {
            super(context, str, cursorFactory, DaoMaster.SCHEMA_VERSION);
        }
    }

    public DaoMaster(SQLiteDatabase sQLiteDatabase) {
        this(new StandardDatabase(sQLiteDatabase));
    }

    public static void createAllTables(Database database, boolean z) {
        GDAOCountryDao.createTable(database, z);
        GDAOStateDao.createTable(database, z);
        GDAOCityDao.createTable(database, z);
        GDAOGenreDao.createTable(database, z);
        GDAOStreamDao.createTable(database, z);
        GDAORadioDao.createTable(database, z);
        GDAORadioCityDao.createTable(database, z);
        GDAORadioGenreDao.createTable(database, z);
        GDAORadioListDao.createTable(database, z);
        GDAORadioListDetailDao.createTable(database, z);
        GDAOPodcastDao.createTable(database, z);
        GDAOUserSelectedEntityDao.createTable(database, z);
        GDAOSettingsDao.createTable(database, z);
        GDAOOperationDao.createTable(database, z);
        GDAOAppUsageEventDao.createTable(database, z);
        GDAOAppPlaybackEventDao.createTable(database, z);
        GDAOAppSongEventDao.createTable(database, z);
        GDAOAppPodcastEventDao.createTable(database, z);
        GDAOAppVolumeChangeEventDao.createTable(database, z);
    }

    public static void dropAllTables(Database database, boolean z) {
        GDAOCountryDao.dropTable(database, z);
        GDAOStateDao.dropTable(database, z);
        GDAOCityDao.dropTable(database, z);
        GDAOGenreDao.dropTable(database, z);
        GDAOStreamDao.dropTable(database, z);
        GDAORadioDao.dropTable(database, z);
        GDAORadioCityDao.dropTable(database, z);
        GDAORadioGenreDao.dropTable(database, z);
        GDAORadioListDao.dropTable(database, z);
        GDAORadioListDetailDao.dropTable(database, z);
        GDAOPodcastDao.dropTable(database, z);
        GDAOUserSelectedEntityDao.dropTable(database, z);
        GDAOSettingsDao.dropTable(database, z);
        GDAOOperationDao.dropTable(database, z);
        GDAOAppUsageEventDao.dropTable(database, z);
        GDAOAppPlaybackEventDao.dropTable(database, z);
        GDAOAppSongEventDao.dropTable(database, z);
        GDAOAppPodcastEventDao.dropTable(database, z);
        GDAOAppVolumeChangeEventDao.dropTable(database, z);
    }

    public static DaoSession newDevSession(Context context, String str) {
        return new DaoMaster(new DevOpenHelper(context, str).getWritableDb()).newSession();
    }

    public DaoMaster(Database database) {
        super(database, SCHEMA_VERSION);
        registerDaoClass(GDAOCountryDao.class);
        registerDaoClass(GDAOStateDao.class);
        registerDaoClass(GDAOCityDao.class);
        registerDaoClass(GDAOGenreDao.class);
        registerDaoClass(GDAOStreamDao.class);
        registerDaoClass(GDAORadioDao.class);
        registerDaoClass(GDAORadioCityDao.class);
        registerDaoClass(GDAORadioGenreDao.class);
        registerDaoClass(GDAORadioListDao.class);
        registerDaoClass(GDAORadioListDetailDao.class);
        registerDaoClass(GDAOPodcastDao.class);
        registerDaoClass(GDAOUserSelectedEntityDao.class);
        registerDaoClass(GDAOSettingsDao.class);
        registerDaoClass(GDAOOperationDao.class);
        registerDaoClass(GDAOAppUsageEventDao.class);
        registerDaoClass(GDAOAppPlaybackEventDao.class);
        registerDaoClass(GDAOAppSongEventDao.class);
        registerDaoClass(GDAOAppPodcastEventDao.class);
        registerDaoClass(GDAOAppVolumeChangeEventDao.class);
    }

    @Override // org.greenrobot.greendao.AbstractDaoMaster
    public DaoSession newSession() {
        return new DaoSession(this.db, IdentityScopeType.Session, this.daoConfigMap);
    }

    @Override // org.greenrobot.greendao.AbstractDaoMaster
    public DaoSession newSession(IdentityScopeType identityScopeType) {
        return new DaoSession(this.db, identityScopeType, this.daoConfigMap);
    }
}
