package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.objects;

import com.appgeneration.mytuner.dataprovider.helpers.DateTimeHelpers;
import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao.DaoSession;
import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao.GDAOAppVolumeChangeEvent;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import kotlin.jvm.internal.Intrinsics;

/* compiled from: AppVolumeChangeEvents.kt */
/* loaded from: classes.dex */
public final class AppVolumeChange {
    public static final AppVolumeChange INSTANCE = new AppVolumeChange();

    /* compiled from: AppVolumeChangeEvents.kt */
    /* loaded from: classes.dex */
    public static final class Event {
        private final float endVolume;
        private final long id;
        private final String startDate;
        private final float startVolume;

        public Event(long j, String str, float f2, float f3) {
            this.id = j;
            this.startDate = str;
            this.startVolume = f2;
            this.endVolume = f3;
        }

        public static /* synthetic */ Event copy$default(Event event, long j, String str, float f2, float f3, int i, Object obj) {
            if ((i & 1) != 0) {
                j = event.id;
            }
            long j2 = j;
            if ((i & 2) != 0) {
                str = event.startDate;
            }
            String str2 = str;
            if ((i & 4) != 0) {
                f2 = event.startVolume;
            }
            float f4 = f2;
            if ((i & 8) != 0) {
                f3 = event.endVolume;
            }
            return event.copy(j2, str2, f4, f3);
        }

        public final long component1() {
            return this.id;
        }

        public final String component2() {
            return this.startDate;
        }

        public final float component3() {
            return this.startVolume;
        }

        public final float component4() {
            return this.endVolume;
        }

        public final Event copy(long j, String str, float f2, float f3) {
            return new Event(j, str, f2, f3);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof Event) {
                Event event = (Event) obj;
                return this.id == event.id && Intrinsics.areEqual(this.startDate, event.startDate) && Float.compare(this.startVolume, event.startVolume) == 0 && Float.compare(this.endVolume, event.endVolume) == 0;
            }
            return false;
        }

        public final float getEndVolume() {
            return this.endVolume;
        }

        public final long getId() {
            return this.id;
        }

        public final String getStartDate() {
            return this.startDate;
        }

        public final float getStartVolume() {
            return this.startVolume;
        }

        public int hashCode() {
            long j = this.id;
            int m = (this.startDate.hashCode() + ((int) (j ^ (j >>> 32))) * 31) * 31;
            return Float.floatToIntBits(this.endVolume) + ((Float.floatToIntBits(this.startVolume) + m) * 31);
        }

        public String toString() {
            StringBuilder m = new StringBuilder("Event(id=");
            m.append(this.id);
            m.append(", startDate=");
            m.append(this.startDate);
            m.append(", startVolume=");
            m.append(this.startVolume);
            m.append(", endVolume=");
            m.append(this.endVolume);
            m.append(')');
            return m.toString();
        }
    }

    private AppVolumeChange() {
    }

    public static final void deleteAll(DaoSession daoSession) {
        daoSession.getGDAOAppVolumeChangeEventDao().deleteAll();
    }

    public static final void reportEvent(DaoSession daoSession, Date date, float f2, float f3) {
        GDAOAppVolumeChangeEvent gDAOAppVolumeChangeEvent = new GDAOAppVolumeChangeEvent();
        gDAOAppVolumeChangeEvent.setStart_date(DateTimeHelpers.formatDateToServer(date));
        gDAOAppVolumeChangeEvent.setStart_volume(Float.valueOf(f2));
        gDAOAppVolumeChangeEvent.setEnd_volume(Float.valueOf(f3));
        daoSession.getGDAOAppVolumeChangeEventDao().insert(gDAOAppVolumeChangeEvent);
    }

    private final Event toDomain(GDAOAppVolumeChangeEvent gDAOAppVolumeChangeEvent) {
        return new Event(gDAOAppVolumeChangeEvent.getId().longValue(), gDAOAppVolumeChangeEvent.getStart_date(), gDAOAppVolumeChangeEvent.getStart_volume().floatValue(), gDAOAppVolumeChangeEvent.getEnd_volume().floatValue());
    }
}
