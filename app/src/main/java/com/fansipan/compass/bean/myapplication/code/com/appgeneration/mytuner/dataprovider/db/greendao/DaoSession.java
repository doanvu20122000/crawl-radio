package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao;

import com.fansipan.compass.bean.myapplication.code.com.greendao.AbstractDao;
import com.fansipan.compass.bean.myapplication.code.com.greendao.AbstractDaoSession;
import com.fansipan.compass.bean.myapplication.code.com.greendao.database.Database;
import com.fansipan.compass.bean.myapplication.code.com.greendao.identityscope.IdentityScopeType;
import com.fansipan.compass.bean.myapplication.code.com.greendao.internal.DaoConfig;

import java.util.Map;

/* loaded from: classes.dex */
public class DaoSession extends AbstractDaoSession {
    private final GDAOAppPlaybackEventDao gDAOAppPlaybackEventDao;
    private final DaoConfig gDAOAppPlaybackEventDaoConfig;
    private final GDAOAppPodcastEventDao gDAOAppPodcastEventDao;
    private final DaoConfig gDAOAppPodcastEventDaoConfig;
    private final GDAOAppSongEventDao gDAOAppSongEventDao;
    private final DaoConfig gDAOAppSongEventDaoConfig;
    private final GDAOAppUsageEventDao gDAOAppUsageEventDao;
    private final DaoConfig gDAOAppUsageEventDaoConfig;
    private final GDAOAppVolumeChangeEventDao gDAOAppVolumeChangeEventDao;
    private final DaoConfig gDAOAppVolumeChangeEventDaoConfig;
    private final GDAOCityDao gDAOCityDao;
    private final DaoConfig gDAOCityDaoConfig;
    private final GDAOCountryDao gDAOCountryDao;
    private final DaoConfig gDAOCountryDaoConfig;
    private final GDAOGenreDao gDAOGenreDao;
    private final DaoConfig gDAOGenreDaoConfig;
    private final GDAOOperationDao gDAOOperationDao;
    private final DaoConfig gDAOOperationDaoConfig;
    private final GDAOPodcastDao gDAOPodcastDao;
    private final DaoConfig gDAOPodcastDaoConfig;
    private final GDAORadioCityDao gDAORadioCityDao;
    private final DaoConfig gDAORadioCityDaoConfig;
    private final GDAORadioDao gDAORadioDao;
    private final DaoConfig gDAORadioDaoConfig;
    private final GDAORadioGenreDao gDAORadioGenreDao;
    private final DaoConfig gDAORadioGenreDaoConfig;
    private final GDAORadioListDao gDAORadioListDao;
    private final DaoConfig gDAORadioListDaoConfig;
    private final GDAORadioListDetailDao gDAORadioListDetailDao;
    private final DaoConfig gDAORadioListDetailDaoConfig;
    private final GDAOSettingsDao gDAOSettingsDao;
    private final DaoConfig gDAOSettingsDaoConfig;
    private final GDAOStateDao gDAOStateDao;
    private final DaoConfig gDAOStateDaoConfig;
    private final GDAOStreamDao gDAOStreamDao;
    private final DaoConfig gDAOStreamDaoConfig;
    private final GDAOUserSelectedEntityDao gDAOUserSelectedEntityDao;
    private final DaoConfig gDAOUserSelectedEntityDaoConfig;

    public DaoSession(Database database, IdentityScopeType identityScopeType, Map<Class<? extends AbstractDao<?, ?>>, DaoConfig> map) {
        super(database);
        DaoConfig daoConfig = map.get(GDAOCountryDao.class);
        daoConfig.getClass();
        DaoConfig daoConfig2 = new DaoConfig(daoConfig);
        this.gDAOCountryDaoConfig = daoConfig2;
        daoConfig2.initIdentityScope(identityScopeType);
        DaoConfig daoConfig3 = map.get(GDAOStateDao.class);
        daoConfig3.getClass();
        DaoConfig daoConfig4 = new DaoConfig(daoConfig3);
        this.gDAOStateDaoConfig = daoConfig4;
        daoConfig4.initIdentityScope(identityScopeType);
        DaoConfig daoConfig5 = map.get(GDAOCityDao.class);
        daoConfig5.getClass();
        DaoConfig daoConfig6 = new DaoConfig(daoConfig5);
        this.gDAOCityDaoConfig = daoConfig6;
        daoConfig6.initIdentityScope(identityScopeType);
        DaoConfig daoConfig7 = map.get(GDAOGenreDao.class);
        daoConfig7.getClass();
        DaoConfig daoConfig8 = new DaoConfig(daoConfig7);
        this.gDAOGenreDaoConfig = daoConfig8;
        daoConfig8.initIdentityScope(identityScopeType);
        DaoConfig daoConfig9 = map.get(GDAOStreamDao.class);
        daoConfig9.getClass();
        DaoConfig daoConfig10 = new DaoConfig(daoConfig9);
        this.gDAOStreamDaoConfig = daoConfig10;
        daoConfig10.initIdentityScope(identityScopeType);
        DaoConfig daoConfig11 = map.get(GDAORadioDao.class);
        daoConfig11.getClass();
        DaoConfig daoConfig12 = new DaoConfig(daoConfig11);
        this.gDAORadioDaoConfig = daoConfig12;
        daoConfig12.initIdentityScope(identityScopeType);
        DaoConfig daoConfig13 = map.get(GDAORadioCityDao.class);
        daoConfig13.getClass();
        DaoConfig daoConfig14 = new DaoConfig(daoConfig13);
        this.gDAORadioCityDaoConfig = daoConfig14;
        daoConfig14.initIdentityScope(identityScopeType);
        DaoConfig daoConfig15 = map.get(GDAORadioGenreDao.class);
        daoConfig15.getClass();
        DaoConfig daoConfig16 = new DaoConfig(daoConfig15);
        this.gDAORadioGenreDaoConfig = daoConfig16;
        daoConfig16.initIdentityScope(identityScopeType);
        DaoConfig daoConfig17 = map.get(GDAORadioListDao.class);
        daoConfig17.getClass();
        DaoConfig daoConfig18 = new DaoConfig(daoConfig17);
        this.gDAORadioListDaoConfig = daoConfig18;
        daoConfig18.initIdentityScope(identityScopeType);
        DaoConfig daoConfig19 = map.get(GDAORadioListDetailDao.class);
        daoConfig19.getClass();
        DaoConfig daoConfig20 = new DaoConfig(daoConfig19);
        this.gDAORadioListDetailDaoConfig = daoConfig20;
        daoConfig20.initIdentityScope(identityScopeType);
        DaoConfig daoConfig21 = map.get(GDAOPodcastDao.class);
        daoConfig21.getClass();
        DaoConfig daoConfig22 = new DaoConfig(daoConfig21);
        this.gDAOPodcastDaoConfig = daoConfig22;
        daoConfig22.initIdentityScope(identityScopeType);
        DaoConfig daoConfig23 = map.get(GDAOUserSelectedEntityDao.class);
        daoConfig23.getClass();
        DaoConfig daoConfig24 = new DaoConfig(daoConfig23);
        this.gDAOUserSelectedEntityDaoConfig = daoConfig24;
        daoConfig24.initIdentityScope(identityScopeType);
        DaoConfig daoConfig25 = map.get(GDAOSettingsDao.class);
        daoConfig25.getClass();
        DaoConfig daoConfig26 = new DaoConfig(daoConfig25);
        this.gDAOSettingsDaoConfig = daoConfig26;
        daoConfig26.initIdentityScope(identityScopeType);
        DaoConfig daoConfig27 = map.get(GDAOOperationDao.class);
        daoConfig27.getClass();
        DaoConfig daoConfig28 = new DaoConfig(daoConfig27);
        this.gDAOOperationDaoConfig = daoConfig28;
        daoConfig28.initIdentityScope(identityScopeType);
        DaoConfig daoConfig29 = map.get(GDAOAppUsageEventDao.class);
        daoConfig29.getClass();
        DaoConfig daoConfig30 = new DaoConfig(daoConfig29);
        this.gDAOAppUsageEventDaoConfig = daoConfig30;
        daoConfig30.initIdentityScope(identityScopeType);
        DaoConfig daoConfig31 = map.get(GDAOAppPlaybackEventDao.class);
        daoConfig31.getClass();
        DaoConfig daoConfig32 = new DaoConfig(daoConfig31);
        this.gDAOAppPlaybackEventDaoConfig = daoConfig32;
        daoConfig32.initIdentityScope(identityScopeType);
        DaoConfig daoConfig33 = map.get(GDAOAppSongEventDao.class);
        daoConfig33.getClass();
        DaoConfig daoConfig34 = new DaoConfig(daoConfig33);
        this.gDAOAppSongEventDaoConfig = daoConfig34;
        daoConfig34.initIdentityScope(identityScopeType);
        DaoConfig daoConfig35 = map.get(GDAOAppPodcastEventDao.class);
        daoConfig35.getClass();
        DaoConfig daoConfig36 = new DaoConfig(daoConfig35);
        this.gDAOAppPodcastEventDaoConfig = daoConfig36;
        daoConfig36.initIdentityScope(identityScopeType);
        DaoConfig daoConfig37 = map.get(GDAOAppVolumeChangeEventDao.class);
        daoConfig37.getClass();
        DaoConfig daoConfig38 = new DaoConfig(daoConfig37);
        this.gDAOAppVolumeChangeEventDaoConfig = daoConfig38;
        daoConfig38.initIdentityScope(identityScopeType);
        GDAOCountryDao gDAOCountryDao = new GDAOCountryDao(daoConfig2, this);
        this.gDAOCountryDao = gDAOCountryDao;
        GDAOStateDao gDAOStateDao = new GDAOStateDao(daoConfig4, this);
        this.gDAOStateDao = gDAOStateDao;
        GDAOCityDao gDAOCityDao = new GDAOCityDao(daoConfig6, this);
        this.gDAOCityDao = gDAOCityDao;
        GDAOGenreDao gDAOGenreDao = new GDAOGenreDao(daoConfig8, this);
        this.gDAOGenreDao = gDAOGenreDao;
        GDAOStreamDao gDAOStreamDao = new GDAOStreamDao(daoConfig10, this);
        this.gDAOStreamDao = gDAOStreamDao;
        GDAORadioDao gDAORadioDao = new GDAORadioDao(daoConfig12, this);
        this.gDAORadioDao = gDAORadioDao;
        GDAORadioCityDao gDAORadioCityDao = new GDAORadioCityDao(daoConfig14, this);
        this.gDAORadioCityDao = gDAORadioCityDao;
        GDAORadioGenreDao gDAORadioGenreDao = new GDAORadioGenreDao(daoConfig16, this);
        this.gDAORadioGenreDao = gDAORadioGenreDao;
        GDAORadioListDao gDAORadioListDao = new GDAORadioListDao(daoConfig18, this);
        this.gDAORadioListDao = gDAORadioListDao;
        GDAORadioListDetailDao gDAORadioListDetailDao = new GDAORadioListDetailDao(daoConfig20, this);
        this.gDAORadioListDetailDao = gDAORadioListDetailDao;
        GDAOPodcastDao gDAOPodcastDao = new GDAOPodcastDao(daoConfig22, this);
        this.gDAOPodcastDao = gDAOPodcastDao;
        GDAOUserSelectedEntityDao gDAOUserSelectedEntityDao = new GDAOUserSelectedEntityDao(daoConfig24, this);
        this.gDAOUserSelectedEntityDao = gDAOUserSelectedEntityDao;
        GDAOSettingsDao gDAOSettingsDao = new GDAOSettingsDao(daoConfig26, this);
        this.gDAOSettingsDao = gDAOSettingsDao;
        GDAOOperationDao gDAOOperationDao = new GDAOOperationDao(daoConfig28, this);
        this.gDAOOperationDao = gDAOOperationDao;
        GDAOAppUsageEventDao gDAOAppUsageEventDao = new GDAOAppUsageEventDao(daoConfig30, this);
        this.gDAOAppUsageEventDao = gDAOAppUsageEventDao;
        GDAOAppPlaybackEventDao gDAOAppPlaybackEventDao = new GDAOAppPlaybackEventDao(daoConfig32, this);
        this.gDAOAppPlaybackEventDao = gDAOAppPlaybackEventDao;
        GDAOAppSongEventDao gDAOAppSongEventDao = new GDAOAppSongEventDao(daoConfig34, this);
        this.gDAOAppSongEventDao = gDAOAppSongEventDao;
        GDAOAppPodcastEventDao gDAOAppPodcastEventDao = new GDAOAppPodcastEventDao(daoConfig36, this);
        this.gDAOAppPodcastEventDao = gDAOAppPodcastEventDao;
        GDAOAppVolumeChangeEventDao gDAOAppVolumeChangeEventDao = new GDAOAppVolumeChangeEventDao(daoConfig38, this);
        this.gDAOAppVolumeChangeEventDao = gDAOAppVolumeChangeEventDao;
        registerDao(GDAOCountry.class, gDAOCountryDao);
        registerDao(GDAOState.class, gDAOStateDao);
        registerDao(GDAOCity.class, gDAOCityDao);
        registerDao(GDAOGenre.class, gDAOGenreDao);
        registerDao(GDAOStream.class, gDAOStreamDao);
        registerDao(GDAORadio.class, gDAORadioDao);
        registerDao(GDAORadioCity.class, gDAORadioCityDao);
        registerDao(GDAORadioGenre.class, gDAORadioGenreDao);
        registerDao(GDAORadioList.class, gDAORadioListDao);
        registerDao(GDAORadioListDetail.class, gDAORadioListDetailDao);
        registerDao(GDAOPodcast.class, gDAOPodcastDao);
        registerDao(GDAOUserSelectedEntity.class, gDAOUserSelectedEntityDao);
        registerDao(GDAOSettings.class, gDAOSettingsDao);
        registerDao(GDAOOperation.class, gDAOOperationDao);
        registerDao(GDAOAppUsageEvent.class, gDAOAppUsageEventDao);
        registerDao(GDAOAppPlaybackEvent.class, gDAOAppPlaybackEventDao);
        registerDao(GDAOAppSongEvent.class, gDAOAppSongEventDao);
        registerDao(GDAOAppPodcastEvent.class, gDAOAppPodcastEventDao);
        registerDao(GDAOAppVolumeChangeEvent.class, gDAOAppVolumeChangeEventDao);
    }

    public void clear() {
        this.gDAOCountryDaoConfig.clearIdentityScope();
        this.gDAOStateDaoConfig.clearIdentityScope();
        this.gDAOCityDaoConfig.clearIdentityScope();
        this.gDAOGenreDaoConfig.clearIdentityScope();
        this.gDAOStreamDaoConfig.clearIdentityScope();
        this.gDAORadioDaoConfig.clearIdentityScope();
        this.gDAORadioCityDaoConfig.clearIdentityScope();
        this.gDAORadioGenreDaoConfig.clearIdentityScope();
        this.gDAORadioListDaoConfig.clearIdentityScope();
        this.gDAORadioListDetailDaoConfig.clearIdentityScope();
        this.gDAOPodcastDaoConfig.clearIdentityScope();
        this.gDAOUserSelectedEntityDaoConfig.clearIdentityScope();
        this.gDAOSettingsDaoConfig.clearIdentityScope();
        this.gDAOOperationDaoConfig.clearIdentityScope();
        this.gDAOAppUsageEventDaoConfig.clearIdentityScope();
        this.gDAOAppPlaybackEventDaoConfig.clearIdentityScope();
        this.gDAOAppSongEventDaoConfig.clearIdentityScope();
        this.gDAOAppPodcastEventDaoConfig.clearIdentityScope();
        this.gDAOAppVolumeChangeEventDaoConfig.clearIdentityScope();
    }

    public GDAOAppPlaybackEventDao getGDAOAppPlaybackEventDao() {
        return this.gDAOAppPlaybackEventDao;
    }

    public GDAOAppPodcastEventDao getGDAOAppPodcastEventDao() {
        return this.gDAOAppPodcastEventDao;
    }

    public GDAOAppSongEventDao getGDAOAppSongEventDao() {
        return this.gDAOAppSongEventDao;
    }

    public GDAOAppUsageEventDao getGDAOAppUsageEventDao() {
        return this.gDAOAppUsageEventDao;
    }

    public GDAOAppVolumeChangeEventDao getGDAOAppVolumeChangeEventDao() {
        return this.gDAOAppVolumeChangeEventDao;
    }

    public GDAOCityDao getGDAOCityDao() {
        return this.gDAOCityDao;
    }

    public GDAOCountryDao getGDAOCountryDao() {
        return this.gDAOCountryDao;
    }

    public GDAOGenreDao getGDAOGenreDao() {
        return this.gDAOGenreDao;
    }

    public GDAOOperationDao getGDAOOperationDao() {
        return this.gDAOOperationDao;
    }

    public GDAOPodcastDao getGDAOPodcastDao() {
        return this.gDAOPodcastDao;
    }

    public GDAORadioCityDao getGDAORadioCityDao() {
        return this.gDAORadioCityDao;
    }

    public GDAORadioDao getGDAORadioDao() {
        return this.gDAORadioDao;
    }

    public GDAORadioGenreDao getGDAORadioGenreDao() {
        return this.gDAORadioGenreDao;
    }

    public GDAORadioListDao getGDAORadioListDao() {
        return this.gDAORadioListDao;
    }

    public GDAORadioListDetailDao getGDAORadioListDetailDao() {
        return this.gDAORadioListDetailDao;
    }

    public GDAOSettingsDao getGDAOSettingsDao() {
        return this.gDAOSettingsDao;
    }

    public GDAOStateDao getGDAOStateDao() {
        return this.gDAOStateDao;
    }

    public GDAOStreamDao getGDAOStreamDao() {
        return this.gDAOStreamDao;
    }

    public GDAOUserSelectedEntityDao getGDAOUserSelectedEntityDao() {
        return this.gDAOUserSelectedEntityDao;
    }
}
