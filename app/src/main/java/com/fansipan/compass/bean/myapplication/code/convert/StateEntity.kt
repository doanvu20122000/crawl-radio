package com.fansipan.compass.bean.myapplication.code.convert

data class StateEntity(
    val country: Int,
    val id: Int,
    val name: String
)