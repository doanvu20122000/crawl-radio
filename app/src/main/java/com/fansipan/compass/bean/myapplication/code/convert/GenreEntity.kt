package com.fansipan.compass.bean.myapplication.code.convert

data class GenreEntity(
    val id: String,
    val image_url: String,
    val name: String
)