package com.fansipan.compass.bean.myapplication.code.com.greendao;

import com.fansipan.compass.bean.myapplication.code.com.greendao.internal.SqlUtils;
import com.fansipan.compass.bean.myapplication.code.com.greendao.query.WhereCondition;

/* loaded from: classes4.dex */
public final class Property {
    public final String columnName;
    public final String name;
    public final int ordinal;
    public final boolean primaryKey;
    public final Class<?> type;

    public Property(int i, Class<?> cls, String str, boolean z, String str2) {
        this.ordinal = i;
        this.type = cls;
        this.name = str;
        this.primaryKey = z;
        this.columnName = str2;
    }

    public final WhereCondition.PropertyCondition eq(Object obj) {
        return new WhereCondition.PropertyCondition(this, "=?", obj);
    }

    public final WhereCondition.PropertyCondition in(Object... objArr) {
        StringBuilder sb = new StringBuilder(" IN (");
        int length = objArr.length;
        int i = SqlUtils.$r8$clinit;
        for (int i2 = 0; i2 < length; i2++) {
            if (i2 < length - 1) {
                sb.append("?,");
            } else {
                sb.append('?');
            }
        }
        sb.append(')');
        return new WhereCondition.PropertyCondition(this, sb.toString(), objArr);
    }
}
