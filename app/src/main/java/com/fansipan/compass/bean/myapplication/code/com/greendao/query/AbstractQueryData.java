package com.fansipan.compass.bean.myapplication.code.com.greendao.query;

import com.fansipan.compass.bean.myapplication.code.com.greendao.AbstractDao;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* loaded from: classes4.dex */
public abstract class AbstractQueryData<T, Q extends AbstractQuery<T>> {
    public final AbstractDao<T, ?> dao;
    public final String[] initialValues;
    public final HashMap queriesForThreads = new HashMap();
    public final String sql;

    public AbstractQueryData(AbstractDao<T, ?> abstractDao, String str, String[] strArr) {
        this.dao = abstractDao;
        this.sql = str;
        this.initialValues = strArr;
    }

    public abstract Q createQuery();

    public final Q forCurrentThread$1() {
        Q q;
        long id = Thread.currentThread().getId();
        synchronized (this.queriesForThreads) {
            WeakReference weakReference = (WeakReference) this.queriesForThreads.get(Long.valueOf(id));
            if (weakReference != null) {
                q = (Q) weakReference.get();
            } else {
                q = null;
            }
            if (q == null) {
                gc();
                q = createQuery();
                this.queriesForThreads.put(Long.valueOf(id), new WeakReference(q));
            } else {
                String[] strArr = this.initialValues;
                System.arraycopy(strArr, 0, q.parameters, 0, strArr.length);
            }
        }
        return q;
    }

    public final void gc() {
        synchronized (this.queriesForThreads) {
            Iterator it = this.queriesForThreads.entrySet().iterator();
            while (it.hasNext()) {
                if (((WeakReference) ((Map.Entry) it.next()).getValue()).get() == null) {
                    it.remove();
                }
            }
        }
    }
}
