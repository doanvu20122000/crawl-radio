package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao;

import java.io.Serializable;

/* loaded from: classes.dex */
public class GDAOAppPodcastEvent implements Serializable {
    private Long episode;
    private Long id;
    private String play_date;
    private Long podcast;
    private String start_date;
    private Boolean success;
    private Long time_played;

    public GDAOAppPodcastEvent() {
    }

    public Long getEpisode() {
        return this.episode;
    }

    public Long getId() {
        return this.id;
    }

    public String getPlay_date() {
        return this.play_date;
    }

    public Long getPodcast() {
        return this.podcast;
    }

    public String getStart_date() {
        return this.start_date;
    }

    public Boolean getSuccess() {
        return this.success;
    }

    public Long getTime_played() {
        return this.time_played;
    }

    public void setEpisode(Long l) {
        this.episode = l;
    }

    public void setId(Long l) {
        this.id = l;
    }

    public void setPlay_date(String str) {
        this.play_date = str;
    }

    public void setPodcast(Long l) {
        this.podcast = l;
    }

    public void setStart_date(String str) {
        this.start_date = str;
    }

    public void setSuccess(Boolean bool) {
        this.success = bool;
    }

    public void setTime_played(Long l) {
        this.time_played = l;
    }

    public GDAOAppPodcastEvent(Long l) {
        this.id = l;
    }

    public GDAOAppPodcastEvent(Long l, Long l2, Long l3, Long l4, String str, String str2, Boolean bool) {
        this.id = l;
        this.podcast = l2;
        this.episode = l3;
        this.time_played = l4;
        this.start_date = str;
        this.play_date = str2;
        this.success = bool;
    }
}
