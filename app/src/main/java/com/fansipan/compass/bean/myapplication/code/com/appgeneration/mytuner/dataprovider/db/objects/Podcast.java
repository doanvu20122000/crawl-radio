package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.objects;


import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao.DaoSession;
import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao.GDAOPodcast;

import java.util.Locale;

/* loaded from: classes.dex */
public class Podcast implements NavigationEntityItem {
    private static final String SIZE_REGEX = "\\d+x\\d+b";
    private static final String SIZE_TARGET_FORMAT = "%1$dx%1$db";
    private final String artistName;
    private final String artworkUrl;
    private final String description;
    private final long id;
    private final String name;

    public Podcast(long j, String str, String str2, String str3, String str4) {
        this.id = j;
        this.name = str;
        this.description = str2;
        this.artistName = str3;
        this.artworkUrl = str4;
    }

    private GDAOPodcast convertToDb() {
        return new GDAOPodcast(Long.valueOf(this.id), this.name, this.artistName, this.description, this.artworkUrl);
    }

    public static Podcast get(DaoSession daoSession, long j) {
        GDAOPodcast loadByRowId = daoSession.getGDAOPodcastDao().loadByRowId(j);
        if (loadByRowId != null) {
            return new Podcast(loadByRowId);
        }
        return null;
    }

    public static void insertOrReplace(DaoSession daoSession, Podcast podcast) {
        if (podcast != null) {
            daoSession.getGDAOPodcastDao().insertOrReplace(podcast.convertToDb());
        }
    }

    public String getArtist() {
        return this.artistName;
    }

    public String getArtworkUrl() {
        return this.artworkUrl;
    }

    public String getDescription() {
        return this.description;
    }

    public long getId() {
        return this.id;
    }


    public String getName() {
        return this.name;
    }

    @Override // com.appgeneration.mytuner.dataprovider.db.objects.NavigationEntityItem
    public long getObjectId() {
        return getId();
    }

    public int hashCode() {
        return Long.valueOf(this.id).hashCode();
    }

    private Podcast(GDAOPodcast gDAOPodcast) {
        this.id = gDAOPodcast.getId().longValue();
        this.name = gDAOPodcast.getName();
        this.artistName = gDAOPodcast.getArtist();
        this.description = gDAOPodcast.getDescription();
        this.artworkUrl = gDAOPodcast.getImage_url();
    }
}
