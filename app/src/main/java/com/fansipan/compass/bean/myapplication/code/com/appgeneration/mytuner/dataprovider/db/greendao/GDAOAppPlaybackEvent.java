package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao;

import java.io.Serializable;

/* loaded from: classes.dex */
public class GDAOAppPlaybackEvent implements Serializable {
    private String end_date;
    private String error_code;
    private String error_description;
    private String error_domain;
    private Long id;
    private Boolean metadata;
    private String play_date;

    /* renamed from: radio  reason: collision with root package name */
    private Long f533radio;
    private String source;
    private String start_date;
    private Long stream;
    private String stream_url;
    private Boolean success;

    public GDAOAppPlaybackEvent() {
    }

    public String getEnd_date() {
        return this.end_date;
    }

    public String getError_code() {
        return this.error_code;
    }

    public String getError_description() {
        return this.error_description;
    }

    public String getError_domain() {
        return this.error_domain;
    }

    public Long getId() {
        return this.id;
    }

    public Boolean getMetadata() {
        return this.metadata;
    }

    public String getPlay_date() {
        return this.play_date;
    }

    public Long getRadio() {
        return this.f533radio;
    }

    public String getSource() {
        return this.source;
    }

    public String getStart_date() {
        return this.start_date;
    }

    public Long getStream() {
        return this.stream;
    }

    public String getStream_url() {
        return this.stream_url;
    }

    public Boolean getSuccess() {
        return this.success;
    }

    public void setEnd_date(String str) {
        this.end_date = str;
    }

    public void setError_code(String str) {
        this.error_code = str;
    }

    public void setError_description(String str) {
        this.error_description = str;
    }

    public void setError_domain(String str) {
        this.error_domain = str;
    }

    public void setId(Long l) {
        this.id = l;
    }

    public void setMetadata(Boolean bool) {
        this.metadata = bool;
    }

    public void setPlay_date(String str) {
        this.play_date = str;
    }

    public void setRadio(Long l) {
        this.f533radio = l;
    }

    public void setSource(String str) {
        this.source = str;
    }

    public void setStart_date(String str) {
        this.start_date = str;
    }

    public void setStream(Long l) {
        this.stream = l;
    }

    public void setStream_url(String str) {
        this.stream_url = str;
    }

    public void setSuccess(Boolean bool) {
        this.success = bool;
    }

    public GDAOAppPlaybackEvent(Long l) {
        this.id = l;
    }

    public GDAOAppPlaybackEvent(Long l, Long l2, String str, String str2, String str3, Boolean bool, Long l3, String str4, Boolean bool2, String str5, String str6, String str7, String str8) {
        this.id = l;
        this.f533radio = l2;
        this.start_date = str;
        this.play_date = str2;
        this.end_date = str3;
        this.success = bool;
        this.stream = l3;
        this.stream_url = str4;
        this.metadata = bool2;
        this.error_domain = str5;
        this.error_code = str6;
        this.error_description = str7;
        this.source = str8;
    }
}
