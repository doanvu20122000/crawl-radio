package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import com.fansipan.compass.bean.myapplication.code.com.POBRequest$AdPosition$EnumUnboxingLocalUtility;
import com.fansipan.compass.bean.myapplication.code.com.greendao.AbstractDao;
import com.fansipan.compass.bean.myapplication.code.com.greendao.Property;
import com.fansipan.compass.bean.myapplication.code.com.greendao.database.Database;
import com.fansipan.compass.bean.myapplication.code.com.greendao.database.DatabaseStatement;
import com.fansipan.compass.bean.myapplication.code.com.greendao.internal.DaoConfig;

/* loaded from: classes.dex */
public class GDAOOperationDao extends AbstractDao<GDAOOperation, Void> {
    public static final String TABLENAME = "operations";

    /* loaded from: classes.dex */
    public static class Properties {
        public static final Property Object_id = new Property(0, String.class, "object_id", false, "OBJECT_ID");
        public static final Property Operation_type = new Property(1, Integer.class, "operation_type", false, "OPERATION_TYPE");
        public static final Property Object_type = new Property(2, Integer.class, "object_type", false, "OBJECT_TYPE");
        public static final Property Timestamp = new Property(3, Long.class, "timestamp", false, "TIMESTAMP");
    }

    public GDAOOperationDao(DaoConfig daoConfig) {
        super(daoConfig);
    }

    public static void createTable(Database database, boolean z) {
        String str;
        if (z) {
            str = "IF NOT EXISTS ";
        } else {
            str = "";
        }
        database.execSQL("CREATE TABLE " + str + "\"operations\" (\"OBJECT_ID\" TEXT,\"OPERATION_TYPE\" INTEGER,\"OBJECT_TYPE\" INTEGER,\"TIMESTAMP\" INTEGER);");
        StringBuilder sb = new StringBuilder();
        sb.append("CREATE UNIQUE INDEX ");
        POBRequest$AdPosition$EnumUnboxingLocalUtility.m(sb, str, "IDX_operations_OBJECT_ID_OPERATION_TYPE_OBJECT_TYPE_TIMESTAMP ON \"operations\" (\"OBJECT_ID\",\"OPERATION_TYPE\",\"OBJECT_TYPE\",\"TIMESTAMP\");", database);
    }

    public static void dropTable(Database database, boolean z) {
        String str;
        StringBuilder m = new StringBuilder("DROP TABLE ");
        if (z) {
            str = "IF EXISTS ";
        } else {
            str = "";
        }
        POBRequest$AdPosition$EnumUnboxingLocalUtility.m(m, str, "\"operations\"", database);
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public Void getKey(GDAOOperation gDAOOperation) {
        return null;
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public boolean hasKey(GDAOOperation gDAOOperation) {
        return false;
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final boolean isEntityUpdateable() {
        return true;
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public Void readKey(Cursor cursor, int i) {
        return null;
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final Void updateKeyAfterInsert(GDAOOperation gDAOOperation, long j) {
        return null;
    }

    public GDAOOperationDao(DaoConfig daoConfig, DaoSession daoSession) {
        super(daoConfig, daoSession);
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final void bindValues(DatabaseStatement databaseStatement, GDAOOperation gDAOOperation) {
        databaseStatement.clearBindings();
        String object_id = gDAOOperation.getObject_id();
        if (object_id != null) {
            databaseStatement.bindString(1, object_id);
        }
        Integer operation_type = gDAOOperation.getOperation_type();
        if (operation_type != null) {
            databaseStatement.bindLong(2, operation_type.intValue());
        }
        Integer object_type = gDAOOperation.getObject_type();
        if (object_type != null) {
            databaseStatement.bindLong(3, object_type.intValue());
        }
        Long timestamp = gDAOOperation.getTimestamp();
        if (timestamp != null) {
            databaseStatement.bindLong(4, timestamp.longValue());
        }
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public GDAOOperation readEntity(Cursor cursor, int i) {
        int i2 = i + 0;
        int i3 = i + 1;
        int i4 = i + 2;
        int i5 = i + 3;
        return new GDAOOperation(cursor.isNull(i2) ? null : cursor.getString(i2), cursor.isNull(i3) ? null : Integer.valueOf(cursor.getInt(i3)), cursor.isNull(i4) ? null : Integer.valueOf(cursor.getInt(i4)), cursor.isNull(i5) ? null : Long.valueOf(cursor.getLong(i5)));
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public void readEntity(Cursor cursor, GDAOOperation gDAOOperation, int i) {
        int i2 = i + 0;
        gDAOOperation.setObject_id(cursor.isNull(i2) ? null : cursor.getString(i2));
        int i3 = i + 1;
        gDAOOperation.setOperation_type(cursor.isNull(i3) ? null : Integer.valueOf(cursor.getInt(i3)));
        int i4 = i + 2;
        gDAOOperation.setObject_type(cursor.isNull(i4) ? null : Integer.valueOf(cursor.getInt(i4)));
        int i5 = i + 3;
        gDAOOperation.setTimestamp(cursor.isNull(i5) ? null : Long.valueOf(cursor.getLong(i5)));
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final void bindValues(SQLiteStatement sQLiteStatement, GDAOOperation gDAOOperation) {
        sQLiteStatement.clearBindings();
        String object_id = gDAOOperation.getObject_id();
        if (object_id != null) {
            sQLiteStatement.bindString(1, object_id);
        }
        Integer operation_type = gDAOOperation.getOperation_type();
        if (operation_type != null) {
            sQLiteStatement.bindLong(2, operation_type.intValue());
        }
        Integer object_type = gDAOOperation.getObject_type();
        if (object_type != null) {
            sQLiteStatement.bindLong(3, object_type.intValue());
        }
        Long timestamp = gDAOOperation.getTimestamp();
        if (timestamp != null) {
            sQLiteStatement.bindLong(4, timestamp.longValue());
        }
    }
}
