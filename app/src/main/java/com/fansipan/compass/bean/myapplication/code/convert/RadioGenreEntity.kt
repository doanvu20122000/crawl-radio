package com.fansipan.compass.bean.myapplication.code.convert

data class RadioGenreEntity(
    val genre: Int,
    val radio: Int
)