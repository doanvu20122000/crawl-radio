package com.fansipan.compass.bean.myapplication.code.convert

data class StreamEntity(
    val id: Int,
    val params_json: String,
    val quality: Int,
    val radio: Int,
    val rank: Int,
    val url: String
)