package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao;

import android.database.Cursor;

import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.objects.UserLocation;
import com.fansipan.compass.bean.myapplication.code.com.greendao.DaoException;
import com.fansipan.compass.bean.myapplication.code.com.greendao.Property;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/* loaded from: classes.dex */
public class GDAORadio implements Serializable {
    private Long country;
    private GDAOCountry countryObject;
    private transient Long countryObject__resolvedKey;
    private transient DaoSession daoSession;
    private List<GDAOStream> gDAOStreamList;
    private String geolocation_codes;
    private Boolean has_metadata;
    private Boolean hidden;
    private Long id;
    private Boolean ignore_metadata;
    private String image_url;
    private String mSubtitle;
    private transient GDAORadioDao myDao;
    private String name;
    private Long ord;
    private String status;

    public GDAORadio() {
    }

    private void __throwIfDetached() {
        if (this.myDao != null) {
            return;
        }
        throw new DaoException("Entity is detached from DAO context");
    }

    public void __setDaoSession(DaoSession daoSession) {
        GDAORadioDao gDAORadioDao;
        this.daoSession = daoSession;
        if (daoSession != null) {
            gDAORadioDao = daoSession.getGDAORadioDao();
        } else {
            gDAORadioDao = null;
        }
        this.myDao = gDAORadioDao;
    }

    public void delete() {
        __throwIfDetached();
        this.myDao.delete(this);
    }

    public Long getCountry() {
        return this.country;
    }

    public GDAOCountry getCountryObject() {
        Long l = this.country;
        Long l2 = this.countryObject__resolvedKey;
        if (l2 == null || !l2.equals(l)) {
            __throwIfDetached();
            GDAOCountry load = this.daoSession.getGDAOCountryDao().load(l);
            synchronized (this) {
                this.countryObject = load;
                this.countryObject__resolvedKey = l;
            }
        }
        return this.countryObject;
    }

    public String getFrequencyDescription(UserLocation userLocation) {
        String str;
        if (this.mSubtitle == null && this.daoSession != null && userLocation != null) {
            double latitude = userLocation.getLatitude();
            Cursor rawQuery = this.daoSession.getDatabase().rawQuery("SELECT CASE WHEN c.id != 0 THEN c.name ELSE '' END || CASE WHEN rc.frequency != '' THEN ' - ' || rc.frequency ELSE '' END\nFROM radios_cities rc INNER JOIN city c ON c.id = rc.city\nWHERE rc.radio = ?1 \nORDER BY (ABS(?2-c.latitude) * ABS(?2-c.latitude)) + (ABS(?3-c.longitude) * ABS(?3-c.longitude)) * ?4 ASC\nLIMIT 1", new String[]{Long.toString(this.id.longValue()), String.valueOf(latitude), String.valueOf(userLocation.getLongitude()), String.valueOf(Math.pow(Math.cos(Math.toRadians(latitude)), 2.0d))});
            if (rawQuery != null && rawQuery.moveToFirst()) {
                this.mSubtitle = rawQuery.getString(0);
                rawQuery.close();
            }
        }
        String str2 = this.mSubtitle;
        if (str2 == null || str2.isEmpty()) {
            GDAOCountry countryObject = getCountryObject();
            if (countryObject != null) {
                str = countryObject.getName();
            } else {
                str = "";
            }
            this.mSubtitle = str;
        }
        return this.mSubtitle;
    }

    public List<GDAOStream> getGDAOStreamList() {
        if (this.gDAOStreamList == null) {
            __throwIfDetached();
            List<GDAOStream> _queryGDAORadio_GDAOStreamList = this.daoSession.getGDAOStreamDao().queryGDAORadio_GDAOStreamList(this.id);
            synchronized (this) {
                if (this.gDAOStreamList == null) {
                    this.gDAOStreamList = _queryGDAORadio_GDAOStreamList;
                }
            }
        }
        return this.gDAOStreamList;
    }

    public String getGeolocation_codes() {
        return this.geolocation_codes;
    }

    public Boolean getHas_metadata() {
        return this.has_metadata;
    }

    public Boolean getHidden() {
        return this.hidden;
    }

    public Long getId() {
        return this.id;
    }

    public Boolean getIgnore_metadata() {
        return this.ignore_metadata;
    }

    public String getImage_url() {
        return this.image_url;
    }

    public String getName() {
        return this.name;
    }

    public Long getOrd() {
        return this.ord;
    }

    public String getStatus() {
        return this.status;
    }

    public void refresh() {
        __throwIfDetached();
        this.myDao.refresh(this);
    }

    public synchronized void resetGDAOStreamList() {
        this.gDAOStreamList = null;
    }

    public void setCountry(Long l) {
        this.country = l;
    }

    public void setCountryObject(GDAOCountry gDAOCountry) {
        Long id;
        synchronized (this) {
            this.countryObject = gDAOCountry;
            if (gDAOCountry == null) {
                id = null;
            } else {
                id = gDAOCountry.getId();
            }
            this.country = id;
            this.countryObject__resolvedKey = id;
        }
    }

    public void setGeolocation_codes(String str) {
        this.geolocation_codes = str;
    }

    public void setHas_metadata(Boolean bool) {
        this.has_metadata = bool;
    }

    public void setHidden(Boolean bool) {
        this.hidden = bool;
    }

    public void setId(Long l) {
        this.id = l;
    }

    public void setIgnore_metadata(Boolean bool) {
        this.ignore_metadata = bool;
    }

    public void setImage_url(String str) {
        this.image_url = str;
    }

    public void setName(String str) {
        this.name = str;
    }

    public void setOrd(Long l) {
        this.ord = l;
    }

    public void setStatus(String str) {
        this.status = str;
    }

    public void update() {
        __throwIfDetached();
        this.myDao.update(this);
    }

    public GDAORadio(Long l) {
        this.id = l;
    }

    public GDAORadio(Long l, Long l2, String str, String str2, Boolean bool, Boolean bool2, Boolean bool3, String str3, String str4, Long l3) {
        this.id = l;
        this.ord = l2;
        this.name = str;
        this.image_url = str2;
        this.hidden = bool;
        this.has_metadata = bool2;
        this.ignore_metadata = bool3;
        this.geolocation_codes = str3;
        this.status = str4;
        this.country = l3;
    }

    public List<GDAORadio> getRadioInDatabase(Cursor cursor) {
        ArrayList<GDAORadio> data = new ArrayList<GDAORadio>();
        if (cursor != null) {
            try {
                while (cursor.moveToNext()) {
                    long column1 = cursor.getLong(cursor.getColumnIndexOrThrow(GDAORadioDao.Properties.columnName().get(0)));
                    long column2 = cursor.getLong(cursor.getColumnIndexOrThrow(GDAORadioDao.Properties.columnName().get(1)));
                    String column3 = cursor.getString(cursor.getColumnIndexOrThrow(GDAORadioDao.Properties.columnName().get(2)));
                    String column4 = cursor.getString(cursor.getColumnIndexOrThrow(GDAORadioDao.Properties.columnName().get(3)));
                    Boolean column5 = cursor.getInt(cursor.getColumnIndexOrThrow(GDAORadioDao.Properties.columnName().get(4))) > 0;
                    Boolean column6 = cursor.getInt(cursor.getColumnIndexOrThrow(GDAORadioDao.Properties.columnName().get(5))) > 0;
                    Boolean column7 = cursor.getInt(cursor.getColumnIndexOrThrow(GDAORadioDao.Properties.columnName().get(6))) > 0;
                    String column8 = cursor.getString(cursor.getColumnIndexOrThrow(GDAORadioDao.Properties.columnName().get(7)));
                    String column9 = cursor.getString(cursor.getColumnIndexOrThrow(GDAORadioDao.Properties.columnName().get(8)));
                    long column10 = cursor.getLong(cursor.getColumnIndexOrThrow(GDAORadioDao.Properties.columnName().get(9)));
                    data.add(new GDAORadio(column1, column2, column3, column4, column5, column6, column7, column8, column9, column10));
                    // Do something with the data
                }
            } finally {
                cursor.close();
            }
        }
        return data;
    }
}
