package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.Set;

/* loaded from: classes.dex */
public class PreferencesHelpers {

    /* loaded from: classes.dex */
    public interface BatchEditor {
        void onEdit(SharedPreferences.Editor editor);
    }

    public static boolean getBooleanSetting(Context context, int i, boolean z) {
        return context.getSharedPreferences(PreferenceManager.getDefaultSharedPreferencesName(context), 0).getBoolean(context.getResources().getString(i), z);
    }

    public static int getIntSetting(Context context, int i, int i2) {
        return context.getSharedPreferences(PreferenceManager.getDefaultSharedPreferencesName(context), 0).getInt(context.getString(i), i2);
    }

    public static long getLongSetting(Context context, int i, long j) {
        return context.getSharedPreferences(PreferenceManager.getDefaultSharedPreferencesName(context), 0).getLong(context.getResources().getString(i), j);
    }

    public static Set<String> getStringSet(Context context, int i) {
        return context.getSharedPreferences(PreferenceManager.getDefaultSharedPreferencesName(context), 0).getStringSet(context.getResources().getString(i), null);
    }

    public static String getStringSetting(Context context, int i) {
        return getStringSetting(context, i, "");
    }

    public static void setBatchSettings(Context context, BatchEditor batchEditor) {
        SharedPreferences.Editor edit = context.getSharedPreferences(PreferenceManager.getDefaultSharedPreferencesName(context), 0).edit();
        batchEditor.onEdit(edit);
        edit.apply();
    }

    public static void setBooleanSetting(Context context, int i, boolean z) {
        SharedPreferences.Editor edit = context.getSharedPreferences(PreferenceManager.getDefaultSharedPreferencesName(context), 0).edit();
        edit.putBoolean(context.getResources().getString(i), z);
        edit.apply();
    }

    public static void setIntSetting(Context context, int i, int i2) {
        SharedPreferences.Editor edit = context.getSharedPreferences(PreferenceManager.getDefaultSharedPreferencesName(context), 0).edit();
        edit.putInt(context.getResources().getString(i), i2);
        edit.apply();
    }

    public static void setLongSetting(Context context, int i, long j) {
        SharedPreferences.Editor edit = context.getSharedPreferences(PreferenceManager.getDefaultSharedPreferencesName(context), 0).edit();
        edit.putLong(context.getResources().getString(i), j);
        edit.apply();
    }

    public static void setStringSet(Context context, int i, Set<String> set) {
        SharedPreferences.Editor edit = context.getSharedPreferences(PreferenceManager.getDefaultSharedPreferencesName(context), 0).edit();
        edit.putStringSet(context.getResources().getString(i), set);
        edit.apply();
    }

    public static void setStringSetting(Context context, int i, String str) {
        if (str == null) {
            return;
        }
        SharedPreferences.Editor edit = context.getSharedPreferences(PreferenceManager.getDefaultSharedPreferencesName(context), 0).edit();
        edit.putString(context.getResources().getString(i), str);
        edit.apply();
    }

    public static String getStringSetting(Context context, int i, String str) {
        return context.getSharedPreferences(PreferenceManager.getDefaultSharedPreferencesName(context), 0).getString(context.getResources().getString(i), str);
    }
}
