package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.objects;

/* loaded from: classes.dex */
public class URLWrapper {
    private final String singleURL;

    public URLWrapper(String str) {
        this.singleURL = str;
    }

    public String getSingleURL() {
        return this.singleURL;
    }

}
