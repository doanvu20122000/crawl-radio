package com.fansipan.compass.bean.myapplication.code.com.greendao.async;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/* loaded from: classes4.dex */
public final class AsyncOperationExecutor implements Runnable, Handler.Callback {
    public static ExecutorService executorService = Executors.newCachedThreadPool();
    public int countOperationsCompleted;
    public final LinkedBlockingQueue queue = new LinkedBlockingQueue();

    @Override // android.os.Handler.Callback
    public final boolean handleMessage(Message message) {
        return false;
    }

    @Override // java.lang.Runnable
    public final void run() {
        while (true) {
            try {
                AsyncOperation asyncOperation = (AsyncOperation) this.queue.poll(1L, TimeUnit.SECONDS);
                if (asyncOperation == null) {
                    synchronized (this) {
                        asyncOperation = (AsyncOperation) this.queue.poll();
                        if (asyncOperation == null) {
                            return;
                        }
                    }
                }
                System.currentTimeMillis();
                try {
                    break;
                } catch (Throwable unused) {
                    System.currentTimeMillis();
                    synchronized (asyncOperation) {
                        asyncOperation.notifyAll();
                        synchronized (this) {
                            int i = this.countOperationsCompleted + 1;
                            this.countOperationsCompleted = i;
                            if (i == 0) {
                                notifyAll();
                            }
                        }
                    }
                }
            } catch (InterruptedException e2) {
                Log.w("greenDAO", Thread.currentThread().getName() + " was interruppted", e2);
                return;
            } finally {
            }
        }
    }
}
