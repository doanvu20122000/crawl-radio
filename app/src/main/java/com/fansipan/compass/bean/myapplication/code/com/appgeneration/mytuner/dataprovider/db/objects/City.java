package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.objects;

import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao.DaoSession;
import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao.GDAOCity;
import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao.GDAOCountry;
import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao.GDAOState;
import com.fansipan.compass.bean.myapplication.code.com.greendao.AbstractDao;
import com.fansipan.compass.bean.myapplication.code.com.greendao.query.Query;
import com.fansipan.compass.bean.myapplication.code.com.greendao.query.QueryBuilder;

import java.util.Locale;

/* loaded from: classes.dex */
public class City implements NavigationEntityItem {
    private final GDAOCity mDbCity;

    private City(GDAOCity gDAOCity) {
        this.mDbCity = gDAOCity;
    }

    public static City getClosest(DaoSession daoSession, UserLocation userLocation) {
        if (userLocation == null) {
            return null;
        }
        QueryBuilder<GDAOCity> queryBuilder = daoSession.getGDAOCityDao().queryBuilder();
        String format = String.format(Locale.US, "(ABS(%1$f-T.latitude) * ABS(%1$f-T.latitude)) + (ABS(%2$f-T.longitude) * ABS(%2$f-T.longitude)) * %3$f ASC", Double.valueOf(userLocation.getLatitude()), Double.valueOf(userLocation.getLongitude()), Double.valueOf(Math.pow(Math.cos(Math.toRadians(userLocation.getLatitude())), 2.0d)));
        StringBuilder sb = queryBuilder.orderBuilder;
        if (sb == null) {
            queryBuilder.orderBuilder = new StringBuilder();
        } else if (sb.length() > 0) {
            queryBuilder.orderBuilder.append(",");
        }
        queryBuilder.orderBuilder.append(format);
        queryBuilder.limit(1);
        Query<GDAOCity> build = queryBuilder.build();
        build.checkThread();
        GDAOCity gDAOCity =
                (GDAOCity) ((AbstractDao) build.dao).loadUniqueAndCloseCursor(build.dao.getDatabase().rawQuery(build.sql,
                        build.parameters));
        if (gDAOCity == null) {
            return null;
        }
        return new City(gDAOCity);
    }

    public long getCountry() {
        return this.mDbCity.getCountry().longValue();
    }

    public Country getCountryObject() {
        GDAOCountry countryObject = this.mDbCity.getCountryObject();
        if (countryObject != null) {
            return new Country(countryObject);
        }
        return null;
    }

    public String getFullName() {
        GDAOCountry gDAOCountry;
        String str;
        GDAOCity gDAOCity = this.mDbCity;
        if (gDAOCity != null) {
            gDAOCountry = gDAOCity.getCountryObject();
        } else {
            gDAOCountry = null;
        }
        if (gDAOCountry != null && gDAOCountry.getUse_states().booleanValue()) {
            GDAOState stateObject = this.mDbCity.getStateObject();
            StringBuilder sb = new StringBuilder();
            sb.append(getName());
            if (stateObject != null) {
                StringBuilder m = new StringBuilder(", ");
                m.append(stateObject.getName());
                str = m.toString();
            } else {
                str = "";
            }
            sb.append(str);
            return sb.toString();
        }
        return getName();
    }

    public long getId() {
        return this.mDbCity.getId().longValue();
    }

    public float getLatitude() {
        return this.mDbCity.getLatitude().floatValue();
    }

    public float getLongitude() {
        return this.mDbCity.getLongitude().floatValue();
    }

    public String getName() {
        return this.mDbCity.getName();
    }

    @Override // com.appgeneration.mytuner.dataprovider.db.objects.NavigationEntityItem
    public long getObjectId() {
        return getId();
    }

    public long getState() {
        return this.mDbCity.getState().longValue();
    }
}
