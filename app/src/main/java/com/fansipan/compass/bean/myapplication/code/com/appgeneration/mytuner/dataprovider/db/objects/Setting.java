package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.objects;

import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao.DaoSession;
import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao.GDAOSettings;
import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao.GDAOSettingsDao;
import com.fansipan.compass.bean.myapplication.code.com.greendao.AbstractDao;
import com.fansipan.compass.bean.myapplication.code.com.greendao.query.Query;
import com.fansipan.compass.bean.myapplication.code.com.greendao.query.QueryBuilder;
import com.fansipan.compass.bean.myapplication.code.com.greendao.query.WhereCondition;

/* loaded from: classes.dex */
public class Setting {
    public static final String SETTING_CURRENT_LOCALE = "setting_key.current_locale";
    public static final String SETTING_DATABASE_VERSION = "setting_key.database_version";
    public static final String SETTING_NETWORK_REQUESTS_LAST_RECEIVED = "setting_key.network_requests.last_received";
    public static final String SETTING_NETWORK_REQUESTS_LAST_REQUESTED = "setting_key.network_requests.last_requested";
    public static final String SETTING_NETWORK_REQUESTS_UPDATE_INTERVAL = "setting_key.network_requests.updated_data.time_interval";
    public static final String SETTING_SERVER_TIMESTAMP = "setting_key.server_timestamp";

    public static Long getLongSetting(DaoSession daoSession, String str, Long l) {
        String stringSetting = getStringSetting(daoSession, str);
        if (stringSetting == null) {
            return l;
        }
        try {
            return Long.valueOf(Long.parseLong(stringSetting));
        } catch (NumberFormatException unused) {
            return l;
        }
    }

    public static String getStringSetting(DaoSession daoSession, String str, String str2) {
        if (daoSession != null) {
            if (str != null) {
                QueryBuilder<GDAOSettings> queryBuilder = daoSession.getGDAOSettingsDao().queryBuilder();
                queryBuilder.whereCollector.add(GDAOSettingsDao.Properties.Key.eq(str), new WhereCondition[0]);
                Query<GDAOSettings> build = queryBuilder.build();
                build.checkThread();
                GDAOSettings gDAOSettings =
                        (GDAOSettings) ((AbstractDao) build.dao).loadUniqueAndCloseCursor(build.dao.getDatabase().rawQuery(build.sql,
                                build.parameters));
                return gDAOSettings != null ? gDAOSettings.getValue() : str2;
            }
            throw new NullPointerException("The key string should not be null!");
        }
        throw new NullPointerException("The session should not be null!");
    }

    public static void setSetting(DaoSession daoSession, String str, String str2) {
        GDAOSettings gDAOSettings = new GDAOSettings();
        gDAOSettings.setKey(str);
        gDAOSettings.setValue(str2);
        daoSession.getGDAOSettingsDao().insertOrReplace(gDAOSettings);
    }

    public static String getStringSetting(DaoSession daoSession, String str) {
        return getStringSetting(daoSession, str, null);
    }
}
