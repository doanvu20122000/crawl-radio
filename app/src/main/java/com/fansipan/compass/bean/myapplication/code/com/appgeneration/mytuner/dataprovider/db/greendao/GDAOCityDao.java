package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import com.fansipan.compass.bean.myapplication.code.com.greendao.AbstractDao;
import com.fansipan.compass.bean.myapplication.code.com.greendao.Property;
import com.fansipan.compass.bean.myapplication.code.com.greendao.database.Database;
import com.fansipan.compass.bean.myapplication.code.com.greendao.database.DatabaseStatement;
import com.fansipan.compass.bean.myapplication.code.com.greendao.internal.DaoConfig;
import com.fansipan.compass.bean.myapplication.code.com.greendao.internal.SqlUtils;

/* loaded from: classes.dex */
public class GDAOCityDao extends AbstractDao<GDAOCity, Long> {
    public static final String TABLENAME = "city";
    private DaoSession daoSession;
    private String selectDeep;

    /* loaded from: classes.dex */
    public static class Properties {
        public static final Property Id = new Property(0, Long.class, "id", true, "ID");
        public static final Property Name = new Property(1, String.class, "name", false, "NAME");
        public static final Property Latitude = new Property(2, Float.class, "latitude", false, "LATITUDE");
        public static final Property Longitude = new Property(3, Float.class, "longitude", false, "LONGITUDE");
        public static final Property State = new Property(4, Long.class, "state", false, "STATE");
        public static final Property Country = new Property(5, Long.class, "country", false, "COUNTRY");

    }

    public GDAOCityDao(DaoConfig daoConfig) {
        super(daoConfig);
    }

    public static void createTable(Database database, boolean z) {
        String str;
        if (z) {
            str = "IF NOT EXISTS ";
        } else {
            str = "";
        }
        database.execSQL("CREATE TABLE " + str + "\"city\" (\"ID\" INTEGER PRIMARY KEY ,\"NAME\" TEXT,\"LATITUDE\" REAL,\"LONGITUDE\" REAL,\"STATE\" INTEGER,\"COUNTRY\" INTEGER);");
    }

    public static void dropTable(Database database, boolean z) {
        String str;
        StringBuilder m = new StringBuilder("DROP TABLE ");
        if (z) {
            str = "IF EXISTS ";
        } else {
            str = "";
        }
        m.append(str);
        m.append("\"city\"");
        database.execSQL(m.toString());
    }

    public String getSelectDeep() {
        if (this.selectDeep == null) {
            StringBuilder sb = new StringBuilder("SELECT ");
            sb.append(',');
            SqlUtils.appendColumns(sb, "T0", this.daoSession.getGDAOStateDao().getAllColumns());
            sb.append(',');
            SqlUtils.appendColumns(sb, "T1", this.daoSession.getGDAOCountryDao().getAllColumns());
            sb.append(" FROM city T");
            sb.append(" LEFT JOIN state T0 ON T.\"STATE\"=T0.\"ID\"");
            sb.append(" LEFT JOIN country T1 ON T.\"COUNTRY\"=T1.\"ID\"");
            sb.append(' ');
            this.selectDeep = sb.toString();
        }
        return this.selectDeep;
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final boolean isEntityUpdateable() {
        return true;
    }

    public GDAOCity loadCurrentDeep(Cursor cursor, boolean z) {
        GDAOCity loadCurrent = loadCurrent(cursor, 0, z);
        int length = getAllColumns().length;
        loadCurrent.setStateObject((GDAOState) loadCurrentOther(this.daoSession.getGDAOStateDao(), cursor, length));
        loadCurrent.setCountryObject((GDAOCountry) loadCurrentOther(this.daoSession.getGDAOCountryDao(), cursor, length + this.daoSession.getGDAOStateDao().getAllColumns().length));
        return loadCurrent;
    }

    public GDAOCity loadDeep(Long l) {
        assertSinglePk();
        if (l == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder(getSelectDeep());
        sb.append("WHERE ");
        Cursor rawQuery = this.db.rawQuery(sb.toString(), new String[]{l.toString()});
        try {
            if (!rawQuery.moveToFirst()) {
                return null;
            }
            if (rawQuery.isLast()) {
                return loadCurrentDeep(rawQuery, true);
            }
            throw new IllegalStateException("Expected unique result, but count was " + rawQuery.getCount());
        } finally {
            rawQuery.close();
        }
    }

    public GDAOCityDao(DaoConfig daoConfig, DaoSession daoSession) {
        super(daoConfig, daoSession);
        this.daoSession = daoSession;
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final void attachEntity(GDAOCity gDAOCity) {
//        super.attachEntity((GDAOCityDao) gDAOCity);
        gDAOCity.__setDaoSession(this.daoSession);
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public Long getKey(GDAOCity gDAOCity) {
        if (gDAOCity != null) {
            return gDAOCity.getId();
        }
        return null;
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public boolean hasKey(GDAOCity gDAOCity) {
        return gDAOCity.getId() != null;
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public Long readKey(Cursor cursor, int i) {
        int i2 = i + 0;
        if (cursor.isNull(i2)) {
            return null;
        }
        return Long.valueOf(cursor.getLong(i2));
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final Long updateKeyAfterInsert(GDAOCity gDAOCity, long j) {
        gDAOCity.setId(Long.valueOf(j));
        return Long.valueOf(j);
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final void bindValues(DatabaseStatement databaseStatement, GDAOCity gDAOCity) {
        databaseStatement.clearBindings();
        Long id = gDAOCity.getId();
        if (id != null) {
            databaseStatement.bindLong(1, id.longValue());
        }
        String name = gDAOCity.getName();
        if (name != null) {
            databaseStatement.bindString(2, name);
        }
        Float latitude = gDAOCity.getLatitude();
        if (latitude != null) {
            databaseStatement.bindDouble(3, latitude.floatValue());
        }
        Float longitude = gDAOCity.getLongitude();
        if (longitude != null) {
            databaseStatement.bindDouble(4, longitude.floatValue());
        }
        Long state = gDAOCity.getState();
        if (state != null) {
            databaseStatement.bindLong(5, state.longValue());
        }
        Long country = gDAOCity.getCountry();
        if (country != null) {
            databaseStatement.bindLong(6, country.longValue());
        }
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public GDAOCity readEntity(Cursor cursor, int i) {
        int i2 = i + 0;
        Long valueOf = cursor.isNull(i2) ? null : Long.valueOf(cursor.getLong(i2));
        int i3 = i + 1;
        String string = cursor.isNull(i3) ? null : cursor.getString(i3);
        int i4 = i + 2;
        Float valueOf2 = cursor.isNull(i4) ? null : Float.valueOf(cursor.getFloat(i4));
        int i5 = i + 3;
        Float valueOf3 = cursor.isNull(i5) ? null : Float.valueOf(cursor.getFloat(i5));
        int i6 = i + 4;
        int i7 = i + 5;
        return new GDAOCity(valueOf, string, valueOf2, valueOf3, cursor.isNull(i6) ? null : Long.valueOf(cursor.getLong(i6)), cursor.isNull(i7) ? null : Long.valueOf(cursor.getLong(i7)));
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public void readEntity(Cursor cursor, GDAOCity gDAOCity, int i) {
        int i2 = i + 0;
        gDAOCity.setId(cursor.isNull(i2) ? null : Long.valueOf(cursor.getLong(i2)));
        int i3 = i + 1;
        gDAOCity.setName(cursor.isNull(i3) ? null : cursor.getString(i3));
        int i4 = i + 2;
        gDAOCity.setLatitude(cursor.isNull(i4) ? null : Float.valueOf(cursor.getFloat(i4)));
        int i5 = i + 3;
        gDAOCity.setLongitude(cursor.isNull(i5) ? null : Float.valueOf(cursor.getFloat(i5)));
        int i6 = i + 4;
        gDAOCity.setState(cursor.isNull(i6) ? null : Long.valueOf(cursor.getLong(i6)));
        int i7 = i + 5;
        gDAOCity.setCountry(cursor.isNull(i7) ? null : Long.valueOf(cursor.getLong(i7)));
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final void bindValues(SQLiteStatement sQLiteStatement, GDAOCity gDAOCity) {
        sQLiteStatement.clearBindings();
        Long id = gDAOCity.getId();
        if (id != null) {
            sQLiteStatement.bindLong(1, id.longValue());
        }
        String name = gDAOCity.getName();
        if (name != null) {
            sQLiteStatement.bindString(2, name);
        }
        Float latitude = gDAOCity.getLatitude();
        if (latitude != null) {
            sQLiteStatement.bindDouble(3, latitude.floatValue());
        }
        Float longitude = gDAOCity.getLongitude();
        if (longitude != null) {
            sQLiteStatement.bindDouble(4, longitude.floatValue());
        }
        Long state = gDAOCity.getState();
        if (state != null) {
            sQLiteStatement.bindLong(5, state.longValue());
        }
        Long country = gDAOCity.getCountry();
        if (country != null) {
            sQLiteStatement.bindLong(6, country.longValue());
        }
    }
}
