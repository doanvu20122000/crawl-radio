package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao;

import java.io.Serializable;

/* loaded from: classes.dex */
public class GDAOCountry implements Serializable {
    private String code;
    private String flag_url;
    private Long id;
    private String name;
    private Boolean show_in_list;
    private Boolean use_states;

    public GDAOCountry() {
    }

    public String getCode() {
        return this.code;
    }

    public String getFlag_url() {
        return this.flag_url;
    }

    public Long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public Boolean getShow_in_list() {
        return this.show_in_list;
    }

    public Boolean getUse_states() {
        return this.use_states;
    }

    public void setCode(String str) {
        this.code = str;
    }

    public void setFlag_url(String str) {
        this.flag_url = str;
    }

    public void setId(Long l) {
        this.id = l;
    }

    public void setName(String str) {
        this.name = str;
    }

    public void setShow_in_list(Boolean bool) {
        this.show_in_list = bool;
    }

    public void setUse_states(Boolean bool) {
        this.use_states = bool;
    }

    public GDAOCountry(Long l) {
        this.id = l;
    }

    public GDAOCountry(Long l, String str, String str2, String str3, Boolean bool, Boolean bool2) {
        this.id = l;
        this.name = str;
        this.flag_url = str2;
        this.code = str3;
        this.show_in_list = bool;
        this.use_states = bool2;
    }
}
