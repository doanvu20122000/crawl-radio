package com.fansipan.compass.bean.myapplication

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.fansipan.compass.bean.myapplication.code.convert.JsonData
import com.fansipan.compass.bean.myapplication.code.convert.formatNumber
import com.fansipan.compass.bean.myapplication.code.convert.gone
import com.fansipan.compass.bean.myapplication.code.convert.show
import com.fansipan.compass.bean.myapplication.databinding.ActivityMainBinding
import java.io.FileOutputStream
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream

const val TAG = "doanvv"

class MainActivity : AppCompatActivity() {
    private val binding by lazy {
        ActivityMainBinding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
//        DatabaseManager.copyDataBase(this)
//        copyDataBaseFromAssets(this)
//        val session = DatabaseManager.createSession(this)
//        val openDatabase = DatabaseManager.openDatabase(DB_NAME)
//        val list2 = DatabaseManager.loadDataFromCursor(
//            DatabaseManager.openDatabase(DB_PATH + DB_NAME).query("radio", arrayOf("*"), null, null, null, null, null)
//        )
//        Log.d(TAG, "onCreate: ${list2}")
//        val listRadio = DatabaseManager.loadRadio(session)
        binding.btnLoad.setOnClickListener {
            readData()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun readData() {
        showLoading()
        val start = System.currentTimeMillis()
        JsonData.loadAllDataFromJson(this) { stream: Long, radio: Long, country: Long, city: Long, state: Long, genre: Long, radioGenre: Long, radioCity: Long ->
            val timeLoad = System.currentTimeMillis() - start
            Log.d(TAG, "----------------time load: $timeLoad miliseconds")
            hideLoading()
            binding.tvResult.text = "time load: ${timeLoad.formatNumber()} ms\n" +
                    "radio: ${JsonData.listRadio.size.formatNumber()} items - loaded: ${radio.formatNumber()} ms\n" +
                    "stream: ${JsonData.listStream.size.formatNumber()} items - loaded: ${stream.formatNumber()} ms\n" +
                    "country: ${JsonData.listCountry.size.formatNumber()} items - loaded: ${country.formatNumber()} ms\n" +
                    "city: ${JsonData.listCity.size.formatNumber()} items - loaded: ${city.formatNumber()} ms\n" +
                    "state: ${JsonData.listState.size.formatNumber()} items - loaded: ${state.formatNumber()} ms\n" +
                    "genre: ${JsonData.listGenre.size.formatNumber()} items - loaded: ${genre.formatNumber()} ms\n" +
                    "radio_genre: ${JsonData.listRadioGenre.size.formatNumber()} items - loaded: ${radioGenre.formatNumber()} " +
                    "ms\n" +
                    "radio_city: ${JsonData.listRadioCity.size.formatNumber()} items - loaded: ${radioCity.formatNumber()} ms\n"
            val listRadio = JsonData.listRadio
            Log.d(TAG, "listRadio: ${listRadio.size}")
//            val radio = listRadio.subList(0, 100)
            listRadio.forEach { radioEntity ->
                //            val listStream = DatabaseManager.loadStream(it.id.toLong())
                //            val listUrl = it.getUrls(listStream)
                //            listUrl.forEach { streamWrapper ->
                //                val url = streamWrapper.getmUnresolvedUrls().poll()?.singleURL
                //                Log.d(TAG, "decode auto: ${it.id} - $url")
                //            }
                //                val listStream = JsonData.listStream.filter { it.radio == radioEntity.id }
                //                listStream.forEach { gad ->
                //                    val url = DecodeBase64.deObfuscate(gad.id.toLong(), gad.radio.toLong(), gad.url)
                //                    if (url.isNotEmpty()) {
                //                        Log.d(TAG, "decode by function: ${radioEntity.id} - $url")
                //                    }
                //                }
            }
        }
    }

    private fun showLoading() {
        binding.loading.show()
        binding.tvResult.gone()
    }

    private fun hideLoading() {
        binding.loading.gone()
        binding.tvResult.show()
    }

    private val DB_PATH = "/data/data/com.fansipan.compass.bean.myapplication/databases/"
    private val DB_NAME = "ituner.sqlite"

    private fun copyDataBaseFromAssets(context: Context) {
        var myInput: InputStream? = null
        var myOutput: OutputStream? = null
        try {

            val folder = context.getDatabasePath("databases")

            if (!folder.exists())
                if (folder.mkdirs()) folder.delete()

            myInput = context.assets.open("database/$DB_NAME")
            val path = "/data/data/${context.packageName}/databases/"
            val outFileName = path + DB_NAME

            myOutput = FileOutputStream(outFileName)

            //transfer bytes from the inputfile to the outputfile
            val buffer = ByteArray(1024)
            var length: Int = myInput.read(buffer)

            while (length > 0) {
                myOutput!!.write(buffer, 0, length)
                length = myInput.read(buffer)
            }

            //Close the streams
            myOutput!!.flush()
            myOutput.close()
            myInput.close()
            Log.d(TAG, "copyDataBaseFromAssets: ok")
        } catch (e: IOException) {
            e.printStackTrace()
        }

    }
}