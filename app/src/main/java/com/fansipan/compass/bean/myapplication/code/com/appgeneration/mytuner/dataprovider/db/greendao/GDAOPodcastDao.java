package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import com.fansipan.compass.bean.myapplication.code.com.POBRequest$AdPosition$EnumUnboxingLocalUtility;
import com.fansipan.compass.bean.myapplication.code.com.greendao.AbstractDao;
import com.fansipan.compass.bean.myapplication.code.com.greendao.Property;
import com.fansipan.compass.bean.myapplication.code.com.greendao.database.Database;
import com.fansipan.compass.bean.myapplication.code.com.greendao.database.DatabaseStatement;
import com.fansipan.compass.bean.myapplication.code.com.greendao.internal.DaoConfig;

/* loaded from: classes.dex */
public class GDAOPodcastDao extends AbstractDao<GDAOPodcast, Long> {
    public static final String TABLENAME = "podcasts";

    /* loaded from: classes.dex */
    public static class Properties {
        public static final Property Id = new Property(0, Long.class, "id", true, "ID");
        public static final Property Name = new Property(1, String.class, "name", false, "NAME");
        public static final Property Artist = new Property(2, String.class, "artist", false, "ARTIST");
        public static final Property Description = new Property(3, String.class, "description", false, "DESCRIPTION");
        public static final Property Image_url = new Property(4, String.class, "image_url", false, "IMAGE_URL");
    }

    public GDAOPodcastDao(DaoConfig daoConfig) {
        super(daoConfig);
    }

    public static void createTable(Database database, boolean z) {
        String str;
        if (z) {
            str = "IF NOT EXISTS ";
        } else {
            str = "";
        }
        database.execSQL("CREATE TABLE " + str + "\"podcasts\" (\"ID\" INTEGER PRIMARY KEY AUTOINCREMENT ,\"NAME\" TEXT,\"ARTIST\" TEXT,\"DESCRIPTION\" TEXT,\"IMAGE_URL\" TEXT);");
    }

    public static void dropTable(Database database, boolean z) {
        String str;
        StringBuilder m = new StringBuilder("DROP TABLE ");
        if (z) {
            str = "IF EXISTS ";
        } else {
            str = "";
        }
        POBRequest$AdPosition$EnumUnboxingLocalUtility.m(m, str, "\"podcasts\"", database);
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final boolean isEntityUpdateable() {
        return true;
    }

    public GDAOPodcastDao(DaoConfig daoConfig, DaoSession daoSession) {
        super(daoConfig, daoSession);
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public Long getKey(GDAOPodcast gDAOPodcast) {
        if (gDAOPodcast != null) {
            return gDAOPodcast.getId();
        }
        return null;
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public boolean hasKey(GDAOPodcast gDAOPodcast) {
        return gDAOPodcast.getId() != null;
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public Long readKey(Cursor cursor, int i) {
        int i2 = i + 0;
        if (cursor.isNull(i2)) {
            return null;
        }
        return Long.valueOf(cursor.getLong(i2));
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final Long updateKeyAfterInsert(GDAOPodcast gDAOPodcast, long j) {
        gDAOPodcast.setId(Long.valueOf(j));
        return Long.valueOf(j);
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final void bindValues(DatabaseStatement databaseStatement, GDAOPodcast gDAOPodcast) {
        databaseStatement.clearBindings();
        Long id = gDAOPodcast.getId();
        if (id != null) {
            databaseStatement.bindLong(1, id.longValue());
        }
        String name = gDAOPodcast.getName();
        if (name != null) {
            databaseStatement.bindString(2, name);
        }
        String artist = gDAOPodcast.getArtist();
        if (artist != null) {
            databaseStatement.bindString(3, artist);
        }
        String description = gDAOPodcast.getDescription();
        if (description != null) {
            databaseStatement.bindString(4, description);
        }
        String image_url = gDAOPodcast.getImage_url();
        if (image_url != null) {
            databaseStatement.bindString(5, image_url);
        }
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public GDAOPodcast readEntity(Cursor cursor, int i) {
        int i2 = i + 0;
        Long valueOf = cursor.isNull(i2) ? null : Long.valueOf(cursor.getLong(i2));
        int i3 = i + 1;
        String string = cursor.isNull(i3) ? null : cursor.getString(i3);
        int i4 = i + 2;
        String string2 = cursor.isNull(i4) ? null : cursor.getString(i4);
        int i5 = i + 3;
        int i6 = i + 4;
        return new GDAOPodcast(valueOf, string, string2, cursor.isNull(i5) ? null : cursor.getString(i5), cursor.isNull(i6) ? null : cursor.getString(i6));
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public void readEntity(Cursor cursor, GDAOPodcast gDAOPodcast, int i) {
        int i2 = i + 0;
        gDAOPodcast.setId(cursor.isNull(i2) ? null : Long.valueOf(cursor.getLong(i2)));
        int i3 = i + 1;
        gDAOPodcast.setName(cursor.isNull(i3) ? null : cursor.getString(i3));
        int i4 = i + 2;
        gDAOPodcast.setArtist(cursor.isNull(i4) ? null : cursor.getString(i4));
        int i5 = i + 3;
        gDAOPodcast.setDescription(cursor.isNull(i5) ? null : cursor.getString(i5));
        int i6 = i + 4;
        gDAOPodcast.setImage_url(cursor.isNull(i6) ? null : cursor.getString(i6));
    }

    @Override // org.greenrobot.greendao.AbstractDao
    public final void bindValues(SQLiteStatement sQLiteStatement, GDAOPodcast gDAOPodcast) {
        sQLiteStatement.clearBindings();
        Long id = gDAOPodcast.getId();
        if (id != null) {
            sQLiteStatement.bindLong(1, id.longValue());
        }
        String name = gDAOPodcast.getName();
        if (name != null) {
            sQLiteStatement.bindString(2, name);
        }
        String artist = gDAOPodcast.getArtist();
        if (artist != null) {
            sQLiteStatement.bindString(3, artist);
        }
        String description = gDAOPodcast.getDescription();
        if (description != null) {
            sQLiteStatement.bindString(4, description);
        }
        String image_url = gDAOPodcast.getImage_url();
        if (image_url != null) {
            sQLiteStatement.bindString(5, image_url);
        }
    }
}
