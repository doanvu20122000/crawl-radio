package com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db;

import android.content.Context;

import com.fansipan.compass.bean.myapplication.code.com.appgeneration.mytuner.dataprovider.db.greendao.DaoMaster;
import com.fansipan.compass.bean.myapplication.code.com.greendao.database.Database;


/* loaded from: classes.dex */
public class DatabaseOpenHelper extends DaoMaster.OpenHelper {
    public DatabaseOpenHelper(Context context, String str) {
        super(context, str, null);
    }

    private void upgrade(Database database, int i) {
        if (i == 1040000) {
            database.execSQL("ALTER TABLE \"stream\" ADD COLUMN 'params_json' TEXT NOT NULL DEFAULT '' ;");
        }
    }

    @Override
    // com.appgeneration.mytuner.dataprovider.db.greendao.DaoMaster.OpenHelper, org.greenrobot.greendao.database.DatabaseOpenHelper
    public void onCreate(Database database) {
        DaoMaster.createAllTables(database, true);
    }

    @Override // org.greenrobot.greendao.database.DatabaseOpenHelper
    public void onUpgrade(Database database, int i, int i2) {
        while (true) {
            i += 1000;
            if (i <= i2) {
                upgrade(database, i);
            } else {
                return;
            }
        }
    }
}
